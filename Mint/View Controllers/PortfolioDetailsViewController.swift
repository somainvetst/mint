//
//  PortfolioDetailsViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 14/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import NVActivityIndicatorView
import UINavigationItem_Margin
class PortfolioDetailsViewController: UIViewController , goinside , UITabBarControllerDelegate , Completion , Savepdfs , Menu {
    
    
    //loaderComponents
    
    @IBOutlet weak var head1: UIView!
    
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var selectOptions: UISegmentedControl!
    @IBOutlet weak var shimmeringLoader: ShimmerView!
    @IBOutlet weak var loader: UIView!
    @IBOutlet weak var floatingView: UIView!
    @IBOutlet weak var viewLeading: NSLayoutConstraint!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activity: NVActivityIndicatorView!
    @IBOutlet weak var selectionView: UIView!
    @IBOutlet weak var otherassert: UILabel!
    @IBOutlet weak var fixedDeposit: UILabel!
    @IBOutlet weak var shareBond: UILabel!
    @IBOutlet weak var mutualFund: UILabel!
   // @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var headView: HeaderView!
    @IBOutlet weak var table: UITableView!
    var type:Int = 1
    var lastClicked = 1
    var portfolio = GetFamilyPortfolioDetails()
    var share = ShareBond()
    var fixed = FixedDeposit()
    let selection = UISelectionFeedbackGenerator()
  var selectedIndexe: Int = 0 {
        didSet {
           handleTabSelection(selectedIndex: selectedIndexe)
        }
     }
    var other = OtherAsserts()
    let graycolor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0)
    var id:String = ""
   var pdf = DownloadPortfolioPdf()
    var leftBarButtonItem : UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
         table.tableFooterView = UIView()
        activityView.isHidden = true
        emptyStateView.isHidden = true
        navigationController?.navigationBar.isHidden = false
        self.leftBarButtonItem = UIBarButtonItem(title: "Back", style:.plain, target: nil, action: nil)
        self.tabBarController?.navigationItem.rightBarButtonItem  = UIBarButtonItem(image:#imageLiteral(resourceName: "pdf") , style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openPdf(sender:)))
       loader.isHidden = true
                   shimmeringLoader.isHidden = false
                   shimmeringLoader.startShimmeringAnimation()
         mutualFund.textColor = Shared.sharedInstance.color
         //   backBtn.isHidden = true
            share.delegate = self
        portfolio.delegate = self
        fixed.delegate = self
        other.delegate = self
        pdf.delegate = self
//         self.tabBarController?.delegate = self
        CoreData.core.fetchCartData(entity: "Logindata")
        table.register(cell: DashboardTableViewCell.self)
        table.register(cell: PortfolioDetailsTableViewCell.self)
        table.register(cell: FolioLookupTableViewCell.self)
        table.register(cell: PortfolioDetailHeader2TableViewCell.self)
        if CoreData.core.data.isEmpty == false {
            
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                return
            }
           
            guard  let id =  login["result"]?["uid"] as? String else {
                
                return
            }
            self.id = id
        }
        portfolio.network(table: table, id: id, componentName: "folioTable")
    }
 
    @objc func openPdf(sender: UIBarButtonItem) {
        activityView.isHidden = false
               activity.startAnimating()
          if Shared.sharedInstance.clientType == "" {
             pdf.network(levelno:  "98" , uid: id) { (result: String , success: Bool) in
                 if success {
                     print("yes")
                     print(self.id)
                 } }
             }
             else {
                 pdf.network(levelno:  "98" , uid: Shared.sharedInstance.typeId) { (result: String , success: Bool) in
                     if success {
                         print("yes")
                         print(self.id)
                     } }
             }
      }
    private func handleTabSelection(selectedIndex: Int) {
     //   Routes.shared.gotoFolio(nav: (tabBarController?.navigationController!)!)
    }
    func closeMenu() {
        self.tabBarController?.navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.setNavigationBarHidden(false, animated: false)
               navigationController?.navigationBar.isHidden = false
               navigationController?.navigationBar.topItem?.title = "         Portfolio Details        "
               navigationController?.navigationBar.prefersLargeTitles = false
               navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
             self.tabBarController?.navigationItem.hidesBackButton = true
    }
    override func viewWillAppear(_ animated: Bool) {
        let titleLabel:UILabel = UILabel(frame: CGRect(x: -45, y: 10, width: 150, height: 25))
        titleLabel.textAlignment = .center
               titleLabel.text = "Portfolio Details"
        let navigationView = UIView(frame: CGRect(x: 0, y: 0, width: 50 , height: 55))
        navigationView.addSubview(titleLabel)
        self.tabBarController?.navigationController?.isToolbarHidden = true
          navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
      //  navigationController?.navigationBar.topItem?.title = "Portfolio Details"
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        self.tabBarController?.navigationItem.hidesBackButton = true
         self.tabBarController?.navigationItem.titleView = navigationView
        let pdfbutton = UIBarButtonItem(image:#imageLiteral(resourceName: "pdf") , style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openPdf(sender:)))
        pdfbutton.imageInsets  = UIEdgeInsets(top: 0, left: 80, bottom: 0, right: -15);
        self.tabBarController?.navigationItem.setRightBarButtonItems([pdfbutton], animated: true)
        
        
    }
   
  override func viewWillDisappear(_ animated: Bool) {
      super.viewWillDisappear(true)
 //   self.tabBarController?.navigationController?.isToolbarHidden = true
    //  navigationController?.setNavigationBarHidden(true, animated: false)
  }
    
    func goback() {
        navigationController?.popViewController(animated: true)
    }
    
    func goinside(index: IndexPath) {
       
    }
    func complete(data: JSON?) {
        shimmeringLoader.isHidden = true
        portfolio.data = data
        print("here")
        if lastClicked == type {
            table.reloadData()
        }
        else if lastClicked > type {
        let transition = CATransition()
        transition.type = CATransitionType.push
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.fillMode = CAMediaTimingFillMode.forwards
        transition.duration = 0.5
        transition.subtype = CATransitionSubtype.fromLeft
        self.table.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
        table.reloadData()
        }
        else if lastClicked < type {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromRight
            self.table.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            table.reloadData()
        }
    }
    func slideView(d:Int) {
      
        if lastClicked == type {
             viewLeading.constant = CGFloat(Int(mutualFund.bounds.width) * d)
        }
        else if lastClicked > type {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromLeft
            self.floatingView.layer.add(transition, forKey: "kCATransition")
              viewLeading.constant = CGFloat(Int(mutualFund.bounds.width) * d)
        }
        else if lastClicked < type {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromRight
            self.floatingView.layer.add(transition, forKey: "kCATransition")
              viewLeading.constant = CGFloat(Int(mutualFund.bounds.width) * d)
        }
        
    }
    
    @IBAction func selecting(_ sender: Any) {
         selection.selectionChanged()
        switch selectOptions.selectedSegmentIndex {
        case 0:
            lastClicked = type
             type = 1
//            shimmeringLoader.isHidden = false
//            shimmeringLoader.startShimmeringAnimation()
              portfolio.network(table: table, id: id, componentName: "folioTable")
        case 1:
            lastClicked = type
             type = 2
//            shimmeringLoader.isHidden = false
//                       shimmeringLoader.startShimmeringAnimation()
             share.network(table: table, id: id)
        case 2:
            lastClicked = type
             type = 3
//            shimmeringLoader.isHidden = false
//                       shimmeringLoader.startShimmeringAnimation()
             fixed.network(table: table, id: id)
        default:
            lastClicked = type
             type = 4
//            shimmeringLoader.isHidden = false
//                       shimmeringLoader.startShimmeringAnimation()
             other.network(table: table, id: id)
        }
    }
    @IBAction func back(_ sender: Any) {
        Shared.sharedInstance.clientType = ""
        Shared.sharedInstance.typeLevel = ""
        Shared.sharedInstance.typeId = ""
        Routes.shared.gotoTab(nav: navigationController!)
    }
    
//pdf
    func showActivity(vc: UIViewController) {
        activityView.isHidden = true
        activity.stopAnimating()
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func downloadPdf(_ sender: Any) {
        activityView.isHidden = false
        activity.startAnimating()
        if Shared.sharedInstance.clientType == "" {
        pdf.network(levelno:  "98" , uid: id) { (result: String , success: Bool) in
            if success {
                print("yes")
                print(self.id)
            } }
        }
        else {
            pdf.network(levelno:  "98" , uid: Shared.sharedInstance.typeId) { (result: String , success: Bool) in
                if success {
                    print("yes")
                    print(self.id)
                } }
        }
    }
    
    
//Page Actions
    
    @IBAction func mutualFundTapped(_ sender: Any) {
        lastClicked = type
        type = 1
        
         mutualFund.textColor = Shared.sharedInstance.color
         shareBond.textColor = graycolor
         fixedDeposit.textColor = graycolor
         otherassert.textColor = graycolor
         portfolio.network(table: table, id: id, componentName: "folioTable")
         slideView(d: 0)
        
    }
    
    @IBAction func shareBondTapped(_ sender: Any) {
        lastClicked = type
         type = 2
       
        mutualFund.textColor = graycolor
        shareBond.textColor = Shared.sharedInstance.color
        fixedDeposit.textColor = graycolor
        otherassert.textColor = graycolor
        share.network(table: table, id: id)
         slideView(d: 1)
    }
    
    @IBAction func fixedDepositTapped(_ sender: Any) {
        lastClicked = type
         type = 3
       
        mutualFund.textColor = graycolor
        shareBond.textColor = graycolor
        fixedDeposit.textColor = Shared.sharedInstance.color
        otherassert.textColor = graycolor
         fixed.network(table: table, id: id)
         slideView(d: 2)
    }
    
    @IBAction func otherAssertTapped(_ sender: Any) {
        lastClicked = type
         type = 4
       
        mutualFund.textColor = graycolor
        shareBond.textColor = graycolor
        fixedDeposit.textColor = graycolor
        otherassert.textColor = Shared.sharedInstance.color
        other.network(table: table, id: id)
         slideView(d: 3)
    }
    
}
extension PortfolioDetailsViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        switch section {
        case 0:
            
           
            if portfolio.data?["result"]["data"].count == 0 {
                tableView.isHidden = true
                emptyStateView.isHidden = false
                return 0
            }
            else {
               
                tableView.isHidden = false
                emptyStateView.isHidden = true
                return 1
            }
        default:
            return portfolio.data?["result"]["data"].count ?? 0
        }
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if type == 1 || type == 2 {
            let cell = tableView.dequeueReusableCell(with: DashboardTableViewCell.self)
//            cell?.backgroundImage.image = UIImage(named: "card_blank")
                cell?.marketChangeImg.isHidden = true
                cell?.MarketChange.isHidden = true
            cell?.loginView.isHidden = true
            cell?.detailsView.isHidden = false
            cell?.detailsView.backgroundColor = .clear
                cell?.bottomHead1.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                cell?.bottomhead2.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                cell?.bottomhead3.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                cell?.bottomValue1.textColor = #colorLiteral(red: 0.5107896924, green: 0.5111948848, blue: 0.5108524561, alpha: 1)
                cell?.bottomValue2.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                cell?.bottomValue3.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                cell?.nameofClient.textColor = #colorLiteral(red: 0.3764705882, green: 0.3450980392, blue: 0.9960784314, alpha: 1)
                cell?.market.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                cell?.marketValue.textColor = #colorLiteral(red: 0.3176470588, green: 0.4705882353, blue: 0.6901960784, alpha: 1)
            cell?.bottomHead1.text = "Purchase Cost"
            cell?.bottomhead2.text = "Gain"
            cell?.bottomhead3.text = "CAGR"
            cell?.market.text = "Market Value"
            cell?.nameofClient.text = Shared.sharedInstance.clientName
            guard let market = portfolio.data?["result"]["summary"]["currentValue"].stringValue , let bottom1 = portfolio.data?["result"]["summary"]["purchaseValue"].stringValue , let bottom2 = portfolio.data?["result"]["summary"]["gain"].stringValue , let bottom3 = portfolio.data?["result"]["summary"]["CAGR"].stringValue  else { return UITableViewCell()}
            guard let marketvalue = Double(market)?.roundtoPlace(places: 2) , let bottom1round = Double(bottom1)?.roundtoPlace(places: 2) , let bottom2round = Double(bottom2)?.roundtoPlace(places: 2)  , let bottom3round = Double(bottom3)?.roundtoPlace(places: 2)  else {return UITableViewCell() }
                cell?.marketValue.text = String(marketvalue).toCurrencyFormat()
            cell?.bottomValue1.text = String(bottom1round).toCurrencyFormat()
            cell?.bottomValue2.text = String(bottom2round).toCurrencyFormat()
            cell?.bottomValue3.text = "\(String(bottom3round))%"
          print("time")
          //  print(portfolio.data)
            return cell ?? UITableViewCell()
            }
            else {
                
                let cell = tableView.dequeueReusableCell(with: PortfolioDetailHeader2TableViewCell.self)
               
              //   cell?.imageV.image = UIImage(named: "card_blank")
                 if type == 3 {
                    cell?.topMiddle.isHidden = false
                    cell?.topMiddleValue.isHidden = false
                    cell?.topLeft.text = "Investment Amount"
                    cell?.topMiddle.text = "Current Value"
                    cell?.topRight.text = "Abs Return"
                    cell?.bottomone.text = "Interest Accrued"
                    cell?.bottomtwo.text = "Gain"
                    cell?.bottomthree.text = "CAGR"
                    cell?.topLeftValue.text = portfolio.data?["result"]["summary"]["purchaseValue"].stringValue.toCurrencyFormat()
                    cell?.topRightValue.text = "\(portfolio.data?["result"]["summary"]["absoluteReturn"].stringValue.roundOffString() ?? "0")%"
                    cell?.topMiddleValue.text = portfolio.data?["result"]["summary"]["currentValue"].stringValue.toCurrencyFormat()
                    cell?.bottomvalue1.text = portfolio.data?["result"]["summary"]["dividend"].stringValue.toCurrencyFormat()
                    cell?.bottomvalue2.text = portfolio.data?["result"]["summary"]["gain"].stringValue.toCurrencyFormat()
                    cell?.bottomvalue3.text = "\(portfolio.data?["result"]["summary"]["CAGR"].stringValue.roundOffString() ?? "0")%"
                }
                 else {
                    cell?.topLeft.text = "Purchase Value"
                    cell?.topMiddle.isHidden = true
                    cell?.topMiddleValue.isHidden = true
                    cell?.topRight.text = "Current Value"
                    cell?.bottomone.text = "Interest Accrued"
                    cell?.bottomtwo.text = "Gain"
                    cell?.bottomthree.text = "XIRR"
                    cell?.topLeftValue.text = portfolio.data?["result"]["summary"]["purchaseValue"].stringValue.toCurrencyFormat()
                    cell?.topRightValue.text = portfolio.data?["result"]["summary"]["currentValue"].stringValue.toCurrencyFormat()
                    cell?.bottomvalue1.text = portfolio.data?["result"]["summary"]["sellValue"].stringValue.toCurrencyFormat()
                    cell?.bottomvalue2.text = portfolio.data?["result"]["summary"]["gain"].stringValue.toCurrencyFormat()
                     cell?.bottomvalue3.text = "\(portfolio.data?["result"]["summary"]["XIRR"].stringValue.roundOffString() ?? "0")%"
                }
                
                return cell ?? UITableViewCell()
            }
        default:
            if type == 1 || type == 2 {
            let cell = tableView.dequeueReusableCell(with: PortfolioDetailsTableViewCell.self)
            cell?.delegate = self
            cell?.index = indexPath
            guard let folio = portfolio.data?["result"]["data"][indexPath.row]["currentValue"].stringValue , let bottom1 = portfolio.data?["result"]["data"][indexPath.row]["purchaseValue"].stringValue , let bottom2 = portfolio.data?["result"]["data"][indexPath.row]["gain"].stringValue , let bottom3 = portfolio.data?["result"]["data"][indexPath.row]["CAGR"].stringValue  else { return UITableViewCell()}
            guard let foliovalue = Double(folio)?.roundtoPlace(places: 2) , let bottom1round = Double(bottom1)?.roundtoPlace(places: 2) , let bottom2round = Double(bottom2)?.roundtoPlace(places: 2)  , let bottom3round = Double(bottom3)?.roundtoPlace(places: 2)  else {return UITableViewCell() }
            cell?.bottomHead1.text = "Purchase Cost"
            cell?.bottomhead2.text = "Gain"
            cell?.bottomhead3.text = "CAGR"
            cell?.market.isHidden = false
            cell?.marketValue.isHidden = false
          //  cell?.nameTop.constant = 30
            //cell?.markettop.constant = 0
            //cell?.marketValuetop.constant = 0
          //  cell?.folioBottom.constant = 0
            cell?.marketValBottom.constant = 10
            cell?.market.text = "Market value"
            cell?.marketValue.text = String(foliovalue).toCurrencyFormat()
             cell?.folioTop.constant = -20
            cell?.folio.isHidden = true
            cell?.foliovalue.isHidden = true
            //cell?.folio.text = "Market value"
           // cell?.foliovalue.text = String(foliovalue).toCurrencyFormat()
            cell?.schemeName.text =  portfolio.data?["result"]["data"][indexPath.row]["applicantName"].stringValue
           
            
            cell?.bottomValue1.text = String(bottom1round).toCurrencyFormat()
            cell?.bottomValue2.text = String(bottom2round).toCurrencyFormat()
            cell?.bottomValue3.text = "\(String(bottom3round))%"
            if String(bottom2round).hasPrefix("-") {
                cell?.bottomvalueimage2.image = UIImage(named: "down")
            }
            else {
                 cell?.bottomvalueimage2.image = UIImage(named: "up")
            }
            if String(bottom3round).hasPrefix("-") {
                cell?.bottomvalueimage3.image = UIImage(named: "down")
            }
            else {
                cell?.bottomvalueimage3.image = UIImage(named: "up")
            }
            return cell ?? UITableViewCell()
            }
            else {
                let cell = tableView.dequeueReusableCell(with: FolioLookupTableViewCell.self)
                cell?.sidemenuButton.isHidden = true
                if type == 3 {
                cell?.amount.text = "Investment Amount"
                cell?.folio.text = "Current Value"
                cell?.ucc.text = "Abs Return"
                cell?.investor.text = "Interest Accrued"
                cell?.holding.text = "Gain"
                cell?.pan.text = "CAGR"
                cell?.name.text =  portfolio.data?["result"]["data"][indexPath.row]["applicantName"].stringValue
                cell?.schemeName.text = portfolio.data?["result"]["data"][indexPath.row]["fixedAssetName"].stringValue
                cell?.amountValue.text = portfolio.data?["result"]["data"][indexPath.row]["purchaseValue"].stringValue.toCurrencyFormat()
                cell?.foliovalue.text = portfolio.data?["result"]["data"][indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
                cell?.uccValue.text = "\(portfolio.data?["result"]["data"][indexPath.row]["absoluteReturn"].stringValue.roundOffString() ?? "0")%"
                cell?.investorvalue.text = portfolio.data?["result"]["data"][indexPath.row]["dividend"].stringValue.toCurrencyFormat()
                cell?.holdingvalue.text = portfolio.data?["result"]["data"][indexPath.row]["gain"].stringValue.toCurrencyFormat()
                cell?.panvalue.text = "\(portfolio.data?["result"]["data"][indexPath.row]["CAGR"].stringValue.roundOffString() ?? "0")%"
                }
                else if type == 4 {
                    cell?.amount.text = "Investment Amount"
                    cell?.folio.text = "Current Value"
                    cell?.ucc.text = "XIRR"
                    cell?.investor.text = "Total Sold Value"
                    cell?.holding.text = "Gain"
                    cell?.pan.text = "Start Date"
                    cell?.name.text =  portfolio.data?["result"]["data"][indexPath.row]["applicantName"].stringValue
                    cell?.schemeName.text = portfolio.data?["result"]["data"][indexPath.row]["assetName"].stringValue
                    cell?.amountValue.text = portfolio.data?["result"]["data"][indexPath.row]["purchaseValue"].stringValue.toCurrencyFormat()
                    cell?.foliovalue.text = portfolio.data?["result"]["data"][indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
                    cell?.uccValue.text = "\(portfolio.data?["result"]["data"][indexPath.row]["XIRR"].stringValue.roundOffString() ?? "0")%"
                    cell?.investorvalue.text = portfolio.data?["result"]["data"][indexPath.row]["sellValue"].stringValue.toCurrencyFormat()
                    var startdate =  portfolio.data?["result"]["data"][indexPath.row]["startDate"].stringValue
                    
                    cell?.holdingvalue.text = portfolio.data?["result"]["data"][indexPath.row]["gain"].stringValue.toCurrencyFormat()
                    print(startdate)
                    cell?.panvalue.text = startdate?.toDate()
                }
                return cell ?? UITableViewCell()
            }
           
        }
       
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1 {
        switch type {
        
        case 1:
        let vc: PortfolioClientViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioClientViewController") as! PortfolioClientViewController;
        guard let id = portfolio.data?["result"]["data"][indexPath.row]["appid"].stringValue else {return}
        vc.folioid = id
        navigationController?.pushViewController(vc, animated: true)
        case 2:
            let vc:ShareBondViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "ShareBondViewController") as! ShareBondViewController
            guard let appid = portfolio.data?["result"]["data"][indexPath.row]["appid"].stringValue else {return}
             vc.id = id
             Shared.sharedInstance.typeAppid = appid
            navigationController?.pushViewController(vc, animated: true)
        case 3:
            return
        default:
            return
    }
    }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
