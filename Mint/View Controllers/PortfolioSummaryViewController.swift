//
//  PortfolioSummaryViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 19/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import NVActivityIndicatorView
class PortfolioSummaryViewController: UIViewController , Completion , Savepdfs {
    
    @IBOutlet weak var emptyStateView: UIView!
    @IBOutlet weak var filters: UISegmentedControl!
    @IBOutlet weak var selectionView: CardView!
    @IBOutlet weak var filterViewHeight: NSLayoutConstraint!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var otherassert: UILabel!
    @IBOutlet weak var fixedDeposit: UILabel!
    @IBOutlet weak var shareBond: UILabel!
    @IBOutlet weak var mutualFund: UILabel!
    @IBOutlet weak var floatingView: UIView!
    @IBOutlet weak var viewLeading: NSLayoutConstraint!
    @IBOutlet weak var selectOptions: UISegmentedControl!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activity: NVActivityIndicatorView!
     var delegates:dismissing?
    let graycolor = UIColor(red: 150.0/255.0, green: 150.0/255.0, blue: 150.0/255.0, alpha: 1.0)
    var type:Int = 1
    var lastClicked = 1
    var selectedFilter:Int = 1
    var network = PortfolioSummary()
    var groupid = "1002"
    var id:String = ""
    var levelId:String = ""
    var pdf = DownloadPortfolioPdf()
      let selection = UISelectionFeedbackGenerator()
    override func viewDidLoad() {
        super.viewDidLoad()
        activityView.isHidden = true
        pdf.delegate = self
        emptyStateView.isHidden = true
        table.tableFooterView = UIView()
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = false
        self.title = "Portfolio Summary"
        self.navigationController?.navigationBar.tintColor = UIColor.black
        network.delegate = self
        //register xibs
        table.register(cell: DashboardTableViewCell.self)
        table.register(cell: PortfolioSummaryTableViewCell.self)
        network.getMutualFunds(table: table, groupid: groupid)
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        let titleLabel:UILabel = UILabel(frame: CGRect(x: -45, y: 10, width: 150, height: 25))
               titleLabel.textAlignment = .center
                      titleLabel.text = "Portfolio Summary"
               let navigationView = UIView(frame: CGRect(x: 0, y: 0, width: 50 , height: 55))
               navigationView.addSubview(titleLabel)
        self.navigationItem.titleView = navigationView
        let pdfbutton = UIBarButtonItem(image:#imageLiteral(resourceName: "pdf") , style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.openPdf(sender:)))
        pdfbutton.imageInsets  = UIEdgeInsets(top: 0, left: 80, bottom: 0, right: -15);
        self.navigationItem.setRightBarButtonItems([pdfbutton], animated: true)
        //coredata
        if CoreData.core.data.isEmpty == false {
            
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                return
            }
            
            guard  let id =  login["result"]?["uid"] as? String else {
                
                return
            }
            guard  let level =  login["result"]?["levelNo"] as? Int else {
                
                return
            }
            self.id = id
            self.levelId = String(level)
        }
    }
    
    
    @objc func back(sender: UIBarButtonItem) {
        delegates?.close()
        dismiss(animated: true, completion: nil)
    }
    @objc func openPdf(sender: UIBarButtonItem) {
                 activityView.isHidden = false
                      activity.startAnimating()
        if Shared.sharedInstance.clientType == "" {
            pdf.networkSummary(levelno:  levelId , uid: id) { (result: String , success: Bool) in
                if success {
                    print("yes")
                    print(self.id)
                } }
        }
        else {
            pdf.networkSummary(levelno:  Shared.sharedInstance.typeLevel , uid: Shared.sharedInstance.typeId) { (result: String , success: Bool) in
                if success {
                    print("yes")
                    print(self.id)
                } }
        }
        //https://moneyempire.investwell.app/api/client/dashboard/downloadPortfolioSummary?filters=[{%22endDate%22:%222019-10-11%22},{},{%22activeFoliosOnly%22:%22false%22}]&selectedUser={%22uid%22:%2206cb8375c19dc50838349cb06ca4ed56%22,%22levelNo%22:%22100%22}
        
        //https://demo.investwell.app/api/client/dashboard/downloadPortfolioSummary?filters=[{%22endDate%22:%222019-10-11%22},{},{%22activeFoliosOnly%22:%22false%22}]&selectedUser={%22uid%22:%22ae60d4ed4afb6ea528d628257330efcb%22,%22levelNo%22:%2298%22}
    }
    @IBAction func back(_ sender: Any) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = false
        
        self.title = "Portfolio Summary"
        self.navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = true
        self.navigationItem.hidesBackButton = false
    }
    func showActivity(vc: UIViewController) {
        activityView.isHidden = true
       activity.stopAnimating()
        present(vc, animated: true, completion: nil)
    }
    
    //callback
    func complete(data: JSON?) {
        network.data = data
        if lastClicked == type {
            table.reloadData()
        }
        else if lastClicked > type {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromLeft
            self.table.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            table.reloadData()
        }
        else if lastClicked < type {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromRight
            self.table.layer.add(transition, forKey: "UITableViewReloadDataAnimationKey")
            table.reloadData()
        }
    }
    @IBAction func selecting(_ sender: Any) {
        selection.selectionChanged()
        switch selectOptions.selectedSegmentIndex {
        case 0:
            if filters.numberOfSegments == 2 {
                filters.insertSegment(withTitle: "Folio", at: 2, animated: false)
                filters.setTitle("Scheme", forSegmentAt: 0)
            }
            filterViewHeight.constant = 80
            filterView.isHidden = false
            lastClicked = type
            type = 1
            //            shimmeringLoader.isHidden = false
            //            shimmeringLoader.startShimmeringAnimation()
            network.getMutualFunds(table: table, groupid: groupid)
        case 1:
           
            if filters.numberOfSegments == 3 {
                filters.removeSegment(at: 2, animated: false)
                filters.setTitle("Scrip", forSegmentAt: 0)
            }
            filterViewHeight.constant = 80
            filterView.isHidden = false
            lastClicked = type
            type = 2
            groupid = "1002"
            //            shimmeringLoader.isHidden = false
            //                       shimmeringLoader.startShimmeringAnimation()
            network.getSchemeBond(table: table, groupid: groupid)
        case 2:
            filterViewHeight.constant = 0
            filterView.isHidden = true
            lastClicked = type
            type = 3
            //            shimmeringLoader.isHidden = false
            //                       shimmeringLoader.startShimmeringAnimation()
            network.getFixedDeposit(table: table)
        default:
            filterViewHeight.constant = 0
            filterView.isHidden = true
            lastClicked = type
            type = 4
            //            shimmeringLoader.isHidden = false
            //                       shimmeringLoader.startShimmeringAnimation()
            network.getOtherAssets(table: table)
        }
    }
    //selection view taps
    @IBAction func mutualFundTapped(_ sender: Any) {
        if filters.numberOfSegments == 2 {
            filters.insertSegment(withTitle: "Folio", at: 2, animated: false)
            filters.setTitle("Scheme", forSegmentAt: 0)
        }
        filterViewHeight.constant = 80
        filterView.isHidden = false
        lastClicked = type
        type = 1
        mutualFund.textColor = Shared.sharedInstance.color
        shareBond.textColor = graycolor
        fixedDeposit.textColor = graycolor
        otherassert.textColor = graycolor
        network.getMutualFunds(table: table, groupid: groupid)
        slideView(d: 0)
    }
    
    @IBAction func shareBondTapped(_ sender: Any) {
        if filters.numberOfSegments == 3 {
            filters.removeSegment(at: 2, animated: false)
            filters.setTitle("Scrip", forSegmentAt: 0)
        }
        filterViewHeight.constant = 80
        filterView.isHidden = false
        lastClicked = type
        type = 2
        mutualFund.textColor = graycolor
        shareBond.textColor = Shared.sharedInstance.color
        fixedDeposit.textColor = graycolor
        otherassert.textColor = graycolor
        groupid = "1002"
        network.getSchemeBond(table: table, groupid: groupid)
        slideView(d: 1)
    }
    
    @IBAction func fixedDepositTapped(_ sender: Any) {
        filterViewHeight.constant = 0
        filterView.isHidden = true
        lastClicked = type
        type = 3
        mutualFund.textColor = graycolor
        shareBond.textColor = graycolor
        fixedDeposit.textColor = Shared.sharedInstance.color
        otherassert.textColor = graycolor
        network.getFixedDeposit(table: table)
        slideView(d: 2)
    }
    
    @IBAction func otherAssertTapped(_ sender: Any) {
        filterViewHeight.constant = 0
        filterView.isHidden = true
        lastClicked = type
        type = 4
        mutualFund.textColor = graycolor
        shareBond.textColor = graycolor
        fixedDeposit.textColor = graycolor
        otherassert.textColor = Shared.sharedInstance.color
        network.getOtherAssets(table: table)
        slideView(d: 3)
    }
    
    @IBAction func filterClicked(_ sender: Any) {
        if type == 1 {
            switch filters.selectedSegmentIndex {
            case 0:
                selectedFilter = 1
                groupid = "1002"
                network.getMutualFunds(table: table, groupid: groupid)
            case 1:
                selectedFilter = 2
                groupid = "100"
                network.getMutualFunds(table: table, groupid: groupid)
            default:
                groupid = "1005"
                network.getMutualFunds(table: table, groupid: groupid)
                selectedFilter = 3
                
            }
        }
        else {
            switch filters.selectedSegmentIndex {
            case 0:
                selectedFilter = 1
                groupid = "1002"
                network.getSchemeBond(table: table, groupid: groupid)
                
            default:
                selectedFilter = 2
                groupid = "100"
                network.getSchemeBond(table: table, groupid: groupid)
                
            }
        }
    }
}
extension PortfolioSummaryViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            if network.data?["result"]["data"].count == 0 {
                tableView.isHidden = true
                emptyStateView.isHidden = false
                return 0
            }
            else {
                tableView.isHidden = false
                emptyStateView.isHidden = true
                return 1
            }
        default:
            return network.data?["result"]["data"].count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: DashboardTableViewCell.self)
            cell?.backgroundImage.image = UIImage(named: "card_blank")
            cell?.marketChangeImg.isHidden = true
            cell?.MarketChange.isHidden = true
            cell?.loginView.isHidden = true
            cell?.detailsView.isHidden = false
            cell?.detailsView.backgroundColor = .clear
            cell?.nameofClient.text = ""
            cell?.market.text = "Current Value"
            if type == 1 || type == 2 {
                cell?.bottomhead3.text = "XIRR"
                cell?.bottomHead1.text = "Realized Gain"
                cell?.bottomhead2.text = "Unrealized Gain"
                cell?.marketValue.text = network.data?["result"]["summary"]["purchaseValue"].stringValue.toCurrencyFormat()
                cell?.bottomValue3.text = "\(network.data?["result"]["summary"]["XIRR"].stringValue.roundOffString() ?? "0")%"
                cell?.bottomValue1.text = network.data?["result"]["summary"]["realizedGain"].stringValue.toCurrencyFormat()
                cell?.bottomValue2.text = network.data?["result"]["summary"]["unrealizedGain"].stringValue.toCurrencyFormat()
            }
            else if type == 3 {
                cell?.bottomhead3.text = "Absolute Return"
                cell?.bottomHead1.text = "Purchase Value"
                cell?.bottomhead2.text = "Gain"
                cell?.marketValue.text = network.data?["result"]["summary"]["currentValue"].stringValue.toCurrencyFormat()
                cell?.bottomValue3.text = "\(network.data?["result"]["summary"]["absoluteReturn"].stringValue.roundOffString() ?? "0")%"
                cell?.bottomValue1.text = network.data?["result"]["summary"]["purchaseValue"].stringValue.toCurrencyFormat()
                cell?.bottomValue2.text = network.data?["result"]["summary"]["gain"].stringValue.toCurrencyFormat()
            }
            else if type == 4 {
                cell?.bottomhead3.text = "XIRR"
                cell?.bottomHead1.text = "Purchase Value"
                cell?.bottomhead2.text = "Gain"
                cell?.marketValue.text = network.data?["result"]["summary"]["currentValue"].stringValue.toCurrencyFormat()
                cell?.bottomValue3.text = "\(network.data?["result"]["summary"]["XIRR"].stringValue.roundOffString() ?? "0")%"
                cell?.bottomValue1.text = network.data?["result"]["summary"]["purchaseValue"].stringValue.toCurrencyFormat()
                cell?.bottomValue2.text = network.data?["result"]["summary"]["gain"].stringValue.toCurrencyFormat()
            }
            
            return cell ?? UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(with: PortfolioSummaryTableViewCell.self)
            if type == 1 {
                cell?.topLabel1value.text =  network.data?["result"]["data"][indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
                cell?.topLabel2value.text = "\(network.data?["result"]["data"][indexPath.row]["XIRR"].stringValue.roundOffString() ?? "0")%"
                cell?.bottomLabelValue.text = network.data?["result"]["data"][indexPath.row]["realizedGain"].stringValue.toCurrencyFormat()
                cell?.bottomLabel2value.text = network.data?["result"]["data"][indexPath.row]["unrealizedGain"].stringValue.toCurrencyFormat()
                cell?.toplabel1.text = "Current value"
                cell?.topLabel2.text = "XIRR"
                cell?.bottomlabel1.text = "Realized Gain"
                cell?.bottomLabel2.text = "Unrealized Gain"
                switch selectedFilter {
                case 1:
                    cell?.folioView.isHidden = true
                    cell?.folioViewHeight.constant = 0
                    cell?.headLabel.text = network.data?["result"]["data"][indexPath.row]["schemeName"].stringValue
                case 2:
                    cell?.folioView.isHidden = true
                    cell?.folioViewHeight.constant = 0
                    cell?.headLabel.text = network.data?["result"]["data"][indexPath.row]["applicantName"].stringValue
                default:
                    cell?.folioView.isHidden = false
                    cell?.folioViewHeight.constant = 65
                    
                    cell?.headLabel.text = network.data?["result"]["data"][indexPath.row]["applicantName"].stringValue
                    cell?.folioLabel1.text =  network.data?["result"]["data"][indexPath.row]["folioNo"].stringValue
                    cell?.folioLabel2.text = network.data?["result"]["data"][indexPath.row]["schemeName"].stringValue
                    
                }
            }
            else if type == 2 {
                cell?.topLabel1value.text =  network.data?["result"]["data"][indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
                cell?.topLabel2value.text = "\(network.data?["result"]["data"][indexPath.row]["XIRR"].stringValue.roundOffString() ?? "0")%"
                cell?.bottomLabelValue.text = network.data?["result"]["data"][indexPath.row]["realizedGain"].stringValue.toCurrencyFormat()
                cell?.bottomLabel2value.text = network.data?["result"]["data"][indexPath.row]["unrealizedGain"].stringValue.toCurrencyFormat()
                cell?.toplabel1.text = "Current value"
                cell?.topLabel2.text = "XIRR"
                cell?.bottomlabel1.text = "Realized Gain"
                cell?.bottomLabel2.text = "Unrealized Gain"
                switch selectedFilter {
                case 1:
                    cell?.folioView.isHidden = true
                    cell?.folioViewHeight.constant = 0
                    var sname = network.data?["result"]["data"][indexPath.row]["schemeName"].stringValue
                    if let range = sname?.range(of: "(") {
                        sname = sname?[..<range.lowerBound].trimmingCharacters(in: .whitespaces)
                    }
                    cell?.headLabel.text = sname ?? ""
                default:
                    cell?.folioView.isHidden = true
                    cell?.folioViewHeight.constant = 0
                    cell?.headLabel.text = network.data?["result"]["data"][indexPath.row]["applicantName"].stringValue
                    
                }
            }
            else if type == 3 {
                cell?.folioView.isHidden = true
                cell?.folioViewHeight.constant = 0
                cell?.topLabel1value.text =  network.data?["result"]["data"][indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
                cell?.topLabel2value.text = "\(network.data?["result"]["data"][indexPath.row]["absoluteReturn"].stringValue.roundOffString() ?? "0")%"
                cell?.bottomLabelValue.text = network.data?["result"]["data"][indexPath.row]["purchaseValue"].stringValue.toCurrencyFormat()
                cell?.bottomLabel2value.text = network.data?["result"]["data"][indexPath.row]["gain"].stringValue.toCurrencyFormat()
                cell?.toplabel1.text = "Current value"
                cell?.topLabel2.text = "Absolute Return"
                cell?.bottomlabel1.text = "Purchase Value"
                cell?.bottomLabel2.text = "Gain"
                cell?.headLabel.text = network.data?["result"]["data"][indexPath.row]["scheme"].stringValue
            }
            else if type == 4 {
                cell?.folioView.isHidden = true
                cell?.folioViewHeight.constant = 0
                cell?.topLabel1value.text =  network.data?["result"]["data"][indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
                cell?.topLabel2value.text = "\(network.data?["result"]["data"][indexPath.row]["XIRR"].stringValue.roundOffString() ?? "0")%"
                cell?.bottomLabelValue.text = network.data?["result"]["data"][indexPath.row]["purchaseValue"].stringValue.toCurrencyFormat()
                cell?.bottomLabel2value.text = network.data?["result"]["data"][indexPath.row]["gain"].stringValue.toCurrencyFormat()
                cell?.toplabel1.text = "Current value"
                cell?.topLabel2.text = "XIRR"
                cell?.bottomlabel1.text = "Purchase Value"
                cell?.bottomLabel2.text = "Gain"
                cell?.headLabel.text = network.data?["result"]["data"][indexPath.row]["assetName"].stringValue
            }
            return cell ?? UITableViewCell()
        }
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
}
extension PortfolioSummaryViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            
        }
    }
}
extension PortfolioSummaryViewController {
    func slideView(d:Int) {
        
        if lastClicked == type {
            viewLeading.constant = CGFloat(Int(mutualFund.bounds.width) * d)
        }
        else if lastClicked > type {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromLeft
            self.floatingView.layer.add(transition, forKey: "kCATransition")
            viewLeading.constant = CGFloat(Int(mutualFund.bounds.width) * d)
        }
        else if lastClicked < type {
            let transition = CATransition()
            transition.type = CATransitionType.push
            transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
            transition.fillMode = CAMediaTimingFillMode.forwards
            transition.duration = 0.5
            transition.subtype = CATransitionSubtype.fromRight
            self.floatingView.layer.add(transition, forKey: "kCATransition")
            viewLeading.constant = CGFloat(Int(mutualFund.bounds.width) * d)
        }
    }
}
