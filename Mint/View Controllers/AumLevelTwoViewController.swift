//
//  AumLevelTwoViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 30/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//
//


import UIKit
import SwiftyJSON
import NVActivityIndicatorView
class AumLevelTwoViewController: UIViewController , Completion {
  
    @IBOutlet weak var name: UILabel!
    var delegates:dismissing?
    var network = Aum()
    var fundis:String = ""
    var sname:String = ""
    var category:String = ""
    @IBOutlet weak var activityView: UIView!
      @IBOutlet weak var activity: NVActivityIndicatorView!
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        activityView.isHidden = false
               activity.startAnimating()
         table.tableFooterView = UIView()
        table.register(cell: PortfolioScheme1TableViewCell.self)
        table.register(cell: AumTableViewCell.self)
        navigationController?.navigationBar.isHidden = false
               navigationController?.navigationBar.prefersLargeTitles = false
               self.title = "AUM Report"
              self.navigationItem.hidesBackButton = true
              let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
              self.navigationItem.leftBarButtonItem = newBackButton
        network.delegate = self
        network.network2(table: table , id: fundis)
        name.text = sname
          }
    
   @objc func back(sender: UIBarButtonItem) {
    navigationController?.popViewController(animated: true)
          }
    
    func complete(data: JSON?) {
            activityView.isHidden = true
               activity.stopAnimating()
        print(data)
      }
      
      
    
}
extension AumLevelTwoViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0 :
            return 1
        default:
            return network.data1?["result"]["data"].count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: PortfolioScheme1TableViewCell.self)
            cell?.headLabel2.isHidden = true
            cell?.headValue2.isHidden = true
            cell?.bottomLabel3.text = ""
            cell?.bottomvalue3.text = ""
            cell?.headLabel1.text = "Total"
            cell?.midLabe1.text = "Debt"
            cell?.misLabel2.text = "Equity"
            cell?.midLabel3.text = "Other"
            cell?.bottomLabel1.text = "Liquied & Ultra short"
            cell?.bottomLabel2.text = "Arbitrage"
            cell?.headValue1.text = network.data1?["result"]["summary"]["AUM"].stringValue.toCurrencyFormat()
            cell?.midvalue1.text = network.data1?["result"]["summary"]["Debt"].stringValue.toCurrencyFormat()
            cell?.midValue2.text = network.data1?["result"]["summary"]["Equity"].stringValue.toCurrencyFormat()
            cell?.midValue3.text = network.data1?["result"]["summary"]["Other"].stringValue.toCurrencyFormat()
            cell?.bottomValue1.text = network.data1?["result"]["summary"]["Liquid and Ultra Short"].stringValue.toCurrencyFormat()
            cell?.bottomValue2.text = network.data1?["result"]["summary"]["Arbitrage"].stringValue.toCurrencyFormat()
            return cell ?? UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(with: AumTableViewCell.self)
            cell?.bottomLabel.text = "Amount"
            cell?.bottomLabel2.text = "Allocation"
            cell?.bottomvalue.text = network.data1?["result"]["data"][indexPath.row]["AUM"].stringValue.toCurrencyFormat()
            cell?.bottomvalue2.text = "\(network.data1?["result"]["data"][indexPath.row]["allocation"].stringValue.roundOffString() ?? "")%"
              cell?.name.text = network.data1?["result"]["data"][indexPath.row]["name"].stringValue
            return cell ?? UITableViewCell()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
}
extension AumLevelTwoViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scid = network.data1?["result"]["data"][indexPath.row]["schid"].stringValue
          let vc: AumLevelThreeViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "AumLevelThreeViewController") as! AumLevelThreeViewController
        vc.name = network.data1?["result"]["data"][indexPath.row]["name"].stringValue ?? ""
        vc.schid = scid ?? ""
        vc.fundis = fundis
        if network.data1?["result"]["data"][indexPath.row]["Equity"].stringValue != "0" {
            category = "Equity"
        }
        else if network.data1?["result"]["data"][indexPath.row]["Debt"].stringValue != "0" {
            category = "Debt"
        }
        else if network.data1?["result"]["data"][indexPath.row]["Arbitrage"].stringValue != "0" {
                   category = "Arbitrage"
               }
        else if network.data1?["result"]["data"][indexPath.row]["Liquid and Ultra Short"].stringValue != "0" {
                         category = "Liquid and Ultra Short"
                     }
        else if network.data1?["result"]["data"][indexPath.row]["Other"].stringValue != "0" {
                                category = "Other"
                            }
        vc.category = category
        navigationController?.pushViewController(vc, animated: true)
    }
}



