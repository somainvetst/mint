//
//  SearchClientViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 26/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class SearchClientViewController: UIViewController , clientSearch  {
  
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var headerView: ColorHeader!
     @IBOutlet weak var searchtf: UITextField!
    var type:String = ""
    var name:String = ""
    var network = SearchClientNetwork()
    override func viewDidLoad() {
        super.viewDidLoad()
       searchView.isHidden = true
       navigationController?.navigationBar.isHidden = false
        table.register(cell: SearchResultTableViewCell.self)
        table.register(cell: ClientSearchTableViewCell.self)
        network.network(table: table, name: name)
    }
    
   
    func logout() {
        Shared.sharedInstance.loggingout(nav: navigationController!)
    }
    func search(name: String, type: String) {
        self.type = type
        network.network(table: table, name: name)
    }
    @IBAction func searching(_ sender: Any) {
        guard let search = searchtf.text else {
            return
        }
        name = search
        network.network(table: table, name: name)
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func backFromSearch(_ sender: Any) {
        viewSlideInFromRightToLeft(view: searchView)
        searchView.isHidden = true
        headerView.isHidden = false
    }
    @IBAction func openSearchView(_ sender: Any) {
        viewSlideInFromTopToBottom(view: headerView)
        
        headerView.isHidden = true
        searchView.alpha = 0
        searchView.isHidden = false
        UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseInOut, animations: {
            self.searchView.alpha = 1.0
        }) { (isCompleted) in
        }
    }
    fileprivate func viewSlideInFromTopToBottom(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        view.layer.add(transition, forKey: kCATransition)
    }
    fileprivate func viewSlideInFromRightToLeft(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        view.layer.add(transition, forKey: kCATransition)
    }
}
extension SearchClientViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 0
        default:
            return network.data?["result"]["data"].count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: ClientSearchTableViewCell.self)
          
            cell?.type = type
            cell?.delegate = self
            
            return cell ?? UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(with: SearchResultTableViewCell.self)
            cell?.name.text = network.data?["result"]["data"][indexPath.row]["name"].stringValue
            cell?.pan.text =  "PAN"
            var panVal = network.data?["result"]["data"][indexPath.row]["PAN"].stringValue
            if panVal == "" {
                panVal = "Not Available"
            }
            cell?.panvalue.text = panVal ?? "Not Available"
            cell?.phone.text = "Phone"
            var phoneVal = network.data?["result"]["data"][indexPath.row]["mobile"].stringValue
            if phoneVal == "" {
                phoneVal = "Not Available"
            }
            cell?.phoneValue.text = phoneVal ?? "Not Available"
            cell?.firstLabel.text = "Name"
            cell?.firstRightLabel.text = "Family Head"
            var types = network.data?["result"]["data"][indexPath.row]["familyHead"].stringValue
            if types == "" {
                types = "Not Available"
            }
            else if types == "self" {
                 types = "Self"
            }
            type = types ?? ""
            cell?.type.text = types
            return cell ?? UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Shared.sharedInstance.typeId = network.data?["result"]["data"][indexPath.row]["uid"].stringValue ?? ""
        Shared.sharedInstance.clientName = network.data?["result"]["data"][indexPath.row]["name"].stringValue ?? ""
        let types = network.data?["result"]["data"][indexPath.row]["familyHead"].stringValue
        Shared.sharedInstance.typeAppid = network.data?["result"]["data"][indexPath.row]["appid"].stringValue ?? ""
        if types == "self" {
            Shared.sharedInstance.typeLevel = "98"
            Shared.sharedInstance.clientType = "familyHead"
        }
        else {
            Shared.sharedInstance.typeLevel = "100"
           
            Shared.sharedInstance.clientType = "client"
        }
        Routes.shared.gotoTab(nav: navigationController!)
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            
        }
    }
}
