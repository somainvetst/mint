//
//  PurchaseViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 07/11/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class PurchaseViewController: UIViewController {

    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        table.register(cell: TransactionHeaderType2TableViewCell.self)
       table.tableFooterView = UIView()
                                   navigationController?.navigationBar.isHidden = false
                                   navigationController?.navigationBar.prefersLargeTitles = false
                                   self.title = "Purchase"
                                   self.navigationItem.hidesBackButton = true
                                   let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                                   self.navigationItem.leftBarButtonItem = newBackButton
    }
    

    @objc func back(sender: UIBarButtonItem) {
      navigationController?.popViewController(animated: true)
    }

}
extension PurchaseViewController : UITableViewDelegate {}
extension PurchaseViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0 :
            let cell = tableView.dequeueReusableCell(with: TransactionHeaderType2TableViewCell.self)
            cell?.schemeName.text = Transactiondata.shared.schemename
            cell?.name.text = Transactiondata.shared.name
            cell?.ucc.text = "UCC"
            cell?.folio.text = "Folio"
            cell?.uccvalue.text = Transactiondata.shared.ucc
            cell?.folioValue.text = Transactiondata.shared.folioNum
            return cell ?? UITableViewCell()
        default:
             return UITableViewCell()
        }
       
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
