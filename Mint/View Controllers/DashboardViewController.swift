//
//  DashboardViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
final class DashboardViewController: UIViewController , UITabBarControllerDelegate , SearchClient , timetap {
   
    @IBOutlet weak var backBtn: UIButton!
    var login : [String:AnyObject] = [:]
    var level:Int = 0
    @IBOutlet weak var table: UITableView!
    lazy var brokerdata = GetBrokerDashboard()
    let clientData = GetNetwork()
    lazy var clientAllocate = GetAllocationDetail()
    let portfolio = GetFamilyPortfolioDetails()
    var time = "thisMonth"
    lazy var clientName:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.parent?.title = ""
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.topItem?.title = ""
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        
         let image = Shared.sharedInstance.logo
        imageView.image = image
        
        navigationItem.titleView = imageView
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        if Shared.sharedInstance.clientType == "" && Shared.sharedInstance.id <= 10 {
            newBackButton.title = ""
            
        }
        else if Shared.sharedInstance.clientType != ""  {
            
            let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
            self.parent?.navigationItem.leftBarButtonItem = newBackButton
            
        }
        else {
            newBackButton.title = ""
        }
        self.tabBarController?.delegate =  UIApplication.shared.delegate as? UITabBarControllerDelegate
        CoreData.core.fetchCartData(entity: "Logindata")
        table.register(cell: DashboardTableViewCell.self)
        table.register(cell: InvestmentRouteTableViewCell.self)
        table.register(cell: CalculatorsTableViewCell.self)
        table.register(cell: BrokerSearchPanelTableViewCell.self)
        table.register(cell: PieChartTableViewCell.self)
        table.register(cell: LineChartTableViewCell.self)
        if CoreData.core.data.isEmpty == false {
            
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                return
            }
            self.login = login
            guard  let id =  login["result"]?["uid"] as? String else {
                
                return
            }
            
            guard  let level =  login["result"]?["levelNo"] as? Int else {
                
                return
            }
            guard  let domain =  login["result"]?["brokerDomain"] as? String else {
                
                return
            }
            guard  let levelid =  login["result"]?["levelid"] as? String else {
                
                return
            }
            guard  let bid =  login["result"]?["bid"] as? String else {
                
                return
            }
            self.level = level
            Shared.sharedInstance.bid = bid
            Shared.sharedInstance.levelidMain = levelid
            Shared.sharedInstance.brokerDomain = domain
            switch level {
            case 1...10 :
                
                brokerdata.network(table: table)
                if Shared.sharedInstance.clientType != "" {
                    clientData.network(table: table, id: Shared.sharedInstance.typeId )
                    clientAllocate.network(table: table , id: Shared.sharedInstance.typeId , level: Shared.sharedInstance.typeLevel )
                     clientAllocate.lineChart(table: table , id: Shared.sharedInstance.typeId , level: Shared.sharedInstance.typeLevel , time: time )
                }
                
            default :
                //   portfolio.network(table: UITableView() , id: id)
                clientData.network(table: table, id: id )
                clientAllocate.network(table: table, id: "", level: "")
            }
        }
        print(CoreData.core.data.count)
    }
   
       
    @IBAction func openOtp(_ sender: Any) {
       
    }
    @objc func back(sender: UIBarButtonItem) {
        
        Shared.sharedInstance.clientType = ""
        Shared.sharedInstance.typeLevel = ""
        Shared.sharedInstance.typeId = ""
        Routes.shared.gotoTab(nav: navigationController!)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.topItem?.title = ""
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.tabBarController?.navigationController?.navigationBar.topItem?.title = ""
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
           imageView.contentMode = .scaleAspectFit
           
        let image = Shared.sharedInstance.logo
           imageView.image = image
           
        self.tabBarController?.navigationItem.titleView = imageView
        //        if Shared.sharedInstance.clientType == "" && Shared.sharedInstance.id <= 10 {
        //                         newBackButton.title = ""
        //                       navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        //                           self.tabBarController?.navigationItem.hidesBackButton = true
        //                     }
        //                     else {
        //                           newBackButton.title = "Back"
        //
        //                     }
        if CoreData.core.data.isEmpty == false {
            
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                return
            }
            self.login = login
            guard  let id =  login["result"]?["uid"] as? String else {
                
                return
            }
            guard  let level =  login["result"]?["levelNo"] as? Int else {
                
                return
            }
            Shared.sharedInstance.id = level
            Shared.sharedInstance.name = login["result"]?["name"] as? String ?? ""
            switch level {
            case 1...10 :
                brokerdata.network(table: table)
                if Shared.sharedInstance.clientType != "" {
                    // clientData.network(table: table, id: Shared.sharedInstance.typeId )
                    // clientAllocate.network(table: table, id: Shared.sharedInstance.typeId, level: Shared.sharedInstance.typeLevel)
                }
            default :
                print("test")
                // clientData.network(table: table, id: id )
                //clientAllocate.network(table: table, id: "", level: "")
            }
        }
    }
    @IBAction func back(_ sender: Any) {
        
    }
    //actions
    @objc func goToLogin(_sender : UIButton) {
        Routes.shared.gotoLogin(nav: navigationController!, id: 1)
    }
    func logout() {
        Shared.sharedInstance.loggingout(nav: navigationController!)
    }
    
    func searchTapped(name: String, type: String) {
        //        let vc = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "SearchClientViewController") as! SearchClientViewController
        //        vc.name = name
        //        vc.type = type
        //        print(type , "vhec")
        //        navigationController?.pushViewController(vc, animated: true)
        let vc = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "ClientSearchViewController") as! ClientSearchViewController
        vc.name = name
        vc.type = type
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func getUserName() {
        
        let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
        if Shared.sharedInstance.clientType == "" {
            let url = "\(Shared.sharedInstance.baseUrl)auth/getLoggedInUser?selectedUser="
            NetworkingHelper.shared.getRequest(url: url, method: HTTPMethods.get.rawValue, completion: { (data : (LoggedInUserDetails)) in
                
            }) { (error) in
                print(error)
                
            }
        }
        else {
            do {
                let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                let url = "\(Shared.sharedInstance.baseUrl)auth/getLoggedInUser?selectedUser=\(urlEncodedJson!)"
                NetworkingHelper.shared.getRequest(url: url, method: HTTPMethods.get.rawValue , completion: { (customer: [LoggedInUserDetails]) in
                    print(data)
                }) { (error) in
                    print(error)
                    print([LoggedInUserDetails].self)
                }
            }
            catch {}
        }
        
    }
    func tap(data: String) {
        if data == "This Month" {
            time = "thisMonth"
        }
        else  if data == "Last 1 month" {
                   time = "lastOneMonth"
               }
        else  if data == "Current FY" {
                   time = "currentFY"
               }
        else  if data == "Last FY" {
                   time = "lastFY"
               }
        else  if data == "Last 1 Year" {
                   time = "lastOneYear"
               }
        else  if data == "Since Inception" {
                          time = "sinceInception"
                      }
        clientAllocate.lineChart(table: table , id: Shared.sharedInstance.typeId , level: Shared.sharedInstance.typeLevel, time: time )
       }
       
      
}
extension DashboardViewController:UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if CoreData.core.data.isEmpty == true  {
            switch indexPath.section {
            case 0 :
                let cell = tableView.dequeueReusableCell(with: DashboardTableViewCell.self)
                cell?.detailsView.isHidden = true
                
                cell?.backgroundImage.image = UIImage(named: "card")
                cell?.loginButton.addTarget(self, action: #selector(self.goToLogin(_sender:)), for: .touchUpInside)
                return cell ?? UITableViewCell()
            case 1 :
                let cell = tableView.dequeueReusableCell(with: InvestmentRouteTableViewCell.self)
                cell?.heading.text = "Top Schemes"
                cell?.details.text = "As per scheme performance in last 5 years. We have listed top 20 schemes with star rating. Investing in Top Performer is a good idea"
                cell?.investImage.image = UIImage(named: "pick")
                return cell ?? UITableViewCell()
            case 2 :
                let cell = tableView.dequeueReusableCell(with: CalculatorsTableViewCell.self)
                return cell ?? UITableViewCell()
            default:
                print(" ")
            }
        }
        else {
            
            guard  var level =  login["result"]?["levelNo"] as? Int else {
                
                return UITableViewCell()
            }
            if Shared.sharedInstance.clientType == "familyHead" {
                level = 98
                Shared.sharedInstance.name = Shared.sharedInstance.clientName
            }
            else  if Shared.sharedInstance.clientType == "client" ||  Shared.sharedInstance.clientType == "investor" {
                level = 100
                Shared.sharedInstance.name = Shared.sharedInstance.clientName
            }
            switch level {
            case 1...10 :
                switch indexPath.section {
                case 0 :
                    let cell = tableView.dequeueReusableCell(with: DashboardTableViewCell.self)
                    cell?.backgroundImage.image = UIImage(named: "card_blank")
                    cell?.loginView.isHidden = true
                    cell?.detailsView.isHidden = false
                    cell?.detailsView.backgroundColor = .clear
                    
                    guard let aumVal1 = brokerdata.data?["result"]["AUMEquity"].stringValue else {return UITableViewCell()}
                    guard let aumVal2 = brokerdata.data?["result"]["AUMNonEquity"].stringValue else {return UITableViewCell()}
                    guard let aumdoub1 = Double(aumVal1) else {return UITableViewCell()}
                    guard let aumdoub2 = Double(aumVal2) else {return UITableViewCell()}
                    var totalAUM = aumdoub1  + aumdoub2
                    totalAUM = totalAUM.roundtoPlace(places: 2)
                    cell?.marketChangeImg.isHidden = true
                    cell?.MarketChange.isHidden = true
                    cell?.bottomHead1.text = "No. of Clients"
                    cell?.bottomhead2.text = "No. of SIP"
                    cell?.bottomhead3.text = "SIP per month"
                    cell?.bottomValue1.text = brokerdata.data?["result"]["noOfInvestors"].stringValue
                    cell?.bottomValue2.text = brokerdata.data?["result"]["noOfSIP"].stringValue
                    cell?.bottomValue3.text = brokerdata.data?["result"]["amountOfSIPPerMonth"].stringValue.toCurrencyFormat()
                    cell?.market.text = "AUM"
                    cell?.marketValue.text = String(totalAUM).toCurrencyFormat()
                    cell?.nameofClient.text = login["result"]?["name"] as? String ?? ""
                    return cell ?? UITableViewCell()
                case 1 :
                    
//                   let cell = tableView.dequeueReusableCell(with: BrokerSearchPanelTableViewCell.self)
////                    cell?.delegate = self
////                    cell?.headView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
////                    cell?.headView.clipsToBounds = true
////                    cell?.headView.layer.cornerRadius = 10
////                    //cell?.familyHeadImage.image = UIImage(named: "filled")
////                    // cell?.investorImage.image = UIImage(named: "notfilled")
//                   cell?.hideView()
//                  return cell ?? UITableViewCell()
                    return UITableViewCell()
                    
                case 2 :
                    let cell = tableView.dequeueReusableCell(with: InvestmentRouteTableViewCell.self)
                    cell?.heading.text = "Top Schemes"
                    cell?.details.text = "As per scheme performance in last 5 years. We have listed top 20 schemes with star rating. Investing in Top Performer is a good idea"
                    cell?.investImage.image = UIImage(named: "pick")
                    return cell ?? UITableViewCell()
                default :
                    let cell = tableView.dequeueReusableCell(with: CalculatorsTableViewCell.self)
                    return cell ?? UITableViewCell()
                    
                }
            default :
                
                switch indexPath.section {
                case 0 :
                    
                    let cell = tableView.dequeueReusableCell(with: DashboardTableViewCell.self)
                    //   cell?.backgroundImage.image = UIImage(named: "card_blank")
                    cell?.loginView.isHidden = true
                    cell?.detailsView.isHidden = false
                    cell?.detailsView.backgroundColor = .clear
                    cell?.marketChangeImg.isHidden = false
                    cell?.MarketChange.isHidden = false
                    cell?.detailsView.backgroundColor = .clear
                    cell?.bottomHead1.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                    cell?.bottomhead2.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                    cell?.bottomhead3.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                    cell?.bottomValue1.textColor = #colorLiteral(red: 0.5107896924, green: 0.5111948848, blue: 0.5108524561, alpha: 1)
                    cell?.bottomValue2.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                    cell?.bottomValue3.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                    cell?.nameofClient.textColor = #colorLiteral(red: 0.2196078431, green: 0.4235294118, blue: 0.8588235294, alpha: 1)
                    cell?.market.textColor = #colorLiteral(red: 0.5176470588, green: 0.5176470588, blue: 0.5176470588, alpha: 1)
                    cell?.marketValue.textColor = #colorLiteral(red: 0.3176470588, green: 0.4705882353, blue: 0.6901960784, alpha: 1)
                    cell?.bottomHead1.text = "Purchase Cost"
                    cell?.bottomhead2.text = "Gain"
                    cell?.bottomhead3.text = "Absolute Return"
                    let oneDayChange =  clientData.data?["result"]["oneDayChange"].stringValue.toCurrencyFormat()
                    if oneDayChange?.hasPrefix("-") ?? false {
                        cell?.MarketChange.textColor = .red
                        cell?.marketChangeImg.image = #imageLiteral(resourceName: "down")
                    }
                    else {
                        cell?.MarketChange.textColor = #colorLiteral(red: 0.2509803922, green: 0.6392156863, blue: 0.1176470588, alpha: 1)
                        cell?.marketChangeImg.image = #imageLiteral(resourceName: "up")
                    }
                    
                    cell?.MarketChange.text = "(\(oneDayChange ?? ""))"
                    cell?.bottomValue1.text = clientData.data?["result"]["purchaseValue"].stringValue.toCurrencyFormat()
                    cell?.bottomValue2.text = clientData.data?["result"]["gain"].stringValue.toCurrencyFormat()
                    guard let absValue = clientData.data?["result"]["returnPercentage"].stringValue else {return UITableViewCell()}
                    guard let dbValueAbs = Double(absValue)?.roundtoPlace(places: 2) else {return UITableViewCell()}
                    cell?.bottomValue3.text = "\(String(dbValueAbs))%"
                    cell?.market.text = "Market Value"
                    cell?.nameofClient.text = Shared.sharedInstance.name
                    
                    cell?.marketValue.text = clientData.data?["result"]["currentValue"].stringValue.toCurrencyFormat()
                    return cell ?? UITableViewCell()
                case 1 :
                    let cell = tableView.dequeueReusableCell(with: PieChartTableViewCell.self)
                    guard let  values = clientAllocate.data?["result"]["data"].arrayValue.map(
                        { $0["currentAmount"].doubleValue }) else { return UITableViewCell() }
                    guard let  labels = clientAllocate.data?["result"]["data"].arrayValue.map(
                        { $0["category"].stringValue }) else { return UITableViewCell() }
                    cell?.setChart(dataPoints: labels , values: values)
                    cell?.nameofChart.text = "Asset Allocation by Category"
                    return cell ?? UITableViewCell()
                case 2 :
                    let cell = tableView.dequeueReusableCell(with: InvestmentRouteTableViewCell.self)
                    cell?.heading.text = "Top Schemes"
                    cell?.details.text = "As per scheme performance in last 5 years. We have listed top 20 schemes with star rating. Investing in Top Performer is a good idea"
                    cell?.investImage.image = UIImage(named: "pick")
                    return cell ?? UITableViewCell()
                case 3:
//                    let cell = tableView.dequeueReusableCell(with: LineChartTableViewCell.self)
//                    cell?.delegate = self
//                    cell?.timePeriod.text = time
//                    guard let  values = clientAllocate.data1?["result"]["data"].arrayValue.map(
//                        { $0["AUM"].doubleValue.roundtoPlace(places: 2) }) else { return UITableViewCell() }
//                    guard let  values1 = clientAllocate.data1?["result"]["data"].arrayValue.map(
//                        { $0["netInvestment"].doubleValue.roundtoPlace(places: 2) }) else { return UITableViewCell() }
//                                      guard let  labels = clientAllocate.data1?["result"]["data"].arrayValue.map(
//                                          { $0["date"].stringValue }) else { return UITableViewCell() }
//
//                    cell?.setChart(dataPoints: labels , values1: values, values2: values1)
//                                      cell?.name.text = "AUM Graph"
//                     return cell ?? UITableViewCell()
                    return UITableViewCell()
                   
                default :
                    let cell = tableView.dequeueReusableCell(with: CalculatorsTableViewCell.self)
                    cell?.nav = navigationController!
                    return cell ?? UITableViewCell()
                    
                }
            }
            
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if CoreData.core.data.isEmpty == false  {
            guard  var level =  login["result"]?["levelNo"] as? Int else {
                
                return
            }
            switch level {
            case 1...10 :
                switch indexPath.section {
                case 2 :
                    print("j")
                    let vc: WebViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                    vc.heading = "Top Schemes"
                    vc.urlLink = "https://m.investwell.in/hc/newtpsch.jsp?bid=30465&user=demo1234&password=123456/"
                    navigationController?.pushViewController(vc, animated: true)
                default:
                    return
                }
            default:
                switch indexPath.section {
                case 2:
                    let vc: WebViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                    vc.heading = "Top Schemes"
                    vc.urlLink = "https://m.investwell.in/hc/newtpsch.jsp?bid=30465&user=demo1234&password=123456/"
                    navigationController?.pushViewController(vc, animated: true)
                default: return
                }
            }
        }
        else {
            switch indexPath.section {
            case 1:
                let vc: WebViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                vc.heading = "Top Schemes"
                vc.urlLink = "https://m.investwell.in/hc/newtpsch.jsp?bid=30465&user=demo1234&password=123456/"
                navigationController?.pushViewController(vc, animated: true)
            default: return
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if CoreData.core.data.isEmpty == true  {
            return 3
        }
        else if Shared.sharedInstance.clientType.isEmpty == false || self.level > 10 {
            return 5
        }
        else {
            return 4
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if CoreData.core.data.isEmpty == true {
            switch section {
            case 2 :
                return ""
            default:
                return ""
            }
        }
        else {
            switch section {
            case 3 :
                return ""
            default:
                return ""
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            headerView.textLabel?.textColor = .black
        }
    }
}

