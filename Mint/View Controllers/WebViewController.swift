//
//  WebViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 05/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import WebKit
import NVActivityIndicatorView
class WebViewController: UIViewController  , WKNavigationDelegate {
   
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activity: NVActivityIndicatorView!
    @IBOutlet weak var webV: WKWebView!
    var heading:String = ""
    var urlLink:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationController?.navigationBar.isHidden = false
                       navigationController?.navigationBar.prefersLargeTitles = false
                       self.title = heading
                         self.navigationController?.navigationBar.tintColor = UIColor.black
        
        activityView.isHidden = false
        activity.startAnimating()
       
        let webView = WKWebView(frame: CGRect(x: view.frame.maxX, y: view.frame.maxY, width: self.view.frame.size.width, height: self.view.frame.size.height))
        self.view.addSubview(webView)
        let url = URL(string: urlLink)
        webV.navigationDelegate = self
        webV.load(URLRequest(url: url!))
        
      
       
      self.navigationItem.hidesBackButton = true
                          let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                                       self.navigationItem.leftBarButtonItem = newBackButton
                                   }

                      @objc func back(sender: UIBarButtonItem) {
                          
                         navigationController?.popViewController(animated: true)
                                   }
    override func viewWillAppear(_ animated: Bool) {
       
    }
func webView(_ webView: WKWebView,didFinish navigation: WKNavigation!) {
    activityView.isHidden = true
    activity.stopAnimating()
    
    }
   
    


}
