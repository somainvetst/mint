//
//  MenuViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 21/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
protocol Menu:class {
    func closeMenu()
}
class MenuViewController: UIViewController , dismissing {
    
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var heightOfView: NSLayoutConstraint!
    //@IBOutlet weak var homeButton: UIButton!
    var delegate:Menu?
    var toolImage:[UIImage] = [UIImage(named: "investment")! , UIImage(named: "my_journey")! , UIImage(named: "tax_saving")! , UIImage(named: "nav_lookup")! , UIImage(named: "insurance")! , UIImage(named: "transfer_holdings")!]
    var toolName:[String] = ["Folio Lookup" , "Capital Gain Realized" , "Portfolio Summary" , "Capital Gain Unrealized" , "Insurance"]
    var toolName1:[String] = ["Folio Lookup" , "Transactions" , "Insurance"]
    var toolImage1:[UIImage] = [UIImage(named: "investment")! , UIImage(named: "my_journey")! , UIImage(named: "insurance")!]
    @IBOutlet weak var collectionView: UICollectionView!
    var login : [String:AnyObject] = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        let vc: FolioLookupViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "FolioLookupViewController") as! FolioLookupViewController;
        vc.delegates = self
        self.navigationController?.navigationBar.isHidden = true
        CoreData.core.fetchCartData(entity: "Logindata")
        collectionView.register(cell: CalculatorsCollectionViewCell.self)
        heightOfView.constant = 0
        levelChecker()
        self.tabBarController?.navigationController?.isToolbarHidden = true
        if Shared.sharedInstance.clientType == "" {
            
            //homeButton.isHidden = true
        }
        else {
            // homeButton.isHidden = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let vc: FolioLookupViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "FolioLookupViewController") as! FolioLookupViewController;
        vc.delegates = self
        self.tabBarController?.navigationController?.isToolbarHidden = true
        self.navigationController?.navigationBar.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        //
        
        
        
    }
    @IBAction func closeMenu(_ sender: Any) {
        
        dismiss(animated: true, completion: nil)
        
    }
    func close() {
        print("here")
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func home(_ sender: Any) {
        Shared.sharedInstance.clientType = ""
        Shared.sharedInstance.typeLevel = ""
        Shared.sharedInstance.typeId = ""
    }
    func levelChecker() {
        if CoreData.core.data.isEmpty == false {
            
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                return
            }
            self.login = login
            
        }
    }
}
extension MenuViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     
        guard  let level =  login["result"]?["levelNo"] as? Int else {
            
            return 0
        }
        
        switch level {
        case 1...10 :
            if Shared.sharedInstance.clientType == "" {
                return toolName1.count
            }
            else {
                return toolName.count
            }
        default :
            
            return toolName.count
            
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalculatorsCollectionViewCell", for: indexPath) as! CalculatorsCollectionViewCell
           collectionHeight.constant = collectionView.contentSize.height
        guard  let level =  login["result"]?["levelNo"] as? Int else {
            
            return UICollectionViewCell()
        }
        
        switch level {
        case 1...10 :
            if Shared.sharedInstance.clientType == "" {
                cell.calculatorImage.image = toolImage1[indexPath.row]
                cell.calculatorName.text = toolName1[indexPath.row]
                cell.horizontalView.isHidden = true
                cell.verticalView.isHidden = true
            }
            else {
                cell.calculatorImage.image = toolImage[indexPath.row]
                cell.calculatorName.text = toolName[indexPath.row]
                cell.horizontalView.isHidden = true
                cell.verticalView.isHidden = true
            }
        default :
            
            cell.calculatorImage.image = toolImage[indexPath.row]
            cell.calculatorName.text = toolName[indexPath.row]
            cell.horizontalView.isHidden = true
            cell.verticalView.isHidden = true
            
        }
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size.width / 3.0
        return CGSize(width:size - 20  , height: size - 20 )
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if Shared.sharedInstance.clientType == "" && Shared.sharedInstance.id > 10 {
            switch indexPath.item {
            case 0:
                
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "FolioLookupViewController"))
                 ( vc.topViewController as? FolioLookupViewController)?.delegates = self
                vc.modalPresentationStyle = .overFullScreen
                self.present(vc, animated: true, completion: nil)
                // Routes.shared.gotoFolio(nav: self.navigationController!)
                
            case 1:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "CapitalGainViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? CapitalGainViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
            case 2:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioSummaryViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? PortfolioSummaryViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
            case 3:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "CapitalGainRealizedViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? CapitalGainRealizedViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
                
            default:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "InsuranceViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? InsuranceViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
                
            }
        }
        else if Shared.sharedInstance.clientType == "" && Shared.sharedInstance.id <= 10 {
            switch indexPath.item {
            case 0:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "FolioLookupViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? FolioLookupViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
            case 1:
                let tranvc =  UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "TransactionsViewController"))
                tranvc.modalPresentationStyle = .overFullScreen
                 ( tranvc.topViewController as? TransactionsViewController)?.delegates = self
                self.present(tranvc, animated: true, completion: nil)
            case 2:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "InsuranceViewController"))
                               vc.modalPresentationStyle = .overFullScreen
                                ( vc.topViewController as? InsuranceViewController)?.delegates = self
                               self.present(vc, animated: true, completion: nil)
               
            default:
              let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "InsuranceViewController"))
                                             vc.modalPresentationStyle = .overFullScreen
                                              ( vc.topViewController as? InsuranceViewController)?.delegates = self
                                             self.present(vc, animated: true, completion: nil)
            }
        }
        else {
            switch indexPath.item {
            case 0:
                
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "FolioLookupViewController"))
                vc.modalPresentationStyle = .overFullScreen
                ( vc.topViewController as? FolioLookupViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
                // Routes.shared.gotoFolio(nav: self.navigationController!)
                
            case 1:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "CapitalGainViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? CapitalGainViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
            case 2:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioSummaryViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? PortfolioSummaryViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
            case 3:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "CapitalGainRealizedViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? CapitalGainRealizedViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
                
            default:
                let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "InsuranceViewController"))
                vc.modalPresentationStyle = .overFullScreen
                 ( vc.topViewController as? InsuranceViewController)?.delegates = self
                self.present(vc, animated: true, completion: nil)
            }
        }
    }
}
