//
//  ClientSearchViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 20/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON

class ClientSearchViewController: UIViewController , UISearchBarDelegate , UISearchResultsUpdating , Completion {
   
    @IBOutlet weak var emptyScreen: EmptyScreen!
    @IBOutlet weak var table: UITableView!
    var type:String = ""
    var name:String = ""
    var network = SearchClientNetwork()
     let search = UISearchController(searchResultsController: nil)
    var searchTexts:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        network.delegate = self
       
        table.register(cell: SearchResultTableViewCell.self)
        network.network(table: table, name: name)

     
        self.definesPresentationContext = true
   }

        @objc func back(sender: UIBarButtonItem) {
            if PinViewController.dynamicMenu == 0 {
                navigationController?.popViewController(animated: true)
            }
            else {
                PinViewController.dynamicMenu = 0
                Routes.shared.gotoTab(nav: navigationController!)
            }
                     }
    func complete(data: JSON?) {
     
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        self.tabBarController?.navigationController?.navigationBar.prefersLargeTitles = true
               navigationController?.navigationBar.topItem?.searchController = search
            navigationController?.navigationBar.topItem?.title = "Client Search"
            navigationController?.navigationBar.topItem?.searchController!.searchBar.delegate = self
              self.tabBarController?.navigationItem.searchController!.searchResultsUpdater = self
            self.tabBarController?.definesPresentationContext = true
       self.tabBarController?.navigationItem.hidesSearchBarWhenScrolling = false
         self.tabBarController?.navigationItem.titleView = nil
               self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationItem.hidesBackButton = true
                   let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                                self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    //Shortcuts
    static func configureDynamicShortcutItem() {
          var shortcutItems = UIApplication.shared.shortcutItems ?? []
                    if let existingShortcutItem = shortcutItems.first {
                        guard let mutableShortcutItem = existingShortcutItem.mutableCopy() as? UIMutableApplicationShortcutItem
                            else { preconditionFailure("Expected a UIMutableApplicationShortcutItem") }
                        guard let index = shortcutItems.index(of: existingShortcutItem)
                            else { preconditionFailure("Expected a valid index") }

                        mutableShortcutItem.localizedTitle = "New Title"
                        shortcutItems[index] = mutableShortcutItem
                        UIApplication.shared.shortcutItems = shortcutItems
                    }
          let type = Bundle.main.bundleIdentifier! + ".Dynamic"
          
          let item = UIApplicationShortcutItem.init(type: type, localizedTitle: "Client Search", localizedSubtitle: "Search Your client", icon: UIApplicationShortcutIcon.init(type: .compose), userInfo: nil)
          let item2 = UIApplicationShortcutItem.init(type: type, localizedTitle: "Transactions", localizedSubtitle: "Transaction Details", icon: UIApplicationShortcutIcon.init(type: .compose), userInfo: nil)
          let item3 = UIApplicationShortcutItem.init(type: type, localizedTitle: "Folio Lookup", localizedSubtitle: "Folio", icon: UIApplicationShortcutIcon.init(type: .compose), userInfo: nil)
        
          UIApplication.shared.shortcutItems = [item , item2 , item3]
      }

    func updateSearchResults(for searchController: UISearchController) {
       
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = searchTexts
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
     
        resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text = search.searchBar.text ?? ""
        searchTexts = text
        network.network(table: table, name: text)
    }
}
extension ClientSearchViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if network.data?["result"]["data"].count == 0 {
            emptyScreen.isHidden = false
            table.isHidden = true
        }
        else {
            emptyScreen.isHidden = true
            table.isHidden = false
        }
        return network.data?["result"]["data"].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: SearchResultTableViewCell.self)
        cell?.name.text = network.data?["result"]["data"][indexPath.row]["name"].stringValue
        cell?.pan.text =  "PAN"
        var panVal = network.data?["result"]["data"][indexPath.row]["pan"].stringValue
        if panVal == "" {
            panVal = "Not Available"
        }
        cell?.panvalue.text = panVal ?? "Not Available"
        cell?.phone.text = "Phone"
        var phoneVal = network.data?["result"]["data"][indexPath.row]["mobile"].stringValue
        if phoneVal == "" {
            phoneVal = "Not Available"
        }
        cell?.phoneValue.text = phoneVal ?? "Not Available"
        cell?.firstLabel.text = "Name"
        cell?.firstRightLabel.text = "Family Head"
        var types = network.data?["result"]["data"][indexPath.row]["familyHead"].stringValue
        if types == "" {
            types = "Not Available"
        }
        else if types == "self" {
            types = "Self"
        }
        type = types ?? ""
        cell?.type.text = types
        return cell ?? UITableViewCell()
    }
}
extension ClientSearchViewController:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Shared.sharedInstance.typeId = network.data?["result"]["data"][indexPath.row]["uid"].stringValue ?? ""
        Shared.sharedInstance.clientName = network.data?["result"]["data"][indexPath.row]["name"].stringValue ?? ""
        let types = network.data?["result"]["data"][indexPath.row]["familyHead"].stringValue
        Shared.sharedInstance.typeAppid = network.data?["result"]["data"][indexPath.row]["appid"].stringValue ?? ""
        if types == "self" {
            Shared.sharedInstance.typeLevel = "98"
            Shared.sharedInstance.clientType = "familyHead"
        }
        else {
            Shared.sharedInstance.typeLevel = "100"
            
            Shared.sharedInstance.clientType = "client"
        }
        Routes.shared.gotoTab(nav: navigationController!)
    }
}

