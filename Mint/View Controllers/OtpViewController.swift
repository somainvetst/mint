//
//  OtpViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 17/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import OTPFieldView
import SwiftyJSON
class OtpViewController: UIViewController , Completion , confirmOtp {
   
    @IBOutlet weak var otpButton: UIButton!
    @IBOutlet weak var mobile: UILabel!
    @IBOutlet weak var otpField: OTPFieldView!
    @IBOutlet weak var seconds: UILabel!
    var second = 1290
    var timer = Timer()
    var isTimerRunning = false
    var token:String = ""
    var network = Otp()
    var expTime:Int = 0
    var mob:String = ""
    var otp:String = ""

    override func viewDidLoad() {
    super.viewDidLoad()
    setupOtpView()
        network.deleg = self
        mobile.text = mob
        network.getOtp(token: token)
        network.delegate = self
        otpButton.isEnabled = false
    }
    func otp(data: JSON) {
        if data["status"] == -1 {
            self.alert(message: "Please Enter Correct OTP")
        }
        else {
            Routes.shared.gotoPin(nav: navigationController!)
        }
       }
       
      
    func complete(data: JSON?) {
            startTimer()
      
        var expiringTime = data?["result"]["otpExpiresOn"].stringValue
        let dateFormatting = DateFormatter()
        dateFormatting.locale = Locale(identifier: "en_US_POSIX")
        dateFormatting.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        guard let date = dateFormatting.date(from: expiringTime!) else {return }
        let diff = date.offsetFrom(date: Date())
        print(diff)
        let min = diff.split(separator: "m")
        var minutes = min[0]
        var sec = min[1]
        var j = Int(sec)
       var m = Int(minutes)
        m = m! * 60
        print(j)
//        second = s! + m!
//        expTime = s! + m!
       }
       
    func setupOtpView(){
        self.otpField.fieldsCount = 6
        self.otpField.fieldBorderWidth = 2
        self.otpField.defaultBorderColor = UIColor.black
        self.otpField.filledBorderColor = UIColor.green
        self.otpField.cursorColor = UIColor.red
        self.otpField.displayType = .underlinedBottom
        self.otpField.fieldSize = 40
        self.otpField.separatorSpace = 8
        self.otpField.shouldAllowIntermediateEditing = false
        self.otpField.delegate = self
        self.otpField.initializeUI()
    }
    func startTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1,target: self , selector: (#selector(OtpViewController.updateTimer)) , userInfo: nil , repeats: true)
    }
    @objc func updateTimer() {
       if second < 1 {
             timer.invalidate()
        seconds.textColor = .red
        seconds.text = "OTP Expired"
             //Send alert to indicate "time's up!"
        } else {
         seconds.textColor = .black
             second -= 1
             seconds.text = timeString(time: TimeInterval(second))
        }
    }
    func timeString(time:TimeInterval) -> String {
    
    let minutes = Int(time) / 60 % 60
    let seconds = Int(time) % 60
        return String(format:"%02i:%02i", minutes, seconds)
    }
    @IBAction func resendOtp(_ sender: Any) {
        timer.invalidate()
       
        network.getOtp(token: token)
    }
    @IBAction func confirmOtp(_ sender: Any) {
        network.authenticateOtp(token: token, otp: otp)
    }
    
    
}
extension OtpViewController: OTPFieldViewDelegate {
    func hasEnteredAllOTP(hasEnteredAll hasEntered: Bool) -> Bool {
        if hasEntered == true {
                  otpButton.backgroundColor = #colorLiteral(red: 0, green: 0.5803921569, blue: 0.462745098, alpha: 1)
                  otpButton.isEnabled = true
              }
              else {
                  otpButton.backgroundColor = #colorLiteral(red: 0.631372549, green: 0.631372549, blue: 0.631372549, alpha: 1)
                  otpButton.isEnabled = false
              }
        print("Has entered all OTP? \(hasEntered)")
        return false
    }
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otp otpString: String) {
        print("OTPString: \(otpString)")
        otp = otpString
      
    }
}
