//
//  SipTransactionViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 28/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class SipTransactionViewController: UIViewController , back {
  
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var headerView: HeaderView!
    var sipDates:[String] = []
    var monthDate:[Int] = []
    var frequency:[String] = []
    var data:PassedSchemeForTransaction?
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationController?.navigationBar.isHidden = true
       monthDate = sipDates.map({Int($0)!})
        headerView.delegate = self
        table.register(cell: TransactionHeaderType2TableViewCell.self)
        table.register(cell: SipTableViewCell.self)
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    func setStructRef(ref:PassedSchemeForTransaction) {
            self.data = ref
    }
  
    func goBack() {
        navigationController?.popViewController(animated: true)
    }
  
}
extension SipTransactionViewController :  UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: TransactionHeaderType2TableViewCell.self)
            cell?.ucc.text = "UCC"
            cell?.folio.text = "Folio"
            return cell ?? UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(with: SipTableViewCell.self)
            cell?.amount.text = "Amount"
            cell?.date.text = "Date"
            cell?.endddate.text = "Date"
            cell?.endyear.text = "Year"
            cell?.endmonth.text = "Month"
            cell?.sipEndDate.text = "SIP End Date"
            cell?.sipstartdate.text = "SIP Start Date"
            cell?.month.text = "Month"
            cell?.year.text = "Year"
            cell?.selectmandate.text = "Select Mandate"
            cell?.selectFrequency.text = "Select Frequency"
            cell?.sipdates = sipDates
            cell?.sipInt = monthDate
            _ = cell?.dates(day: monthDate[0])
            cell?.selectFrequencytf.text = frequency[0]
            cell?.frequency = frequency
           
            return cell ?? UITableViewCell()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
