//
//  CapitalGainRealizedViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftyJSON
class CapitalGainRealizedViewController: UIViewController , Completion  , Savepdf  {
  
    @IBOutlet weak var searchtf: UITextField!
    @IBOutlet weak var activity: NVActivityIndicatorView!
    @IBOutlet weak var activityView: UIView!
    var picker = UIPickerView()
    var network = GetUsers()
      var capital = DownloadCapitalReport()
     var uid:String = ""
     var delegates:dismissing?
    override func viewDidLoad() {
        super.viewDidLoad()
        network.delegate = self
        searchtf.inputView = picker
        picker.delegate = self
        capital.delegate = self
        //navigation
        activityView.isHidden = true
              navigationController?.navigationBar.isHidden = false
              navigationController?.navigationBar.prefersLargeTitles = false
              self.title = "Capital Gain Unrealized"
              self.navigationController?.navigationBar.tintColor = UIColor.black
              navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = true
              self.navigationItem.hidesBackButton = false
              
              let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
              self.navigationItem.leftBarButtonItem = newBackButton
       
        //networking
        network.network(levelNo: "100")
        
        //id
        if Shared.sharedInstance.clientType.isEmpty == true {
          if CoreData.core.data.isEmpty == false {
              
              let result = CoreData.core.data[0]
              let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
              
              guard let login = logindata else {
                  return
              }
              
              guard  let id =  login["result"]?["uid"] as? String else {
                  
                  return
              }
              self.uid = id
          }
          }
    }
    func complete(data: JSON?) {
        print("here")
        picker.reloadAllComponents()
      }
      

    @objc func back(sender: UIBarButtonItem) {
        delegates?.close()
           dismiss(animated: true, completion: nil)
       }
       override func viewWillAppear(_ animated: Bool) {
           navigationController?.navigationBar.isHidden = false
           navigationController?.navigationBar.prefersLargeTitles = false
           self.title = "Capital Gain Unrealized"
           self.navigationController?.navigationBar.tintColor = UIColor.black
           navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = true
           self.navigationItem.hidesBackButton = false
       }
    @IBAction func downloadPdf(_ sender: Any) {
          if UITextField.validateAll(textFields: [searchtf]) {
            activityView.isHidden = false
            activity.startAnimating()
            if Shared.sharedInstance.clientType.isEmpty == true {
                capital.networkUnrealized(uid: uid) { (result: String , success: Bool) in
                    if success {
                        print("yes")
                    } }
            }
            else {
                  capital.networkUnrealized(uid: uid) { (result: String , success: Bool) in
                    if success {
                        print("yes")
                    } }
            }
            
        }
            else {
                self.alert(message: "Please select valid option", title: "Error")
            }
        }
        func showActivity(vc: UIViewController) {
            activityView.isHidden = true
            activity.stopAnimating()
            present(vc, animated: true, completion: nil)
        }
    
}
extension CapitalGainRealizedViewController: UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        guard let name = self.network.data?["result"]["data"].arrayValue.map ({
                  $0["name"].stringValue
              }) else {return 0}
        print(name)
        return name.count
    }
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        guard let name = self.network.data?["result"]["data"].arrayValue.map ({
                  $0["name"].stringValue
              }) else {return ""}
    return name[row]
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        guard let name = self.network.data?["result"]["data"].arrayValue.map ({
                         $0["name"].stringValue
                     }) else {return}
        searchtf.text = name[row]
        uid = (self.network.data?["result"]["data"][row]["uid"].stringValue)!
    }
    
}
//https://demo.investwell.app/api/client/reports/capitalGain/exportUnrealizedPDF?applicantUid=5553ce3575b8e1a64181794eb86c5b27

