//
//  TabBar.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 14/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class TabBar: UITabBarController , logging  {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        tabBarController?.delegate = self
        Shared.sharedInstance.delegate = self
        CoreData.core.fetchCartData(entity: "Logindata")
        self.definesPresentationContext = true
        let size = CGSize(width:25 , height:25)
        UITabBar.appearance().barTintColor = UIColor.white
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.gray], for:.normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Shared.sharedInstance.color], for:.selected)
        if CoreData.core.data.isEmpty == false {
            
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                return
            }
            guard  let level =  login["result"]?["levelNo"] as? Int else {
                
                return
            }
            //vcs
            //vc1
            let dashvc: DashboardViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController;
            self.navigationController?.navigationBar.topItem?.title = "Dashboard"
            dashvc.title = "Home"
            dashvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
            let item1 =  resizeImage(image: UIImage(named: "menu1")!, targetSize: size)
            dashvc.tabBarItem.image =  item1
            dashvc.tabBarItem.selectedImage = item1
            //vc2
            let portvc : PortfolioDetailsViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioDetailsViewController") as! PortfolioDetailsViewController;
            portvc.title = "Portfolio"
            portvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
            let item2 =  resizeImage(image: UIImage(named: "menu2")!, targetSize: size)
            portvc.tabBarItem.image =  item2
            portvc.tabBarItem.selectedImage = item2
            //vc3
            let tranvc : TransactionsViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController;
            tranvc.title = "Transactions"
            tranvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
            let item3 =  resizeImage(image: UIImage(named: "menu4")!, targetSize: size)
            tranvc.tabBarItem.image =  item3
            tranvc.tabBarItem.selectedImage = item3
            //vc4
            let menuvc : MenuViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController;
            menuvc.title = "Menu"
            menuvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
            let item4 =  resizeImage(image: UIImage(named: "menu3")!, targetSize: size)
            menuvc.tabBarItem.image =  item4
            menuvc.tabBarItem.selectedImage = item4
            //vc5
            let portclientvc:PortfolioClientViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioClientViewController") as! PortfolioClientViewController
            portclientvc.title = "Portfolio"
            portclientvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
            let item5 =  resizeImage(image: UIImage(named: "menu2")!, targetSize: size)
            portclientvc.tabBarItem.image =  item5
            portclientvc.tabBarItem.selectedImage = item5
            //vc6
            let settingvc:SettingsViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            settingvc.title = "Settings"
            settingvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
            let item6 =  resizeImage(image: UIImage(named: "menu5")!, targetSize: size)
            settingvc.tabBarItem.image =  item6
            settingvc.tabBarItem.selectedImage = item6
            //vc7
            let clientvc:ClientSearchViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "ClientSearchViewController") as! ClientSearchViewController
                       clientvc.title = "Clients"
                       clientvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
                       let item7 =  resizeImage(image: UIImage(named: "menu2")!, targetSize: size)
                       clientvc.tabBarItem.image =  item7
                       clientvc.tabBarItem.selectedImage = item7
            //vc8
                       let aumvc:AumLevelOneViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "AumLevelOneViewController") as! AumLevelOneViewController
                                 aumvc.title = "AUM"
                                  aumvc.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0,vertical: -4.0)
                                  let item8 =  resizeImage(image: UIImage(named: "menu2")!, targetSize: size)
                                  aumvc.tabBarItem.image =  item8
                                 aumvc.tabBarItem.selectedImage = item8
            //cases
            switch level  {
            case 98 :
                self.viewControllers = [dashvc , portvc , menuvc , tranvc , settingvc]
            case 100 :
                self.viewControllers = [dashvc , portclientvc , menuvc , tranvc , settingvc]
            default :
                if Shared.sharedInstance.clientType == "familyHead" {
                    self.viewControllers = [dashvc , portvc , menuvc , tranvc]
                }
                else  if Shared.sharedInstance.clientType == "client" {
                    self.viewControllers = [dashvc , portclientvc , menuvc , tranvc]
                }
                else {
                    self.viewControllers = [dashvc , clientvc , menuvc  ,aumvc , settingvc]
                }
            }
        }
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    func logout() {
        Shared.sharedInstance.loggingout(nav: navigationController!)
    }
    
    
}
extension TabBar: UITabBarControllerDelegate {
    
    /*
     Called to allow the delegate to return a UIViewControllerAnimatedTransitioning delegate object for use during a noninteractive tab bar view controller transition.
     ref: https://developer.apple.com/documentation/uikit/uitabbarcontrollerdelegate/1621167-tabbarcontroller
     */
    func tabBarController(_ tabBarController: UITabBarController, animationControllerForTransitionFrom fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        print(tabBarController.selectedIndex)
        return TabBarAnimatedTransitioning()
    }
    
}

/*
 UIViewControllerAnimatedTransitioning
 A set of methods for implementing the animations for a custom view controller transition.
 ref: https://developer.apple.com/documentation/uikit/uiviewcontrolleranimatedtransitioning
 */
final class TabBarAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {
    
    /*
     Tells your animator object to perform the transition animations.
     */
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let destination = transitionContext.view(forKey: UITransitionContextViewKey.to) else { return }
        
        destination.alpha = 0.0
        destination.transform = .init(scaleX: 1.5, y: 1.5)
        transitionContext.containerView.addSubview(destination)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            destination.alpha = 1.0
            destination.transform = .identity
        }, completion: { transitionContext.completeTransition($0) })
    }
    
    /*
     Asks your animator object for the duration (in seconds) of the transition animation.
     */
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.25
    }
    
}
