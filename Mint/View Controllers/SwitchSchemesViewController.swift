//
//  SwitchSchemesViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 04/11/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
protocol Sname:class  {
    func schemename(name:String)
}
class SwitchSchemesViewController: UIViewController , Completion {
 
  var network = AdditionalInvestment()
    var delegate:Sname?
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        network.delegate = self
    network.getswitchSchemes(table: table, id: Transactiondata.shared.schid)
       
     navigationController?.navigationBar.isHidden = false
                                navigationController?.navigationBar.prefersLargeTitles = false
                                self.title = "Switch Schemes"
                               self.navigationItem.hidesBackButton = true
                               let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                               self.navigationItem.leftBarButtonItem = newBackButton
              }
              
              @objc func back(sender: UIBarButtonItem) {
                 dismiss(animated: true, completion: nil)
                       }
    

   func complete(data: JSON?) {
              
    var type = data?["result"]["data"]["allowedInvestmentTypes"].count ?? 0
    for i in 0..<type {
        var j = data?["result"]["data"]["allowedInvestmentTypes"][i].stringValue ?? ""
        Transactiondata.shared.subType.append(j)
        
        
    }
    if Transactiondata.shared.subType.contains("") {
        Transactiondata.shared.subType.remove(object: "")
    }
     delegate?.schemename(name: Transactiondata.shared.switchSchemeName)
                dismiss(animated: true, completion: nil)
    }
    

}
extension SwitchSchemesViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return network.data?["result"]["schemes"].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SwitchSchemeTableViewCell", for: indexPath) as! SwitchSchemeTableViewCell
        cell.name.text = network.data?["result"]["schemes"][indexPath.row
            ]["name"].stringValue
        return cell
      
    }
}
extension SwitchSchemesViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          Transactiondata.shared.switchSchemeName = network.data?["result"]["schemes"][indexPath.row]["name"].stringValue ?? ""
        let id = network.data?["result"]["schemes"][indexPath.row]["schemeGroupId"].stringValue ?? ""
        network.getValidInvestment(id: id, type: "SWO")
    }
}
