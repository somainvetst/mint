//
//  AumLevelThreeViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 31/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import SwiftyJSON
class AumLevelThreeViewController: UIViewController , Completion {
 
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activity: NVActivityIndicatorView!
    @IBOutlet weak var sName: UILabel!
    @IBOutlet weak var table: UITableView!
    var name:String = ""
    var schid:String = ""
    var fundis:String = ""
    var category:String = ""
    var network = Aum()
    override func viewDidLoad() {
        super.viewDidLoad()
          table.tableFooterView = UIView()
        activityView.isHidden = false
        activity.startAnimating()
        network.delegate = self
        table.register(cell: AumTableViewCell.self)
        table.register(cell: Aum1TableViewCell.self)
        sName.text = name
        network.network3(table: table, id: fundis, sid: schid)
   
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = false
        self.title = "AUM Report"
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
       navigationController?.popViewController(animated: true)
             }
       
    func complete(data: JSON?) {
        activityView.isHidden = true
        activity.stopAnimating()
     }
     
}
extension AumLevelThreeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return network.data2?["result"]["data"].count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: Aum1TableViewCell.self)
            cell?.head1.text = "Total"
            cell?.head2.text = "Category"
            cell?.value1.text = network.data2?["result"]["summary"]["AUM"].stringValue.toCurrencyFormat()
            cell?.value2.text = category
            return cell ?? UITableViewCell()
        default:
              let cell = tableView.dequeueReusableCell(with: AumTableViewCell.self)
              cell?.bottomLabel.text = "Purchase Cost"
              cell?.bottomLabel2.text = "Allocation"
              cell?.bottomvalue.text = network.data2?["result"]["data"][indexPath.row]["AUM"].stringValue.toCurrencyFormat()
              cell?.bottomvalue2.text = "\(network.data2?["result"]["data"][indexPath.row]["allocation"].stringValue.roundOffString() ?? "")%"
              cell?.name.text = network.data2?["result"]["data"][indexPath.row]["name"].stringValue
                      return cell ?? UITableViewCell()
        }
        //https://demo.investwell.app/api/broker/AUMReport/downloadAUMReportForMutualFundsCSV?filters=[]&groupBy=1001&orderBy=AUM&orderByDesc=true
        //https://demo.investwell.app/api/broker/AUMReport/downloadAUMReportForMutualFundsCSV?filters=[{%22fundid%22:%22e09e7bff22c4a6cb67d00b399ecadedf%22}]&groupBy=1002&orderBy=AUM&orderByDesc=true
        //https://demo.investwell.app/api/broker/AUMReport/downloadAUMReportForMutualFundsCSV?filters=[{%22fundid%22:%22e09e7bff22c4a6cb67d00b399ecadedf%22},{%22schid%22:%22bc6c1cba172d38bbb61f87d6795e5358%22}]&groupBy=100&orderBy=AUM&orderByDesc=true
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
}
extension AumLevelThreeViewController: UITableViewDelegate {
    
}
