//
//  PinViewController.swift
//  Thukral Capital Market
//
//  Created by Excel Net Solutions Pvt Ltd on 01/05/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import NVActivityIndicatorView
class PinViewController: UIViewController , Success {
  
    var pinString = ""
    var tempPin: String?
    var timer: Timer?
    var isItPresentingViaShortcutAction = false
   static var dynamicMenu:Int = 0
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activity: NVActivityIndicatorView!
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pinImage: UIImageView!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var pinLabel: UILabel!
    var login : [String:AnyObject] = [:]
    var type = 0
    var emails:String = ""
    var passwords:String = ""
    var time:String = ""
    let network = Network()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        network.delegate = self
          activityView.isHidden = true
        CoreData.core.fetchCartData(entity: "Logindata")
        setup()
        check()
     
       // createTimer()
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.navigationBar.isHidden = true
    }
    func dynamicScreens() {
        switch PinViewController.dynamicMenu {
        case 03:
            Routes.shared.goToClientsSearch(nav: navigationController!)
        case 01:
              Routes.shared.goToTransaction(nav: self.navigationController!)
        case 02:
            Routes.shared.gotoFolio(nav: self.navigationController!)
        case 10:
             if CoreData.core.data.isEmpty == false {
                 
                 let result = CoreData.core.data[0]
                 let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
                 
                 guard let login = logindata else {
                     
                     return
                 }
                 self.login = login
                 
                 guard  let level =  login["result"]?["levelNo"] as? Int else {
                     
                     return
                 }
                 
                 switch level {
                 case 98:
                    Routes.shared.goToPortfolio(nav: navigationController!)
                 default:
                    Routes.shared.goToPortfolioClient(nav: navigationController!)
                     
                 }
             }
        case 11:
             print("")
        case 12:
             print("")
        default:
             print("")
        }
    }
    func check() {
        if type == 0 {
           let value:BioMetricSupported = AuthUtlity.supportedBiometricType()
           if (value == .none)
           {
               print(value.rawValue)
           }
           else
           {
              AuthUtlity.isValidUer(reasonString: "BioMetric Authentication") {[unowned self] (isSuccess, stringValue) in
                   
                   if isSuccess
                   {
                    self.loginAgain()
                   }
                    else  {
                   
                     // print(stringValue)
                   }
                   
               
           
       }
        }
        }
    }
    func successfull() {
        print("jerer")
        levelChecker()
    }
    
    func completed() {
        activityView.isHidden = true
        activity.stopAnimating()
    }
    
    func error(msg: String) {
         self.alert(message: msg, title: "Error")
    }
    
    @objc func logOut() {
        let vc: LoginViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController;
        Shared.sharedInstance.clear()
        CoreData.core.deleteAllData(entity: "Logindata")
        let cookieStore = HTTPCookieStorage.shared
        for cookie in cookieStore.cookies ?? [] {
            cookieStore.deleteCookie(cookie)
        }
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        let nav = UINavigationController(rootViewController: vc)
        nav.navigationBar.isHidden = true
        nav.interactivePopGestureRecognizer?.isEnabled = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = nav
    }
    func createTimer() {
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
        if timer == nil {
            // 2
            timer = Timer.scheduledTimer(timeInterval: 1200.0,
                                         target: self,
                                         selector: #selector(logOut),
                                         userInfo: nil,
                                         repeats: true)
        }
        
    }
    private func loginAgain() {
        CoreData.core.deleteAllData(entity: "Logindata")
                                let cookieStore = HTTPCookieStorage.shared
                                for cookie in cookieStore.cookies ?? [] {
                                    cookieStore.deleteCookie(cookie)
                                }
                                
        guard let email = UserDefaults.standard.value(forKey: "email") , let password = UserDefaults.standard.value(forKey: "password") , let brokerid = UserDefaults.standard.value(forKey: "brokerid") else { self.alert(message: "Please Reset")
                                    return
                                    
                                }
     
        
        guard let emailTouse = (email as? String)?.trailingTrim() else { self.alert(message: "Please Reset")
            return
            
        }
        guard let brokerIdTouse = (brokerid as? String)?.trailingTrim() else {  self.alert(message: "Please Reset")
            return
            
            
        }
        
                              activityView.isHidden = false
                              activity.startAnimating()
                                 Shared.sharedInstance.baseUrl = "https://\(brokerIdTouse).investwell.app/api/"
                                let url = "\(Shared.sharedInstance.baseUrl)auth/login"
                                let parameters: Parameters = ["email": emailTouse , "password": password , "brokerDomain": brokerIdTouse]
        network.hitServer(url: url, parameters: parameters, method: .post , type: "Login", brokerdomain: brokerIdTouse, headers: ["User-Agent":"MintApp"])
    }
    func levelChecker() {
           CoreData.core.fetchCartData(entity: "Logindata")
        if CoreData.core.data.isEmpty == false {
          
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                
                return
            }
            self.login = login
            
            guard  let level =  login["result"]?["levelNo"] as? Int else {
                
                return
            }
            
            switch level {
            case 1...10 :
                   ClientSearchViewController.configureDynamicShortcutItem()
                if PinViewController.dynamicMenu == 0 {
                    print("fdaaaaa")
                Routes.shared.gotoTab(nav: navigationController!)
                }
                else {
                    dynamicScreens()
                }
            default :
                FolioLookupViewController.configureDynamicShortcutItem2()
                if PinViewController.dynamicMenu == 0 {
                    print("fdaaaassssa")
                Routes.shared.gotoTab(nav: navigationController!)
                }
                else {
                    dynamicScreens()
                }
                
            }
        }
        else {
           print("dhnjfjfj")
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        resetButton.layer.borderColor = #colorLiteral(red: 0.1144756451, green: 0.190612942, blue: 0.4077430367, alpha: 1)
    }
    func setup(){
        
        if let pin = UserDefaults.standard.value(forKey: "pin") as? String{
            pinLabel.text = "Enter Pin"
            tempPin = pin
        }else{
            if tempPin != nil{
                pinLabel.text = "Confirm Pin"
            }else{
                pinLabel.text = "Generate Pin"
            }
            
        }
        
        
        if UserDefaults.standard.bool(forKey: "resetButton"){
            resetButton.isHidden =  false
        }else{
            resetButton.isHidden = true
        }
    }
    
    @IBAction func reset(_ sender: Any) {
        resetConfirmation()
    }
    func resetConfirmation(){
        let title =  "Do you want to reset your security pin"
        let message = "It will require your user id and password to generate new pin"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let okAction = UIAlertAction(title: "OK", style: .default) { (action) in
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            let cookieStore = HTTPCookieStorage.shared
            for cookie in cookieStore.cookies ?? [] {
                cookieStore.deleteCookie(cookie)
            }
            Routes.shared.gotoLogin(nav: self.navigationController!, id: 0)
            
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        if let popOverPresentation = alertController.popoverPresentationController{
            popOverPresentation.sourceView = self.view
            popOverPresentation.sourceRect = resetButton.frame
        }
        
        
        present(alertController, animated: true, completion: nil)
        
    }
    func removeCharacter(){
        
        pinString.removeLast()
        passwordField.text = pinString
    }
    func addToString(passCharacter: String) {
        pinString.append(passCharacter)
        passwordField.text = pinString
        if pinString.count == 4 {
            if let userPin = tempPin {
                if userPin == pinString{
                    UserDefaults.standard.set(true, forKey: "resetButton")
                    UserDefaults.standard.setValue(pinString, forKey: "pin")
                    UserDefaults.standard.setValue(true, forKey: "canLogin")
                    Shared.sharedInstance.clear()
                    loginAgain()
                }
                else{
                    pinString = ""
                    passwordField.text = pinString
                    let title =  "Failed"
                    let message = "Pattern Mismatch"
                    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            else {
                let storyboard = UIStoryboard(name: "Main", bundle: nil);
                let Pvc: PinViewController = storyboard.instantiateViewController(withIdentifier: "PinViewController") as! PinViewController;
                Pvc.tempPin = pinString
                navigationController?.pushViewController(Pvc, animated: false)
            }
        }
    }
}
extension PinViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PinCell", for: indexPath) as! PinCollectionViewCell
        cell.numberLabel.isHidden = false
        cell.eraseImage.isHidden = true
        cell.circularView.isHidden = false
        if indexPath.item == 10{
            cell.numberLabel.text = "0"
        }else if indexPath.item == 9{
            cell.numberLabel.text = ""
            cell.circularView.isHidden = true
        }else if indexPath.item == 11{
            cell.circularView.isHidden = true
            cell.numberLabel.isHidden = true
            cell.eraseImage.isHidden = false
        }else{
            cell.numberLabel.text = "\(indexPath.item + 1)"
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 3, height: collectionView.frame.width / 4)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 11 {
            if pinString != ""{
                removeCharacter()
            }
        }else if indexPath.item == 9{
            
        }else if indexPath.item == 10{
            addToString(passCharacter: "0")
        }else{
            addToString(passCharacter: String(indexPath.item + 1))
        }
        
        
        
    }
}
