//
//  TransactionsViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 20/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import DropDown
import CTSlidingUpPanel
import SwiftyJSON
import NVActivityIndicatorView
class TransactionsViewController: UIViewController , UITextFieldDelegate , UITabBarControllerDelegate , UIGestureRecognizerDelegate , Completion {
   
    @IBOutlet weak var memberHeight: NSLayoutConstraint!
    @IBOutlet weak var rfHeight: NSLayoutConstraint!
    @IBOutlet weak var memberTop: NSLayoutConstraint!
    @IBOutlet weak var rfTop: NSLayoutConstraint!
    @IBOutlet weak var swipeUp: UIView!
    @IBOutlet weak var relationshiptf: UITextField!
    @IBOutlet weak var apply: UIButton!
    @IBOutlet weak var transactionDuration: UISegmentedControl!
    @IBOutlet weak var filterView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var membertf: UITextField!
    @IBOutlet weak var endtf: UITextField!
    @IBOutlet weak var starttf: UITextField!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var customViewHeight: NSLayoutConstraint!
    @IBOutlet weak var customDateView: UIView!
    @IBOutlet weak var end: UILabel!
    @IBOutlet weak var start: UILabel!
    @IBOutlet weak var customButton: UIButton!
    @IBOutlet weak var currentYearButton: UIButton!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var currentButton: UIButton!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var blurView: UIView!
    let datePicker = UIDatePicker()
    let relationshipPicker = UIPickerView()
    let clientPicker = UIPickerView()
    var amountColor = UIColor.red
    var colorArray:[UIColor] = []
    let dropDown = DropDown()
    var startdate:String = ""
    var enddate:String = ""
    let formatter = DateFormatter()
    var transaction = Transaction()
    var uid:String = ""
    var levelid:String = ""
    var users = GetUsers()
    var brokerarr:[String] = []
    var brokerarrid:[String] = []
    var customType:String = ""
    var delegates:dismissing?
    @IBOutlet weak var memberView: UIView!
    @IBOutlet weak var rmView: UIView!
    @IBOutlet weak var activityView: UIView!
       @IBOutlet weak var activity: NVActivityIndicatorView!
    var bottomController:CTBottomSlideController?;
    override func viewDidLoad() {
        super.viewDidLoad()
        transaction.delegate = self
          activityView.isHidden = true
        membertf.isHidden = false
        memberView.isHidden = false
        memberTop.constant = 5
        memberHeight.constant = 30
        swipeUp.isHidden = true
        relationshipPicker.delegate = self
        clientPicker.delegate = self
        relationshiptf.inputView = relationshipPicker
        membertf.inputView = clientPicker
        apply.layer.cornerRadius = 20
        apply.layer.backgroundColor = #colorLiteral(red: 0.8392156863, green: 0.8392156863, blue: 0.8431372549, alpha: 1)
        apply.titleLabel?.textColor = UIColor.black
        bottomController = CTBottomSlideController(parent: view, bottomView:filterView,
                                                   tabController: self.tabBarController,
                                                   navController: self.navigationController, visibleHeight: 64)
        //0 is bottom and 1 is top. 0.5 would be center
        bottomController?.setAnchorPoint(anchor: 0.6)
        bottomController?.setExpandedTopMargin(pixels: 300)
        //
        
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.topItem?.title = "Transactions"
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        if Shared.sharedInstance.clientType == "" && Shared.sharedInstance.id <= 10 {
            relationshiptf.isHidden = false
            rmView.isHidden = false
            rfTop.constant = 10
            rfHeight.constant = 30
            newBackButton.title = "Back"
            navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
            self.tabBarController?.navigationItem.hidesBackButton = true
        }
        else {
            rfTop.constant = 0
            rfHeight.constant = 0
            relationshiptf.isHidden = true
            rmView.isHidden = true
            newBackButton.title = ""
            navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
            self.tabBarController?.navigationItem.hidesBackButton = true
            if Shared.sharedInstance.typeLevel == "98" {
                membertf.isHidden = false
                memberView.isHidden = false
                memberTop.constant = 5
                  rfTop.constant = 0
                  memberHeight.constant = 30
                rfHeight.constant = 0
            }
            else  if Shared.sharedInstance.typeLevel == "100" {
                membertf.isHidden = false
                memberView.isHidden = false
                memberTop.constant = 0
                  rfTop.constant = 0
                rfHeight.constant = 0
                  memberHeight.constant = 0
            }
        }
        //         self.tabBarController?.delegate = self
        // blurView.isHidden = true
        table.register(cell: TransactionTableViewCell.self)
        formatter.dateFormat = "yyyy-MM-dd"
        customViewHeight.constant = 0
        customDateView.isHidden = true
        starttf.delegate = self
        endtf.delegate = self
        membertf.delegate = self
        startdate = formatter.string(from: Date().getThisMonthStart()!)
        enddate = formatter.string(from: Date().getThisMonthEnd()!)
        transaction.network(table: table , startdate: startdate, enddate: enddate, id: uid, level: levelid, type: [], page: "1")
     
       
        //coredata
        guard let brokerarr = UserDefaults.standard.value(forKey: "brokerArray") else {return}
         guard let brokerarrid = UserDefaults.standard.value(forKey: "brokeridsss") else {return}
        self.brokerarr = (brokerarr as? [String])!
        self.brokerarrid = (brokerarrid as? [String])!
        if self.brokerarrid.isEmpty == false  && Shared.sharedInstance.clientType == "" {
            users.network(levelNo: self.brokerarrid[0])
            relationshiptf.text = self.brokerarr[0]
        }
        else {
            users.network(levelNo: "100")
        }
       
    }
    
    
    func complete(data: JSON?) {
        activity.stopAnimating()
        activityView.isHidden = true
       }
       
       
    @IBAction func openTransactionFilter(_ sender: Any) {
        let vc  = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "TrabsactionTypeFilterViewController"))
                      vc.modalPresentationStyle = .overFullScreen
                      self.present(vc, animated: true, completion: nil)
    }
    @objc func back(sender: UIBarButtonItem) {
        
        if PinViewController.dynamicMenu == 0 {
            delegates?.close()
            dismiss(animated: true, completion: nil)
        }
        else {
            PinViewController.dynamicMenu = 0
            Routes.shared.gotoTab(nav: navigationController!)
        }
    }
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        bottomController?.viewWillTransition(to: size, with: coordinator)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.navigationItem.rightBarButtonItem = nil
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.topItem?.title = "Transactions"
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.tabBarController?.navigationItem.titleView = nil
    }
    
    @IBAction func currentMonthPressed(_ sender: Any) {
        currentButton.setImage(UIImage(named: "filled"), for: .normal)
        previousButton.setImage(UIImage(named: "notfilled"), for: .normal)
        currentYearButton.setImage(UIImage(named: "notfilled"), for: .normal)
        customButton.setImage(UIImage(named: "notfilled"), for: .normal)
        customDateView.isHidden = true
        customViewHeight.constant = 0
        startdate = formatter.string(from: Date().getThisMonthStart()!)
        enddate = formatter.string(from: Date().getThisMonthEnd()!)
        
    }
    @IBAction func durationFilterTapped(_ sender: Any) {
        switch transactionDuration.selectedSegmentIndex {
        case 0:
            customDateView.isHidden = true
            customViewHeight.constant = 0
            startdate = formatter.string(from: Date().getThisMonthStart()!)
            enddate = formatter.string(from: Date().getThisMonthEnd()!)
            bottomController?.setExpandedTopMargin(pixels: 300)
        case 1:
            customViewHeight.constant = 0
            customDateView.isHidden = true
            startdate = formatter.string(from: Date().getLastMonthStart()!)
            enddate = formatter.string(from: Date().getLastMonthEnd()!)
            bottomController?.setExpandedTopMargin(pixels: 300)
        case 2:
            customViewHeight.constant = 0
            customDateView.isHidden = true
            let now = Date()
            let calendar: Calendar = Calendar.current
            let startDate = calendar.startOfYear(now)
            let lastDay = calendar.endOfYear(now)
            startdate =  formatter.string(from: startDate)
            enddate = formatter.string(from: lastDay)
            bottomController?.setExpandedTopMargin(pixels: 300)
        default:
            customDateView.isHidden = false
            customViewHeight.constant = 80
            bottomController?.setAnchorPoint(anchor: 0.77)
            bottomController?.setExpandedTopMargin(pixels: 150)
        }
    }
    
    @IBAction func previousPressed(_ sender: Any) {
        currentButton.setImage(UIImage(named: "notfilled"), for: .normal)
        previousButton.setImage(UIImage(named: "filled"), for: .normal)
        currentYearButton.setImage(UIImage(named: "notfilled"), for: .normal)
        customButton.setImage(UIImage(named: "notfilled"), for: .normal)
        customViewHeight.constant = 0
        customDateView.isHidden = true
        startdate = formatter.string(from: Date().getLastMonthStart()!)
        enddate = formatter.string(from: Date().getLastMonthEnd()!)
        
    }
    @IBAction func currentPressed(_ sender: Any) {
        currentButton.setImage(UIImage(named: "notfilled"), for: .normal)
        previousButton.setImage(UIImage(named: "notfilled"), for: .normal)
        currentYearButton.setImage(UIImage(named: "filled"), for: .normal)
        customButton.setImage(UIImage(named: "notfilled"), for: .normal)
        customViewHeight.constant = 0
        customDateView.isHidden = true
        let now = Date()
        let calendar: Calendar = Calendar.current
        let startDate = calendar.startOfYear(now)
        let lastDay = calendar.endOfYear(now)
        startdate =  formatter.string(from: startDate)
        enddate = formatter.string(from: lastDay)
        
    }
    
    @IBAction func openCustomView(_ sender: Any) {
        currentButton.setImage(UIImage(named: "notfilled"), for: .normal)
        previousButton.setImage(UIImage(named: "notfilled"), for: .normal)
        currentYearButton.setImage(UIImage(named: "notfilled"), for: .normal)
        customButton.setImage(UIImage(named: "filled"), for: .normal)
        customDateView.isHidden = false
        customViewHeight.constant = 80
    }
    
    @IBAction func startDatePressed(_ sender: Any) {
        
        
    }
    @IBAction func endDatePressed(_ sender: Any) {
        
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == starttf {
            showDatePicker(type: "start")
            datePicker.minimumDate = Calendar.current.date(byAdding: .year , value: -50, to: Date())
              datePicker.maximumDate = Calendar.current.date(byAdding: .day , value: 0, to: Date())
        }
        else if textField == endtf  {
            showDatePicker(type: "end")
            let date = formatter.date(from: startdate)
            datePicker.minimumDate = Calendar.current.date(byAdding: .day , value: 1, to: date!)
            datePicker.maximumDate = Calendar.current.date(byAdding: .year , value: 50, to: Date())
        }
    }
    @IBAction func openFilter(_ sender: Any) {
        swipeUp.isHidden = false
        swipeUp.transform = swipeUp.transform.scaledBy(x: 0.001, y: 0.001)
        
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            self.swipeUp.transform = CGAffineTransform.identity.scaledBy(x: 1.0, y: 1.0)
        },  completion: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.swipeUp.isHidden = true
        }
    }
    
    @IBAction func apply(_ sender: Any) {
        activityView.isHidden = false
        activity.startAnimating()
        // blurView.isHidden = true
        let type = TrabsactionTypeFilterViewController.unselectedFilter.joined(separator: ",")
        print(type)
        transaction.network(table: table , startdate: startdate, enddate: enddate, id: uid, level: levelid, type: TrabsactionTypeFilterViewController.unselectedFilter , page: "1")
    }
    @IBAction func closeFilter(_ sender: Any) {
        //        filterView.hideView()
        //        blurView.isHidden = true
    }
    @objc func wasDragged(gestureRecognizer: UIPanGestureRecognizer) {
        if gestureRecognizer.state == UIGestureRecognizer.State.began || gestureRecognizer.state == UIGestureRecognizer.State.changed {
            let translation = gestureRecognizer.translation(in: self.view)
            // print(gestureRecognizer.view!.center.y)
            print(self.view.bounds.maxY)
            if(gestureRecognizer.view!.center.y < self.view.bounds.maxY + 1) {
                if ((gestureRecognizer.view!.center.y + translation.y) <= self.view.bounds.maxY ) {
                    gestureRecognizer.view!.center = CGPoint(x:gestureRecognizer.view!.center.x, y:gestureRecognizer.view!.center.y + translation.y)
                }
                else {
                    print("hereere")
                    gestureRecognizer.view!.center = CGPoint(x:gestureRecognizer.view!.center.x, y:self.view.frame.maxY + 23)
                }
                print(gestureRecognizer.view!.center.y + translation.y)
            }else {
                gestureRecognizer.view!.center = CGPoint(x:gestureRecognizer.view!.center.x, y: self.view.frame.maxY)
                
            }
            
            gestureRecognizer.setTranslation(CGPoint(x:0,y:0), in: self.view)
        }
        
    }
    func addTextOfDropdown() {
        guard var name = self.users.data?["result"]["data"].arrayValue.map ({
            $0["name"].stringValue
        }) else {return}
        name.append("All")
        dropDown.anchorView = membertf
        dropDown.dataSource = name
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.membertf.text = item
            if item == "All" {
                self.uid = ""
                self.levelid = ""
            }
            else {
                guard let uid = self.users.data?["result"]["data"][index]["uid"].stringValue else {return}
                guard let levelid = self.users.data?["result"]["data"][index]["levelid"].stringValue else {return}
                self.uid = uid
                self.levelid = levelid
            }
            
            
        }
        
    }

}
extension TransactionsViewController:UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if transaction.data?["result"]["data"].count != 0 {
            noDataView.isHidden = true
        }
        else {
            noDataView.isHidden = false
        }
        return transaction.data?["result"]["data"].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: TransactionTableViewCell.self)
        cell?.name.text = transaction.data?["result"]["data"][indexPath.row]["clientName"].stringValue
        cell?.schemeName.text = transaction.data?["result"]["data"][indexPath.row]["schemeName"].stringValue
        cell?.folio.text = "Folio"
        cell?.date.text = "Date"
        cell?.transactionType.text = "Transaction Type"
        cell?.amount.text = "Amount"
        cell?.folioValue.text = transaction.data?["result"]["data"][indexPath.row]["folioNo"].stringValue
        var trntype = transaction.data?["result"]["data"][indexPath.row]["txnType"].stringValue
        if trntype == "SWI" {
            amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            
            trntype = "Switch In"
        }
        else if trntype == "DVP" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            
            trntype = "Dividend Payout"
        }
        else if trntype == "SWO" {
            amountColor = .red
            
            trntype = "Switch Out"
        }
        else if trntype == "NRP" || trntype == "ESP"  || trntype == "EBP" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Purchase"
        }
        else if trntype == "DRI" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Dividend Reinvestment"
        }
        else if trntype == "NRS" || trntype == "ESS"  || trntype == "EBS" {
            amountColor = .red
            colorArray.append(amountColor)
            trntype = "Sell"
        }
        else if trntype == "BON" || trntype == "BOB"  {
            amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Bonus"
        }
        else if trntype == "STI" {
            amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Systematic Transfer In"
        }
        else if trntype == "STO" {
              amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Systematic Transfer Out"
        }
        else if trntype == "POS" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Dividend Paid"
        }
        else if trntype == "POB" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Interest"
        }
        else if trntype == "SI" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Single"
        }
        else if trntype == "JO" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Joint"
        }
        else if trntype == "AS" {
             amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Anyone or Survivor"
        }
        else if trntype == "ES" {
              amountColor = #colorLiteral(red: 0.2509803922, green: 0.6352941176, blue: 0.1176470588, alpha: 1)
            colorArray.append(amountColor)
            trntype = "Either or Survivor"
        }
        else {
            colorArray.append(amountColor)
        }
        cell?.transactionvalue.text = trntype ?? ""
       
        cell?.amountValue.text = transaction.data?["result"]["data"][indexPath.row]["amount"].stringValue.toDoubleCuurency()
        cell?.amountValue.textColor = amountColor
        let date =  transaction.data?["result"]["data"][indexPath.row]["navDate"].stringValue.toDate()
     //  print(transaction.data?["result"]["data"][indexPath.row]["navDate"].stringValue)
        cell?.dateValue.text = date
        //        cell?.dateValue.text = formatter.string(from: date!)
        return cell ?? UITableViewCell()
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom < height {
      
        }
    }
    @IBAction func back(_ sender: Any) {
        if PinViewController.dynamicMenu == 0 {
            navigationController?.popViewController(animated: true)
        }
        else {
            PinViewController.dynamicMenu = 0
            Routes.shared.gotoTab(nav: navigationController!)
        }
    }
    
}
extension TransactionsViewController {
    func showDatePicker(type:String){
        //Formate Date
        datePicker.datePickerMode = .date
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        if type == "start" {
            customType = "start"
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
            
            toolbar.setItems([cancelButton ,spaceButton , doneButton], animated: false)
            starttf.inputAccessoryView = toolbar
            starttf.inputView = datePicker
        }
        else {
            
            customType = "end"
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneenddatePicker));
            let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
            toolbar.setItems([cancelButton ,spaceButton , doneButton], animated: false)
            endtf.inputAccessoryView = toolbar
            endtf.inputView = datePicker
        }
        
        
        
        
    }
    
    @objc func donedatePicker(){
        starttf.text = formatter.string(from: datePicker.date).toBasicDate()
        startdate = formatter.string(from: datePicker.date)

        self.view.endEditing(true)
    }
    @objc func doneenddatePicker(){
        endtf.text = formatter.string(from: datePicker.date).toBasicDate()
        enddate = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
extension TransactionsViewController : CTBottomSlideDelegate {
    func didPanelCollapse() {
        blurView.isHidden = true
    }
    
    func didPanelExpand() {
        swipeUp.isHidden = true
        blurView.isHidden = false
    }
    
    func didPanelAnchor() {
        
    }
    
    func didPanelMove(panelOffset: CGFloat) {
        
    }
    
    
}
extension TransactionsViewController: UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == relationshipPicker {
            return brokerarr.count
        }
        else {
            guard var name = self.users.data?["result"]["data"].arrayValue.map ({
                $0["name"].stringValue
            }) else {return 0}
            name.append("All")
            return name.count
        }
    }
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == relationshipPicker {
            return brokerarr[row]
        }
        else {
        guard var name = self.users.data?["result"]["data"].arrayValue.map ({
            $0["name"].stringValue
        }) else {return ""}
        name.append("All")
        return name[row]
        }
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case relationshipPicker:
            relationshiptf.text = brokerarr[row]
            users.network(levelNo: brokerarrid[row])
        default:
            guard var name = self.users.data?["result"]["data"].arrayValue.map ({
                $0["name"].stringValue
            }) else {return }
            name.append("All")
            membertf.text = name[row]
            if  membertf.text == "All" {
                           self.uid = ""
                           self.levelid = ""
               
                       }
                       else {
                           guard let uid = self.users.data?["result"]["data"][row]["uid"].stringValue else {return}
                           guard let levelid = self.users.data?["result"]["data"][row]["levelNo"].stringValue else {return}
                           self.uid = uid
                           self.levelid = levelid
                
                       }
            
        }
    }
    
}
