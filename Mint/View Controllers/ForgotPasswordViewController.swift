//
//  ForgotPasswordViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 18/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
class ForgotPasswordViewController: UIViewController , Completion {
   
    
    @IBOutlet weak var nametf: UITextField!
    var forgot = ForgotPassword()
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.setNavigationBarHidden(false, animated: false)
               navigationController?.navigationBar.isHidden = false
               navigationController?.navigationBar.prefersLargeTitles = false
               self.title = "Forgot Password"
        forgot.delegate = self
    }
    @IBAction func submit(_ sender: Any) {
        if UITextField.validateAll(textFields: [nametf]) {
        guard let name = nametf.text else {
            return
        }
        forgot.forgot(name: name)
    }
        else {
            self.alert(message: "Please Enter Username")
        }
    }
    func complete(data: JSON?) {
        self.alert(message: "Check your Email for reset password", title: "Reset")
       }
}
