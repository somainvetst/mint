//
//  LoginViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import TextFieldEffects
import Alamofire
import NVActivityIndicatorView
class LoginViewController: UIViewController , Success , back , goback   {
  
    @IBOutlet weak var headV: HeaderView!
   
    let login = Login()
    let network = Network()
   
    @IBOutlet weak var backbt: UIButton!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activity: NVActivityIndicatorView!
    @IBOutlet weak var enterPassword: HoshiTextField!
    @IBOutlet weak var enterUsername: HoshiTextField!
    @IBOutlet weak var enterBrokerId: HoshiTextField!
    @IBOutlet weak var eye: UIButton!
     var iconClick = true
    var id:Int = 0
    override func viewDidLoad() {
        super.viewDidLoad()
         navigationController?.navigationBar.isHidden = true
    //    print("dd" , UserDefaults.standard.value(forKey: "cux"))
       activityView.isHidden = true
        network.delegate = self
        if id == 1 {
            backbt.isHidden = false
        }
        else {
            backbt.isHidden = true
        }
     
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
    }
    func goback() {
       
        navigationController?.popViewController(animated: true)
    }
   
    @IBAction func forgotPassword(_ sender: Any) {
        let vc: ForgotPasswordViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "ForgotPasswordViewController") as! ForgotPasswordViewController
              navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    
    
    func goBack() {
        print("here")
        
      
        navigationController?.popViewController(animated: true)
        
    }
    func getLogin(email:String , password:String , brokerdomain:String) {
        UserDefaults.standard.setValue(brokerdomain, forKey: "DomainName")
        guard let domainName = UserDefaults.standard.value(forKey: "DomainName") as? String else {return}
       
         let emailTouse = (email).trailingTrim()
       let brokerIdTouse = (domainName).trailingTrim()
         Shared.sharedInstance.baseUrl = "https://\(brokerIdTouse).investwell.app/api/"
        let url = "\(Shared.sharedInstance.baseUrl)auth/login"
        let parameters: Parameters = ["email": emailTouse , "password": password , "brokerDomain": brokerIdTouse]
        network.hitServer(url: url, parameters: parameters, method: .post , type: "Login", brokerdomain: brokerIdTouse, headers: ["nil":"nil"])
        
    }
    func successfull() {
        if network.data?["result"]["token"].stringValue.isEmpty == false {
                 let vc: OtpViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "OtpViewController") as!OtpViewController
            vc.token = network.data?["result"]["token"].stringValue ?? ""
            vc.mob = network.data?["result"]["mobile"].stringValue ?? ""
                        navigationController?.pushViewController(vc, animated: true)
             }
             else {
       Routes.shared.gotoPin(nav: navigationController!)
        }
    }
    func completed() {
        activityView.isHidden = true
        activity.stopAnimating()
    }
    func error(msg: String) {
        self.alert(message: msg, title: "Error")
    }
    @IBAction func login(_ sender: Any) {
        activityView.isHidden = false
        activity.startAnimating()
        guard let email = enterUsername.text , let password = enterPassword.text , let brokerid = enterBrokerId.text  else {
            return
        }
        UserDefaults.standard.setValue(email, forKey: "email")
        UserDefaults.standard.setValue(password, forKey: "password")
        UserDefaults.standard.setValue(brokerid, forKey: "brokerid")
        getLogin(email: email, password: password, brokerdomain: brokerid)
    }
    @IBAction func passwordShowHide(_ sender: Any) {
        if(iconClick == true) {
            enterPassword.isSecureTextEntry = false
            eye.setImage(UIImage(named: "eyeon"), for: .normal)
        } else {
            enterPassword.isSecureTextEntry = true
            eye.setImage(UIImage(named: "eyeoff"), for: .normal)
        }
        iconClick = !iconClick
    }
    
}
