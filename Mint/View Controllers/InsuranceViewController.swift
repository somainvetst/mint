//
//  InsuranceViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
class InsuranceViewController: UIViewController , Completion {
    let selection = UISelectionFeedbackGenerator()
    @IBOutlet weak var emptyState: EmptyScreen!
    @IBOutlet weak var segments: UISegmentedControl!
    @IBOutlet weak var table: UITableView!
    var network = Insurance()
    var id = "L"
     var delegates:dismissing?
    override func viewDidLoad() {
        super.viewDidLoad()
        network.delegate = self
        emptyState.isHidden = true
         //navigation
        delegates?.close()
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = false
        self.title = "Insurance"
        self.navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = true
        self.navigationItem.hidesBackButton = false
        
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        
        
        //table
        table.tableFooterView = UIView()
        table.register(cell: FolioLookupTableViewCell.self)
        
        //network call
        network.network(table: table, id: id)
    }
    
    func complete(data: JSON?) {
          
      }
      
      
    @objc func back(sender: UIBarButtonItem) {
        
        dismiss(animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = false
        self.title = "Insurance"
        self.navigationController?.navigationBar.tintColor = UIColor.black
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = true
        self.navigationItem.hidesBackButton = false
    }
    @IBAction func segmentChanged(_ sender: Any) {
        selection.selectionChanged()
        switch segments.selectedSegmentIndex {
        case 0:
           id = "L"
           network.network(table: table, id: id)
        case 1:
          id = "M"
        network.network(table: table, id: id)
        default:
           id = "G"
        network.network(table: table, id: id)
        }
    }
    
    
}
extension InsuranceViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if network.data?["result"]["data"].count == 0 {
            emptyState.isHidden = false
            tableView.isHidden = true
        }
        else {
            emptyState.isHidden = true
            tableView.isHidden = false
        }
        return network.data?["result"]["data"].count ?? 0
          
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: FolioLookupTableViewCell.self)
         cell?.sidemenuButton.isHidden = true
        cell?.amount.text = "Issue Date"
        cell?.folio.text = "Policy Number"
        cell?.ucc.text = "Renewal Date"
        cell?.investor.text = "Premium"
        cell?.holding.text = "Sum Assured"
        cell?.pan.text = "Status"
        cell?.amountValue.text = network.data?["result"]["data"][indexPath.row]["issuingDate"].stringValue.toDate()
        cell?.foliovalue.text =  network.data?["result"]["data"][indexPath.row]["policyNumber"].stringValue
        cell?.uccValue.text = network.data?["result"]["data"][indexPath.row]["renewalDate"].stringValue.toDate()
        cell?.investorvalue.text = network.data?["result"]["data"][indexPath.row]["premiumAmount"].stringValue.toCurrencyFormat()
        cell?.holdingvalue.text = network.data?["result"]["data"][indexPath.row]["sumAssured"].stringValue.toCurrencyFormat()
        cell?.panvalue.text = network.data?["result"]["data"][indexPath.row]["status"].stringValue
        cell?.name.text =  network.data?["result"]["data"][indexPath.row]["investorName"].stringValue
        cell?.schemeName.text =  network.data?["result"]["data"][indexPath.row]["policyName"].stringValue
        return cell ?? UITableViewCell()
    }
}

extension InsuranceViewController : UITableViewDelegate {}
