//
//  ShareBondViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class ShareBondViewController: UIViewController {
 
  
    @IBOutlet weak var table: UITableView!
    var share = ShareBond()
    var id:String = ""
    var appid:String = ""
    var schid:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
          table.tableFooterView = UIView()
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = false
        self.title = "Share & Bond"
        table.register(cell: DashboardTableViewCell.self)
        table.register(cell: ShareBondTableViewCell.self)
        share.network(table: table, id: id)
        share.shareDetails(table: table, id: "")
    }
  
   
}
extension ShareBondViewController : UITableViewDelegate {}

extension ShareBondViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return share.data1?["result"]["data"].count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: DashboardTableViewCell.self)
            cell?.marketChangeImg.isHidden = true
            cell?.MarketChange.isHidden = true
            cell?.backgroundImage.image = UIImage(named: "card_blank")
            cell?.loginView.isHidden = true
            cell?.detailsView.isHidden = false
            cell?.detailsView.backgroundColor = .clear
            
            cell?.bottomHead1.text = "Purchase Cost"
            cell?.bottomhead2.text = "Gain"
            cell?.bottomhead3.text = "CAGR"
            cell?.market.text = "Market Value"
            cell?.nameofClient.text = Shared.sharedInstance.clientName
            guard let market = share.data?["result"]["summary"]["currentValue"].stringValue , let bottom1 = share.data?["result"]["summary"]["purchaseValue"].stringValue , let bottom2 = share.data?["result"]["summary"]["gain"].stringValue , let bottom3 = share.data?["result"]["summary"]["CAGR"].stringValue  else { return UITableViewCell()}
            guard let marketvalue = Double(market)?.roundtoPlace(places: 2) , let bottom1round = Double(bottom1)?.roundtoPlace(places: 2) , let bottom2round = Double(bottom2)?.roundtoPlace(places: 2)  , let bottom3round = Double(bottom3)?.roundtoPlace(places: 2)  else {return UITableViewCell() }
            cell?.marketValue.text = String(marketvalue).toCurrencyFormat()
            cell?.bottomValue1.text = String(bottom1round).toCurrencyFormat()
            cell?.bottomValue2.text = String(bottom2round).toCurrencyFormat()
            cell?.bottomValue3.text = "\(String(bottom3round))%"
            return cell ?? UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(with: ShareBondTableViewCell.self)
            cell?.topleft.text = "Purchase Value"
            cell?.topmiddle.text = "Current Value"
            cell?.topRight.text = "CAGR"
            cell?.bottomleft.text = "Purchase Date"
            cell?.bottommiddle.text = "Gain"
            cell?.bottomright.text = "Holding days"
            cell?.lowerLeft.text = "Dividend:"
            cell?.lowerRight.text = "Absolute Return"
            var sname = share.data1?["result"]["data"][indexPath.row]["schemeName"].stringValue
                                 if let range = sname?.range(of: "(") {
                                     sname = sname?[..<range.lowerBound].trimmingCharacters(in: .whitespaces)
                                                                   }
            cell?.headLabel.text = sname
            cell?.topLeftvalue.text = share.data1?["result"]["data"][indexPath.row]["purchaseValue"].stringValue.toCurrencyFormat()
            cell?.topMiddlevalue.text = share.data1?["result"]["data"][indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
            cell?.topRightValue.text = "\(share.data1?["result"]["data"][indexPath.row]["CAGR"].stringValue.roundOffString() ?? "0")%"
            var startdate =  share.data1?["result"]["data"][indexPath.row]["purchaseDate"].stringValue
           
            cell?.bottomLeftValue.text = startdate?.toDate()
            cell?.bottomMiddleValue.text = share.data1?["result"]["data"][indexPath.row]["gain"].stringValue.toCurrencyFormat()
            var days = share.data1?["result"]["data"][indexPath.row]["avgHoldingDays"].stringValue
                      if let range = days?.range(of: ".") {
                          days = days?[..<range.lowerBound].trimmingCharacters(in: .whitespaces)
                                                        }
            cell?.bottomRightValue.text = days
            
            
            cell?.lowerLeftValue.text = share.data1?["result"]["data"][indexPath.row]["dividend"].stringValue.toCurrencyFormat()
            cell?.lowerRightValue.text = "\(share.data1?["result"]["data"][indexPath.row]["absoluteReturn"].stringValue.roundOffString() ?? "0")%"
            return cell ?? UITableViewCell()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
