//
//  TrabsactionTypeFilterViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 11/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class TrabsactionTypeFilterViewController: UIViewController {
       let selection = UISelectionFeedbackGenerator()
    @IBOutlet weak var filterType: UISegmentedControl!
    @IBOutlet weak var table: UITableView!
    static var unselectedFilter:[String] = []
    var purchasetypes:[String] = [ "Purchase" , "SIP" , "STI" , "Switch In" , "Div Reinvestment" , "Bonus"]
    var sellTypes:[String] = ["Sell" , "STO" , "Switch Out" , "SWP" , "Div Payout"]
    override func viewDidLoad() {
        super.viewDidLoad()
        table.tableFooterView = UIView()
        self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.done, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.rightBarButtonItem = newBackButton
    }
    
    @objc func back(sender: UIBarButtonItem) {
        print( TrabsactionTypeFilterViewController.unselectedFilter)
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func TransactionSegment(_ sender: Any) {
         selection.selectionChanged()
        table.reloadData()
    }
    
}
extension TrabsactionTypeFilterViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch filterType.selectedSegmentIndex {
        case 0:
            return purchasetypes.count
        default:
            return sellTypes.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionTypeFilterTableViewCell", for: indexPath) as? TransactionTypeFilterTableViewCell
        switch filterType.selectedSegmentIndex {
        case 0:
            cell?.typeName.text = purchasetypes[indexPath.row]
            if Shared.sharedInstance.purchaseArray[indexPath.row] == 1 {
                cell?.accessoryType = .checkmark
            }
            else {
                cell?.accessoryType = .none
            }
        default:
            cell?.typeName.text = sellTypes[indexPath.row]
            if Shared.sharedInstance.sellArray[indexPath.row] == 1 {
                cell?.accessoryType = .checkmark
            }
            else {
                cell?.accessoryType = .none
            }
            
        }
        return cell ?? UITableViewCell()
    }
    
    
}
extension TrabsactionTypeFilterViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? TransactionTypeFilterTableViewCell
        switch filterType.selectedSegmentIndex {
        case 0:
            
          
            if Shared.sharedInstance.purchaseArray[indexPath.row] == 1 {
                Shared.sharedInstance.purchaseArray[indexPath.row] = 0
                cell?.accessoryType = .none
                  let type = cell?.typeName.text?.transactionToShortForm()
                TrabsactionTypeFilterViewController.unselectedFilter.append(type ?? "")
            }
            else {
                Shared.sharedInstance.purchaseArray[indexPath.row] = 1
                cell?.accessoryType = .checkmark
                  let type = cell?.typeName.text?.transactionToShortForm()
                let indexes = TrabsactionTypeFilterViewController.unselectedFilter.firstIndex(of: (type)!)
                TrabsactionTypeFilterViewController.unselectedFilter.remove(at: indexes!)
            }
            
        default:
            if Shared.sharedInstance.sellArray[indexPath.row] == 1 {
                Shared.sharedInstance.sellArray[indexPath.row] = 0
                cell?.accessoryType = .none
                  let type = cell?.typeName.text?.transactionToShortForm()
                  TrabsactionTypeFilterViewController.unselectedFilter.append(type ?? "")
            }
            else {
                Shared.sharedInstance.sellArray[indexPath.row] = 1
                cell?.accessoryType = .checkmark
                  let type = cell?.typeName.text?.transactionToShortForm()
                let indexes = TrabsactionTypeFilterViewController.unselectedFilter.firstIndex(of: (type)!)
                TrabsactionTypeFilterViewController.unselectedFilter.remove(at: indexes!)
            }
            
          
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? TransactionTypeFilterTableViewCell
        switch filterType.selectedSegmentIndex {
        case 0:
            
            
            if Shared.sharedInstance.purchaseArray[indexPath.row] == 1 {
                Shared.sharedInstance.purchaseArray[indexPath.row] = 0
                cell?.accessoryType = .none
                let type = cell?.typeName.text?.transactionToShortForm()
                 TrabsactionTypeFilterViewController.unselectedFilter.append(type ?? "")
            }
            else {
                Shared.sharedInstance.purchaseArray[indexPath.row] = 1
                cell?.accessoryType = .checkmark
                  let type = cell?.typeName.text?.transactionToShortForm()
                let indexes = TrabsactionTypeFilterViewController.unselectedFilter.firstIndex(of: type!)
                               TrabsactionTypeFilterViewController.unselectedFilter.remove(at: indexes!)
            }
            
        default:
            if Shared.sharedInstance.sellArray[indexPath.row] == 1 {
                Shared.sharedInstance.sellArray[indexPath.row] = 0
                cell?.accessoryType = .none
                  let type = cell?.typeName.text?.transactionToShortForm()
                 TrabsactionTypeFilterViewController.unselectedFilter.append(type ?? "")
            }
            else {
                Shared.sharedInstance.sellArray[indexPath.row] = 1
                cell?.accessoryType = .checkmark
                  let type = cell?.typeName.text?.transactionToShortForm()
                let indexes = TrabsactionTypeFilterViewController.unselectedFilter.firstIndex(of: (type)!)
                               TrabsactionTypeFilterViewController.unselectedFilter.remove(at: indexes!)
            }
            
           
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
}
