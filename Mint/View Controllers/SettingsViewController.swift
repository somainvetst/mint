//
//  SettingsViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 26/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var table: UITableView!
    var heading:String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        table.register(cell: Settings1TableViewCell.self)
        table.register(cell: Settings2TableViewCell.self)
        table.register(cell: Settings3TableViewCell.self)
        table.tableFooterView = UIView()
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.tabBarController?.navigationController?.isToolbarHidden = true
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.topItem?.title = "Settings"
         navigationController?.navigationBar.topItem?.searchController = nil
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        self.tabBarController?.navigationItem.hidesBackButton = true
        self.tabBarController?.navigationItem.titleView = nil
        
        
        
    }
    final func logout() {
        let title =  "Logout"
        let message = "Are you sure , you want to logout"
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        let okAction = UIAlertAction(title: "Logout", style: .default) { (action) in
            Shared.sharedInstance.clear()
            CoreData.core.deleteAllData(entity: "Logindata")
            let cookieStore = HTTPCookieStorage.shared
            for cookie in cookieStore.cookies ?? [] {
                cookieStore.deleteCookie(cookie)
            }
            UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
            Routes.shared.gotoLogin(nav: self.navigationController! , id: 0)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        if let popOverPresentation = alertController.popoverPresentationController{
            popOverPresentation.sourceView = self.view
            
        }
        
        
        present(alertController, animated: true, completion: nil)
    }
    
}
extension SettingsViewController : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch Shared.sharedInstance.id {
        case 1...10 :
            return 2
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ar = ["Support" , "Logout"]
        let im = [UIImage(named: "help") , UIImage(named: "logout")]
        switch Shared.sharedInstance.id  {
        case 1...10:
            switch indexPath.section {
            case 0:
                let cell = tableView.dequeueReusableCell(with: Settings3TableViewCell.self)
                cell?.imageV.image = im[indexPath.row]
                cell?.name.text = ar[indexPath.row]
                return cell ?? UITableViewCell()
                
            default:
                let cell = tableView.dequeueReusableCell(with: Settings3TableViewCell.self)
                cell?.imageV.image = UIImage(named: "logout")
                cell?.name.text = "Logout"
                return cell ?? UITableViewCell()
            }
        default:
            switch indexPath.section {
                //        case 0:
                //            let cell = tableView.dequeueReusableCell(with: Settings1TableViewCell.self)
                //            cell?.imageV.image = UIImage(named: "screen_lock")
                //            cell?.name.text = "Change Screen Lock"
                //            cell?.name2.text = "None"
                //            return cell ?? UITableViewCell()
                //        case 1:
                //            let cell = tableView.dequeueReusableCell(with: Settings2TableViewCell.self)
                //            cell?.imageV.image = UIImage(named: "finger_print")
                //            cell?.name.text = "Fingerprint"
            //            return cell ?? UITableViewCell()
            default:
                let cell = tableView.dequeueReusableCell(with: Settings3TableViewCell.self)
                cell?.imageV.image = UIImage(named: "logout")
                cell?.name.text = "Logout"
                return cell ?? UITableViewCell()
            }
            
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch Shared.sharedInstance.id  {
        case 1...10:
            switch indexPath.row {
            case 0:
                let vc: WebViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                vc.heading = "Support"
                vc.urlLink = "https://investwell.freshdesk.com/support/home"
                navigationController?.pushViewController(vc, animated: true)
            default:
                logout()
            }
        default:
            switch indexPath.section {
                
            default:
                logout()
                
            }
        }
        
    }
}
