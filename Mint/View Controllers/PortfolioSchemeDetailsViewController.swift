//
//  PortfolioSchemeDetailsViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 19/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

import SwiftyJSON
class PortfolioSchemeDetailsViewController: UIViewController , back , Completion  {
    
    var transaction = GetSchemeTransactionType()
    var datas:PassedSchemeForTransaction?
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var headView: HeaderView!
    var levelid:String = ""
    var uid:String = ""
    var schid:String = ""
    var data:JSON?
    var folioId:String = ""
    var folioNum:String = ""
    var uccNum:String = ""
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var fundnamer: UILabel!
    var network = GetClientPortfolioDetails()
    override func viewDidLoad() {
        super.viewDidLoad()
        table.tableFooterView = UIView()
        network.delegate = self
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.isHidden = false
        
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = false
        self.navigationItem.hidesBackButton = true
        self.title = "Portfolio"
        self.navigationController?.navigationBar.tintColor = UIColor.black
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
        self.navigationItem.leftBarButtonItem = newBackButton
        table.register(cell: PortfolioTransactionDetailsTableViewCell.self)
        table.register(cell: TransactionTypeTableViewCell.self)
        table.register(cell: PortfolioScheme1TableViewCell.self)
        table.register(cell: PortfolioScheme2TableViewCell.self)
        transaction.network(table: table, schid: schid, bid: Shared.sharedInstance.bid , levelid: Shared.sharedInstance.levelidMain)
        network.detailedPortfolio(table: table, id: uid, folioId: folioId)
        Transactiondata.shared.folioid = folioId
        Transactiondata.shared.folioNum = folioNum
        Transactiondata.shared.ucc = uccNum
        
    }
    override func viewWillAppear(_ animated: Bool) {
    }
    func complete(data: JSON?) {
        clientName.text = network.data?["result"]["rows"][0]["applicantName"].stringValue
        fundnamer.text = network.data?["result"]["rows"][0]["schemeName"].stringValue
        Transactiondata.shared.name = network.data?["result"]["rows"][0]["applicantName"].stringValue ?? ""
        Transactiondata.shared.schemename = network.data?["result"]["rows"][0]["schemeName"].stringValue ?? ""
        let schid = network.data?["result"]["rows"][0]["schid"].stringValue ?? ""
        Transactiondata.shared.schid = schid
        print(schid)
        table.reloadData()
        
    }
    
    func goBack() {
        uccNum = ""
        navigationController?.popViewController(animated: true)
    }
    @objc func back(sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    func setStructRef(ref:PassedSchemeForTransaction) {
        self.datas = ref
    }
}
extension PortfolioSchemeDetailsViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1 :
            return 1
        default:
            
            return network.data?["result"]["rows"].count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: PortfolioScheme1TableViewCell.self)
            cell?.headLabel1.text = "Purchase Cost"
            cell?.headLabel2.text = "Market Value"
            cell?.midLabe1.text = "Folio"
            cell?.misLabel2.text = "Gain"
            cell?.midLabel3.text = "Balance Units"
            cell?.bottomLabel1.text = "Avg.Holding days"
            cell?.bottomLabel2.text = "Abs. return"
            cell?.bottomLabel3.text = "CAGR"
            cell?.headValue1.text = network.data?["result"]["summary"]["purchaseValue"].stringValue.toCurrencyFormat()
            cell?.headValue2.text = network.data?["result"]["summary"]["currentValue"].stringValue.toCurrencyFormat()
            cell?.midvalue1.text = folioNum
            cell?.midValue2.text = network.data?["result"]["summary"]["gain"].stringValue.toCurrencyFormat()
            cell?.midValue3.text = network.data?["result"]["summary"]["balanceUnits"].stringValue.roundOffString()
            let days = network.data?["result"]["summary"]["avgHoldingDays"].stringValue
            var rounddays = Double(days ?? "0")
            rounddays = rounddays?.rounded()
            let round = Int(rounddays!)
            cell?.bottomValue1.text = String(round)
            cell?.bottomValue2.text = "\(network.data?["result"]["summary"]["absoluteReturn"].stringValue.roundOffString() ?? "0")%"
            cell?.bottomvalue3.text = "\(network.data?["result"]["summary"]["CAGR"].stringValue.roundOffString() ?? "0")%"
            return cell ?? UITableViewCell()
        case 1:
            if uccNum == "" {
                return UITableViewCell()
            }
            else {
            let cell = tableView.dequeueReusableCell(with: TransactionTypeTableViewCell.self)
                        cell?.count = transaction.data?["result"]["allowedInvestmentTypes"].count ?? 0
                        cell?.nav = navigationController!
            cell?.collectionHeight.constant = 250

                        guard let ty = (transaction.data?["result"]["allowedInvestmentTypes"]) else { return UITableViewCell() }
                        cell?.toolName = (ty.arrayObject as? [String] ?? [""])
                        cell?.sipDates = transaction.data?["result"]["sipDates"].arrayObject as? [String] ?? [""]
                        cell?.stpDates = transaction.data?["result"]["sipDates"].arrayObject as? [String] ?? [""]
                        cell?.swpDates = transaction.data?["result"]["swpDates"].arrayObject as? [String] ?? [""]
                         cell?.frequency = transaction.data?["result"]["frequencies"].arrayObject as? [String] ?? [""]
                        cell?.toolImage = [UIImage](repeatElement(UIImage(named: "stp2")!, count: transaction.data?["result"]["allowedInvestmentTypes"].count ?? 0))
            cell?.collectionView.reloadData()

            return cell ?? UITableViewCell()
                // return UITableViewCell()
        }
        default:
            let cell = tableView.dequeueReusableCell(with: PortfolioScheme2TableViewCell.self)
            cell?.headLabel.text = network.data2?[indexPath.row]["txnType"].stringValue.mappedTransactionType()
            cell?.topLabel1.text = "Purchase Cost"
            cell?.topLabel2.text = "Current Value"
            cell?.topLabel3.text = "CAGR"
            cell?.midLabel1.text = "Purchase Date"
            cell?.midLabel2.text = "Gain"
            cell?.midLabel3.text = "Holding Days"
            cell?.bottomLeftlabel.text = "Dividend:"
            cell?.bottomRightLabel.text = "Absolute Return:"
            cell?.topValue1.text = network.data2?[indexPath.row]["purchaseValue"].stringValue.toCurrencyFormat()
            cell?.topvalue2.text = network.data2?[indexPath.row]["currentValue"].stringValue.toCurrencyFormat()
            cell?.topvalue3.text = "\(network.data2?[indexPath.row]["CAGR"].stringValue.roundOffString() ?? "0")%"
            let startdate = network.data2?[indexPath.row]["purchaseDate"].stringValue
            cell?.midValue1.text = startdate?.toDate()
            cell?.midValue2.text = network.data2?[indexPath.row]["gain"].stringValue.toCurrencyFormat()
            let days = network.data2?[indexPath.row]["avgHoldingDays"].stringValue
            var rounddays = Double(days ?? "0")
            rounddays = rounddays?.rounded()
            let round = Int(rounddays!)
            cell?.midValue3.text = String(round)
            cell?.bottomLeftValue.text = network.data2?[indexPath.row]["dividend"].stringValue
            cell?.bottomRightValue.text = "\(network.data2?[indexPath.row]["absoluteReturn"].stringValue.roundOffString() ?? "0")%"
            return cell ?? UITableViewCell()
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return UITableView.automaticDimension
//    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let headerView = view as? UITableViewHeaderFooterView {
            headerView.contentView.backgroundColor = .white
            
        }
    }
}

