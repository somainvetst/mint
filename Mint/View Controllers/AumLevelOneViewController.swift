//
//  AumLevelOneViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 30/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import NVActivityIndicatorView
class AumLevelOneViewController: UIViewController , Completion {
  @IBOutlet weak var activityView: UIView!
     @IBOutlet weak var activity: NVActivityIndicatorView!
     var delegates:dismissing?
    var network = Aum()
    @IBOutlet weak var table: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        activityView.isHidden = false
              activity.startAnimating()
         table.tableFooterView = UIView()
        table.register(cell: PortfolioScheme1TableViewCell.self)
        table.register(cell: AumTableViewCell.self)
       
        network.delegate = self
        network.network(table: table)
          }
    
   @objc func back(sender: UIBarButtonItem) {
               delegates?.close()
               UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
          }
    
    func complete(data: JSON?) {
           activityView.isHidden = true
                activity.stopAnimating()
      }
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
                      navigationController?.navigationBar.prefersLargeTitles = false
                      navigationController?.navigationBar.topItem?.title = "AUM Report"
         navigationController?.navigationBar.topItem?.searchController = nil
                     self.navigationItem.hidesBackButton = true
        self.tabBarController?.navigationItem.titleView = nil
                     let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                     self.navigationItem.leftBarButtonItem = newBackButton
    }
      
    
}
extension AumLevelOneViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0 :
            return 1
        default:
            return network.data?["result"]["data"].count ?? 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: PortfolioScheme1TableViewCell.self)
            cell?.headLabel2.isHidden = true
            cell?.headValue2.isHidden = true
            cell?.bottomLabel3.text = ""
            cell?.bottomvalue3.text = ""
            cell?.headLabel1.text = "Total"
            cell?.midLabe1.text = "Debt"
            cell?.misLabel2.text = "Equity"
            cell?.midLabel3.text = "Other"
            cell?.bottomLabel1.text = "Liquied & Ultra short"
            cell?.bottomLabel2.text = "Arbitrage"
          
                cell?.headValue1.text = network.data?["result"]["summary"]["AUM"].stringValue.toCurrencyFormat()
            cell?.midvalue1.text = network.data?["result"]["summary"]["Debt"].stringValue.toCurrencyFormat()
            cell?.midValue2.text = network.data?["result"]["summary"]["Equity"].stringValue.toCurrencyFormat()
            cell?.midValue3.text = network.data?["result"]["summary"]["Other"].stringValue.toCurrencyFormat()
            cell?.bottomValue1.text = network.data?["result"]["summary"]["Liquid and Ultra Short"].stringValue.toCurrencyFormat()
            cell?.bottomValue2.text = network.data?["result"]["summary"]["Arbitrage"].stringValue.toCurrencyFormat()
            return cell ?? UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(with: AumTableViewCell.self)
            cell?.bottomLabel.text = "Amount"
            cell?.bottomLabel2.text = "Allocation"
            cell?.bottomvalue.text = network.data?["result"]["data"][indexPath.row]["AUM"].stringValue.toCurrencyFormat()
            cell?.bottomvalue2.text = "\(network.data?["result"]["data"][indexPath.row]["allocation"].stringValue.roundOffString() ?? "")%"
              cell?.name.text = network.data?["result"]["data"][indexPath.row]["name"].stringValue
            return cell ?? UITableViewCell()
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
}
extension AumLevelOneViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let fundid = network.data?["result"]["data"][indexPath.row]["fundid"].stringValue
          let vc: AumLevelTwoViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "AumLevelTwoViewController") as! AumLevelTwoViewController;
        vc.sname = network.data?["result"]["data"][indexPath.row]["name"].stringValue ?? ""
        vc.fundis = fundid ?? ""
        navigationController?.pushViewController(vc, animated: true)
    }
}
