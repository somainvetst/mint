//
//  FolioLookupViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 22/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import ListPlaceholder
protocol dismissing:class {
    func close()
}
class FolioLookupViewController: UIViewController , UITextFieldDelegate , tappedfolio , CompletedFolio , UISearchBarDelegate , UISearchResultsUpdating {
  
    @IBOutlet weak var EmptyScreen: EmptyScreen!
    @IBOutlet weak var searchtf: UITextField!
    var delegate:Menu?
    var delegates:dismissing?
    @IBOutlet weak var shimmerLoader: ShimmerView!
    @IBOutlet weak var table: UITableView!
     let search = UISearchController(searchResultsController: nil)
    var folio = FolioLookup()
    var uccCheck = GetUccInnList()
    var schemeName:String = ""
    var type = ""
   
    override func viewDidLoad() {
        super.viewDidLoad()
        EmptyScreen.isHidden = true
         self.extendedLayoutIncludesOpaqueBars = true
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.isToolbarHidden = true
        
          table.tableFooterView = UIView()
        shimmerLoader.isHidden = false
                         shimmerLoader.startShimmeringAnimation()
        self.title = "Folio Lookup"
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.searchController = search
        self.navigationItem.searchController!.searchBar.delegate = self
        self.navigationItem.searchController!.searchResultsUpdater = self
        self.search.searchBar.placeholder = "Search Scheme"
        self.definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationController?.navigationBar.tintColor = UIColor.black
      
        Loader.addLoaderToTableView(table: self.table) 
       // searchView.isHidden = true
        folio.delegate = self
    table.register(cell: FolioLookupTableViewCell.self)
    folio.network(table: table, schemename: schemeName)
   //     searchtf.delegate = self
       uccCheck.network(table: table , domain: Shared.sharedInstance.brokerDomain)
  
            self.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                     self.navigationItem.leftBarButtonItem = newBackButton
                 }

    @objc func back(sender: UIBarButtonItem) {
        
        if PinViewController.dynamicMenu == 0 {
            delegates?.close()
          UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: false, completion: nil)
        }
        else {
            PinViewController.dynamicMenu = 0
            Routes.shared.gotoTab(nav: navigationController!)
        }
                 }
   
    override func viewWillAppear(_ animated: Bool) {
        print(self.presentingViewController)
        self.extendedLayoutIncludesOpaqueBars = true
        self.title = "Folio Lookup"
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationItem.searchController = search
        self.navigationItem.searchController!.searchBar.delegate = self
        self.navigationItem.searchController!.searchResultsUpdater = self
        self.definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = false
        self.navigationController?.navigationBar.tintColor = UIColor.black
    }
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    static func configureDynamicShortcutItem2() {
           var shortcutItems = UIApplication.shared.shortcutItems ?? []
           if let existingShortcutItem = shortcutItems.first {
               guard let mutableShortcutItem = existingShortcutItem.mutableCopy() as? UIMutableApplicationShortcutItem
                   else { preconditionFailure("Expected a UIMutableApplicationShortcutItem") }
               guard let index = shortcutItems.index(of: existingShortcutItem)
                   else { preconditionFailure("Expected a valid index") }

               mutableShortcutItem.localizedTitle = "New Title"
               shortcutItems[index] = mutableShortcutItem
               UIApplication.shared.shortcutItems = shortcutItems
           }
              let type = Bundle.main.bundleIdentifier! + ".Dynamic"
                    
                    let item = UIApplicationShortcutItem.init(type: type, localizedTitle: "Portfolio Details", localizedSubtitle: "Portfolio Details", icon: UIApplicationShortcutIcon.init(type: .compose), userInfo: nil)
                    let item2 = UIApplicationShortcutItem.init(type: type, localizedTitle: "Transactions", localizedSubtitle: "Transaction Details", icon: UIApplicationShortcutIcon.init(type: .compose), userInfo: nil)
                    let item3 = UIApplicationShortcutItem.init(type: type, localizedTitle: "Folio Lookup", localizedSubtitle: "Folio", icon: UIApplicationShortcutIcon.init(type: .compose), userInfo: nil)
                  
                    UIApplication.shared.shortcutItems = [item , item2 , item3]
         }
    func completed() {
        shimmerLoader.isHidden = true
        self.table.hideLoader()
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let text = search.searchBar.text ?? ""
        folio.network(table: table, schemename: text)
    }
    func error(msg: String) {
        self.alert(message: msg, title: "Error")
    }
    
    
    
    @IBAction func searching(_ sender: Any) {
        guard let search = searchtf.text else {
            return
        }
        schemeName = search
        folio.network(table: table, schemename: schemeName)
    }
    
    
    
//    @IBAction func search(_ sender: Any) {
//        viewSlideInFromTopToBottom(view: headerView)
//
//        headerView.isHidden = true
//        searchView.alpha = 0
//        searchView.isHidden = false
//        UIView.animate(withDuration: 0.7, delay: 0, options: .curveEaseInOut, animations: {
//            self.searchView.alpha = 1.0
//        }) { (isCompleted) in
//        }
//    }
//
//    @IBAction func backFromSearch(_ sender: Any) {
//
//        viewSlideInFromRightToLeft(view: searchView)
//        searchView.isHidden = true
//        headerView.isHidden = false
//
//    }
    fileprivate func viewSlideInFromTopToBottom(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        view.layer.add(transition, forKey: kCATransition)
    }
    fileprivate func viewSlideInFromRightToLeft(view: UIView) -> Void {
        let transition:CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        view.layer.add(transition, forKey: kCATransition)
    }
    func tapped(index: IndexPath) {
        let alert  = UIAlertController()
       alert.view.tintColor = .black
        alert.addAction(UIAlertAction(title: "View SOA", style: .default , handler:{ (UIAlertAction)in
            let vc = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "StatementOfAccountViewController") as! StatementOfAccountViewController
            guard let folioid = self.folio.data?["result"]["data"][index.row]["folioid"].stringValue else {return}
            vc.folioid = folioid
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        
        alert.addAction(UIAlertAction(title: "View Folio Details", style: .default , handler:{ (UIAlertAction)in
            let vc = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "FolioDetailsViewController") as! FolioDetailsViewController
            guard let folioid = self.folio.data?["result"]["data"][index.row]["folioid"].stringValue else {return}
            vc.folioId = folioid
            vc.type = self.type
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension FolioLookupViewController : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if folio.data?["result"]["data"].count == 0 {
            EmptyScreen.isHidden = false
            table.isHidden = true
        }
        else {
            EmptyScreen.isHidden = true
            table.isHidden = false
        }
        return folio.data?["result"]["data"].count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(with: FolioLookupTableViewCell.self)
        cell?.index = indexPath
        cell?.delegate = self
        var typeValue = ""
        if uccCheck.data?["result"]["exchange"].intValue == 2 {
            type = "UCC"
            typeValue = folio.data?["result"]["data"][indexPath.row]["uccNumber"].stringValue ?? "Not Available"
        }
        else {
            type = "IIN"
             typeValue = folio.data?["result"]["data"][indexPath.row]["iinNumber"].stringValue ?? "Not Available"
        }
        guard let amount = folio.data?["result"]["data"][indexPath.row]["AUM"].stringValue else {return UITableViewCell()}
        guard let amountDouble = Double(amount)?.roundtoPlace(places: 1) else {return UITableViewCell()}
        cell?.name.isHidden = true
        cell?.schemeName.text = folio.data?["result"]["data"][indexPath.row]["schemeName"].stringValue
        cell?.amount.text = "Amount"
        cell?.amountValue.text = String(amountDouble).toCurrencyFormat()
        cell?.folio.text = "Folio"
        cell?.foliovalue.text = folio.data?["result"]["data"][indexPath.row]["folioNo"].stringValue
        cell?.ucc.text = type
        cell?.uccValue.text = typeValue
        cell?.holding.text = "Holding"
        let holding  =  folio.data?["result"]["data"][indexPath.row]["holding"].stringValue
        if holding == "SI" {
            cell?.holdingvalue.text = "Single"
        }
        else if holding == "JO" {
            cell?.holdingvalue.text = "Joint"
        }
        else if holding == "AS" {
            cell?.holdingvalue.text = "Anyone or Survivor"
        }
        else if holding == "ES" {
            cell?.holdingvalue.text = "Either or Survivor"
        }
        else {
            cell?.holdingvalue.text = "Not Available"
        }
       
        cell?.investor.text = "Investor"
        cell?.investorvalue.text = folio.data?["result"]["data"][indexPath.row]["investorName"].stringValue
        cell?.pan.text = "PAN"
        cell?.panvalue.text = folio.data?["result"]["data"][indexPath.row]["pan"].stringValue
       return cell ?? UITableViewCell()
    }
    
    
}
