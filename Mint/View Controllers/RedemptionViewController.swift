//
//  RedemptionViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 04/11/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
class RedemptionViewController: UIViewController , Completion {
 
    @IBOutlet weak var table: UITableView!
    var network = AdditionalInvestment()
    override func viewDidLoad() {
        super.viewDidLoad()
        table.register(cell: TransactionHeaderType1TableViewCell.self)
        network.delegate = self
        network.getbalanceUnits(table: table, id: Transactiondata.shared.folioid)
        table.tableFooterView = UIView()
        navigationController?.navigationBar.isHidden = false
                          navigationController?.navigationBar.prefersLargeTitles = false
                          self.title = "Redeem"
                         self.navigationItem.hidesBackButton = true
                         let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                         self.navigationItem.leftBarButtonItem = newBackButton
        }
        
        @objc func back(sender: UIBarButtonItem) {
           navigationController?.popViewController(animated: true)
                 }
    
    func complete(data: JSON?) {
         
     }
     


}
extension RedemptionViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch  indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(with: TransactionHeaderType1TableViewCell.self)
            cell?.name.text = Transactiondata.shared.name
            cell?.schemeName.text = Transactiondata.shared.schemename
            cell?.shortTerm.text = "Short Term"
            cell?.longTerm.text = "Long Term"
            cell?.longTermUnits.text =  network.data?["result"]["longTerm"]["units"].stringValue
            cell?.longTermAmount.text = network.data?["result"]["longTerm"]["amount"].stringValue.toCurrencyFormat()
            cell?.shortTermUnits.text = network.data?["result"]["shortTerm"]["units"].stringValue
            cell?.shortTermamount.text = network.data?["result"]["shortTerm"]["amount"].stringValue.toCurrencyFormat()
            return cell ?? UITableViewCell()
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "RedeemTableViewCell") as! RedeemTableViewCell
            return cell
        }
       
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
}
extension RedemptionViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
