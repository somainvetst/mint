//
//  CapitalGainViewController.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 23/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import DropDown
class CapitalGainViewController: UIViewController , UITextFieldDelegate , Savepdf , back{
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activity: NVActivityIndicatorView!
    @IBOutlet weak var headerView: HeaderView!
    @IBOutlet weak var reporttf: UITextField!
    @IBOutlet weak var fyeartf: UITextField!
    @IBOutlet weak var clienttf: UITextField!
    var reportdropdown = DropDown()
    var fyeardropDown = DropDown()
    var clientdropDown = DropDown()
    var uid:String = ""
    var levelid:String = ""
    var users = GetUsers()
    var dateArray:[String] = []
    var year:String = ""
    var id:String = ""
    var pdftype:String = ""
    var capital = DownloadCapitalReport()
     var delegates:dismissing?
    override func viewDidLoad() {
        super.viewDidLoad()
        
          navigationController?.navigationBar.isHidden = false
               navigationController?.navigationBar.prefersLargeTitles = false
               self.title = "Capital Gain Realized"
                 self.navigationController?.navigationBar.tintColor = UIColor.black
       activityView.isHidden = true
      
        reporttf.delegate = self
        fyeartf.delegate = self
        clienttf.delegate = self
        let currentdate = Date()
        let calendar = Calendar.current
        let year =  calendar.component(.year, from: currentdate)
        let lastYear = year - 10
        dateArray = (lastYear...year).map { String($0) }
        users.network(levelNo: "100")
        capital.delegate = self
        if Shared.sharedInstance.clientType.isEmpty == true {
        if CoreData.core.data.isEmpty == false {
            
            let result = CoreData.core.data[0]
            let logindata = result.value(forKeyPath: "logindata") as? [String:AnyObject]
            
            guard let login = logindata else {
                return
            }
            
            guard  let id =  login["result"]?["uid"] as? String else {
                
                return
            }
            self.id = id
        }
        }
     self.navigationItem.hidesBackButton = true
                    let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.back(sender:)))
                                 self.navigationItem.leftBarButtonItem = newBackButton
                             }

                @objc func back(sender: UIBarButtonItem) {
                     delegates?.close()
                   dismiss(animated: true, completion: nil)
                             }
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.navigationBar.isHidden = false
              navigationController?.navigationBar.prefersLargeTitles = false
              self.title = "Capital Gain Realized"
                self.navigationController?.navigationBar.tintColor = UIColor.black
              navigationController?.navigationBar.backItem?.backBarButtonItem?.isEnabled = true
                    self.navigationItem.hidesBackButton = false
    }
    func goBack() {
        delegates?.close()
       dismiss(animated: true, completion: nil)
    }
    
    func logout() {
        Shared.sharedInstance.loggingout(nav: navigationController!)
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField {
        case reporttf:
            resignFirstResponder()
          let type = ["Summary" , "Detailed" , "ITR Report"]
            addTextOfDropdown(textf: reporttf , dropDown: reportdropdown, datasource: type)
        case fyeartf:
            var newArray = dateArray
            newArray.remove(at: 0)
           addTextOfDropdown(textf: fyeartf, dropDown: fyeardropDown, datasource: newArray)
        default:
           resignFirstResponder()
            guard let name = self.users.data?["result"]["data"].arrayValue.map ({
                $0["name"].stringValue
            }) else {return}
            print(name)
            addTextOfDropdown(textf: clienttf, dropDown: clientdropDown, datasource: name)
        }
    }
    
    @IBAction func download(_ sender: Any) {
        if UITextField.validateAll(textFields: [clienttf , fyeartf , reporttf]) {
        activityView.isHidden = false
        activity.startAnimating()
        if Shared.sharedInstance.clientType.isEmpty == true {
            capital.network(year: year, uid: uid, pdf: pdftype ) { (result: String , success: Bool) in
                if success {
                    print("yes")
                } }
        }
        else {
            capital.network(year: year, uid: uid, pdf: pdftype ) { (result: String , success: Bool) in
                if success {
                    print("yes")
                } }
        }
        
    }
        else {
            self.alert(message: "Please select valid option", title: "Error")
        }
    }
    func showActivity(vc: UIViewController) {
        activityView.isHidden = true
        activity.stopAnimating()
        present(vc, animated: true, completion: nil)
    }
    
    func addTextOfDropdown(textf:UITextField , dropDown:DropDown , datasource:[String]) {
       
       
        dropDown.anchorView = textf
        dropDown.dataSource = datasource
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            textf.text = item
            switch textf {
            case self.reporttf:
                self.pdftype = item.lowercased()
                if self.pdftype == "itr report" {
                    self.pdftype = "itrReport"
                }
                
                textf.resignFirstResponder()
            case self.fyeartf:
                let n = index + 1
                self.year = item
                textf.text = ("FY" + " " + self.dateArray[n - 1] + "-" + item)
                textf.resignFirstResponder()
            default:
                guard let uid = self.users.data?["result"]["data"][index]["uid"].stringValue else {return}
                guard let levelid = self.users.data?["result"]["data"][index]["levelid"].stringValue else {return}
                self.uid = uid
                self.levelid = levelid
                textf.resignFirstResponder()
            }
            
            
            
            
        }
        
    }
}
