//
//  ColorHeader.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class ColorHeader: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         self.layer.backgroundColor = Shared.sharedInstance.color.cgColor
    }
}
