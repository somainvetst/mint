//
//  CalculatorsCollectionViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class CalculatorsCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var verticalView: UIView!
    @IBOutlet weak var horizontalView: UIView!
    @IBOutlet weak var calculatorName: UILabel!
    @IBOutlet weak var calculatorImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

}
