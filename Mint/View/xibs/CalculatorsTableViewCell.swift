//
//  CalculatorsTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 12/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class CalculatorsTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    var nav = UINavigationController()
    var toolImage:[UIImage] = [UIImage(named: "sip")! , UIImage(named: "step")! , UIImage(named: "cost")! , UIImage(named: "edu")! , UIImage(named: "mar")! , UIImage(named: "ret")!]
    var toolName:[String] = ["Lumpsum Calculator" , "SIP Calculator" , "Cost of Delay SIP" ]
    var tool2Image:[UIImage] = [UIImage(named: "edu")! , UIImage(named: "mar")! , UIImage(named: "ret")!]
    var tool2Name:[String] = [ "Education Calculator" , "Marriage Calculator" , "Retirement Calculator"]
    override func awakeFromNib() {
        super.awakeFromNib()
       collectionView.register(cell: CalculatorsCollectionViewCell.self)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
extension CalculatorsTableViewCell : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 3
        default:
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        switch indexPath.section {
        case 0:
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalculatorsCollectionViewCell", for: indexPath) as! CalculatorsCollectionViewCell
                    cell.calculatorImage.image = toolImage[indexPath.row]
                    cell.calculatorName.text = toolName[indexPath.row]
                    if indexPath.item == 2 {
                        cell.verticalView.isHidden = true
                    }
                return cell
        default:
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalculatorsCollectionViewCell", for: indexPath) as! CalculatorsCollectionViewCell
                    cell.calculatorImage.image = tool2Image[indexPath.row]
                    cell.calculatorName.text = tool2Name[indexPath.row]
                    cell.horizontalView.isHidden = true
                    if indexPath.item == 2 {
                        cell.verticalView.isHidden = true
                    }
                    
                    return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size.width / 3.0
        return CGSize(width:size - 20 , height: size)
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0:
            switch indexPath.item {
            case 0:
                Routes.shared.goToWebView(nav: nav, heading: "Lumpsum Calculator", url: "")
            case 1:
                  Routes.shared.goToWebView(nav: nav, heading: "SIP Calculator", url: "")
            default:
                 Routes.shared.goToWebView(nav: nav, heading: "Cost of Delay SIP", url: "")
            }
            
        default:
            switch indexPath.item {
            case 0:
                Routes.shared.goToWebView(nav: nav, heading: "Education Calculator", url: "")
            case 1:
                 Routes.shared.goToWebView(nav: nav, heading: "Marriage Calculator", url: "")
            default:
                  Routes.shared.goToWebView(nav: nav, heading: "Retirement Calculator", url: "")
            }
        }
    }
}
