//
//  SipTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 28/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import DropDown
import RxSwift
import RxCocoa
class SipTableViewCell: UITableViewCell , UITextFieldDelegate {

    @IBOutlet weak var untilcancelB: UIButton!
    @IBOutlet weak var transactionB: FilledButton!
    @IBOutlet weak var payfirstB: UIButton!
    @IBOutlet weak var selectmandatetf: UITextField!
    @IBOutlet weak var selectmandate: UILabel!
    @IBOutlet weak var untilcancelledB: UIButton!
    @IBOutlet weak var endYeartf: UITextField!
    @IBOutlet weak var endMonthtf: UITextField!
    @IBOutlet weak var enddatetf: UITextField!
    @IBOutlet weak var endyear: UILabel!
    @IBOutlet weak var endmonth: UILabel!
    @IBOutlet weak var endddate: UILabel!
    @IBOutlet weak var sipEndDate: UILabel!
    @IBOutlet weak var endDateViewheight: NSLayoutConstraint!
    @IBOutlet weak var enddateView: UIView!
    @IBOutlet weak var startyeartf: UITextField!
    @IBOutlet weak var startmonthtf: UITextField!
    @IBOutlet weak var startdatetf: UITextField!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var month: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var sipstartdate: UILabel!
    @IBOutlet weak var amounttf: UITextField!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var selectFrequencytf: UITextField!
    @IBOutlet weak var selectFrequency: UILabel!
    @IBOutlet weak var editSchemeImage: UIImageView!
    
    //Dropdowns
    let frequencyDropdown = DropDown()
    let startdateDropdown = DropDown()
    let startmonthDropdown = DropDown()
    let startyearDropdown = DropDown()
    let enddateDropdown = DropDown()
    let endmonthDropdown = DropDown()
    let endyearDropdown = DropDown()
    let mandateDropdown = DropDown()
    
    //arrays
    var sipdates:[String] = []
    var sipInt:[Int] = []
    var sipmonths:[String] = []
    var currentYear:Int = 0
    var frequency:[String] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpTransact()
        selectFrequencytf.delegate = self
        startdatetf.delegate = self
        startyeartf.delegate = self
        startmonthtf.delegate = self
        enddatetf.delegate = self
        endMonthtf.delegate = self
        endYeartf.delegate = self
        selectmandatetf.delegate = self
    }
private let disposableBag = DisposeBag()
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
       textField.resignFirstResponder()
        switch textField {
        case startdatetf:
            addToDropdown(textf: startdatetf, dropDown: startdateDropdown, datasource: sipdates)
        case startmonthtf:
            addToDropdown(textf: startmonthtf, dropDown: startmonthDropdown, datasource: sipmonths)
        case endYeartf:
            let df = DateFormatter()
            let endyear = df.years(currentYear...2099)
             addToDropdown(textf: endYeartf, dropDown: endyearDropdown, datasource: endyear)
        case endMonthtf:
            let df = DateFormatter()
            let months = df.monthSymbols
            addToDropdown(textf: endMonthtf, dropDown: endmonthDropdown, datasource: months!)
        case selectFrequencytf:
            addToDropdown(textf: selectFrequencytf, dropDown: frequencyDropdown, datasource: frequency)
        default:
            print("")
        }
    }
    @IBAction func changeCurrentScheme(_ sender: Any) {
        
    }
    @IBAction func untilcancel(_ sender: Any) {
    
    }
    @IBAction func payFirst(_ sender: Any) {
        
    }
    @IBAction func transactNow(_ sender: Any) {
  
    }
    //rx
    func setUpTransact() {
        self.transactionB.rx.tap.asObservable()
            .filter({
                (_) -> Bool in
                guard !(self.amounttf.text ?? "").isEmpty else {
                    self.amounttf.becomeFirstResponder()
                    return false
                }
                return true
            })
            .subscribe {
                _ in
                print(self.amounttf.text)
            }
            .disposed(by: disposableBag)
        
    }
}
extension SipTableViewCell {
    final func addToDropdown(textf:UITextField , dropDown:DropDown , datasource:[String]) {
        dropDown.anchorView = textf
        dropDown.dataSource = datasource
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            textf.text = item
        }
    }
    final func dates(day:Int) -> Date {
        let currentdate = Date()
        var calender = Calendar.current
        calender.timeZone = NSTimeZone(name: "UTC")! as TimeZone
        let n = 8
        let nextMonth = calender.date(byAdding: .month, value: 1, to: currentdate)
        var components = DateComponents()
        components.year = calender.component(.year, from: currentdate)
        let currentDay = calender.component(.day, from: currentdate)
        currentYear = calender.component(.year, from: currentdate)
        startyeartf.text = String(currentYear)
        components.month = calender.component(.month, from: currentdate)
        
        let  d = currentDay + n
        
        components.day = d
        print(d)
        print(components)
        let checkDate = calender.date(from: components)
        print(checkDate)
        let check = calender.component(.day, from: checkDate!)
        print(check)
        
        if day >= check && day < currentDay {
            print(day)
            print(currentDay)
            components.day = day
            components.month = calender.component(.month, from: nextMonth!)
        }
        else {
            print("here")
            components.day = check
        }
        let chosenDate = calender.date(from: components)
        var month = calender.component(.month, from: chosenDate!)
        var months:[Int] = [month]
        for _ in 0..<2 {
            month += 1
            months.append(month)
        }
        let fmt = DateFormatter()
        fmt.dateFormat = "MM"
        print(months)
        for i in months {
            let monthsym = fmt.monthSymbols[i - 1]
            sipmonths.append(monthsym)
        }
        print(sipmonths)
        endMonthtf.text = sipmonths.last
        startdatetf.text = String(calender.component(.day, from: chosenDate!))
        startmonthtf.text = sipmonths.first
        enddatetf.text = String(calender.component(.day, from: chosenDate!))
        return chosenDate!
    }
}
