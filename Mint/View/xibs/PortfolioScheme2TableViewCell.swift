//
//  PortfolioScheme2TableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 08/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class PortfolioScheme2TableViewCell: UITableViewCell {

    @IBOutlet weak var bottomRightValue: UILabel!
    @IBOutlet weak var bottomRightLabel: UILabel!
    @IBOutlet weak var bottomLeftValue: UILabel!
    @IBOutlet weak var bottomLeftlabel: UILabel!
    @IBOutlet weak var midValue3: UILabel!
    @IBOutlet weak var midValue1: UILabel!
    @IBOutlet weak var midValue2: UILabel!
    @IBOutlet weak var midLabel3: UILabel!
    @IBOutlet weak var midLabel2: UILabel!
    @IBOutlet weak var midLabel1: UILabel!
    @IBOutlet weak var topvalue3: UILabel!
    @IBOutlet weak var topvalue2: UILabel!
    @IBOutlet weak var topValue1: UILabel!
    @IBOutlet weak var topLabel3: UILabel!
    @IBOutlet weak var topLabel2: UILabel!
    @IBOutlet weak var topLabel1: UILabel!
    @IBOutlet weak var headLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
}
