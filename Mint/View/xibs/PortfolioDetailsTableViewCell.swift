//
//  PortfolioDetailsTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
protocol goinside:class {
    func goinside(index:IndexPath)
}
class PortfolioDetailsTableViewCell: UITableViewCell {
    var index:IndexPath?
    var delegate:goinside?
    
    @IBOutlet weak var folioTop: NSLayoutConstraint!
    // @IBOutlet weak var folioTop: NSLayoutConstraint!
    @IBOutlet weak var marketValBottom: NSLayoutConstraint!
   // @IBOutlet weak var folioBottom: NSLayoutConstraint!
  //  @IBOutlet weak var foliotop: NSLayoutConstraint!
    @IBOutlet weak var clickForDetails: UIButton!
    @IBOutlet weak var marketValuetop: NSLayoutConstraint!
    @IBOutlet weak var markettop: NSLayoutConstraint!
    @IBOutlet weak var nameTop: NSLayoutConstraint!
    @IBOutlet weak var bottomvalueimage3: UIImageView!
    @IBOutlet weak var bottomvalueimage2: UIImageView!
    @IBOutlet weak var foliovalue: UILabel!
    @IBOutlet weak var folio: UILabel!
    @IBOutlet weak var marketValue: UILabel!
    @IBOutlet weak var market: UILabel!
    @IBOutlet weak var schemeName: UILabel!
    @IBOutlet weak var bottomValue3: UILabel!
    @IBOutlet weak var bottomValue2: UILabel!
    @IBOutlet weak var bottomValue1: UILabel!
    @IBOutlet weak var bottomhead3: UILabel!
    @IBOutlet weak var bottomhead2: UILabel!
    @IBOutlet weak var bottomHead1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    @IBAction func click(_ sender: Any) {
        delegate?.goinside(index: index!)
    }
    
}
