//
//  TransactionHeaderType1TableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 28/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class TransactionHeaderType1TableViewCell: UITableViewCell {

    @IBOutlet weak var longTermAmount: UILabel!
    @IBOutlet weak var shortTermamount: UILabel!
    @IBOutlet weak var longTermUnits: UILabel!
    @IBOutlet weak var shortTermUnits: UILabel!
    @IBOutlet weak var longTerm: UILabel!
    @IBOutlet weak var shortTerm: UILabel!
    @IBOutlet weak var schemeName: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
