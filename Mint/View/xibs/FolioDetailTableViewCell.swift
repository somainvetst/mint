//
//  FolioDetailTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 23/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class FolioDetailTableViewCell: UITableViewCell {
   
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topLabelValue: UILabel!
    @IBOutlet weak var midLabel1: UILabel!
    @IBOutlet weak var midLabel2: UILabel!
    @IBOutlet weak var midLabel3: UILabel!
    @IBOutlet weak var midValue1: UILabel!
    @IBOutlet weak var midValue2: UILabel!
    @IBOutlet weak var midValue3: UILabel!
    @IBOutlet weak var bottomLabel1: UILabel!
    @IBOutlet weak var topMidLabel: UILabel!
    @IBOutlet weak var topMidValue: UILabel!
    @IBOutlet weak var topMidLabel3: UILabel!
    @IBOutlet weak var topMidLabel3Value: UILabel!
    
    @IBOutlet weak var topMidValue2: UILabel!
    @IBOutlet weak var topMidLabel2: UILabel!
    @IBOutlet weak var bottomLabelValue1: UILabel!
   
    @IBOutlet weak var bottomLeftLabel: UILabel!
    @IBOutlet weak var bottomLeftValue: UILabel!
    
  
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
}
