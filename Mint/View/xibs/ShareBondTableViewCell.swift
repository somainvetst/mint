//
//  ShareBondTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class ShareBondTableViewCell: UITableViewCell {

    @IBOutlet weak var lowerRightValue: UILabel!
    @IBOutlet weak var lowerRight: UILabel!
    @IBOutlet weak var lowerLeftValue: UILabel!
    @IBOutlet weak var lowerLeft: UILabel!
    @IBOutlet weak var bottomRightValue: UILabel!
    @IBOutlet weak var bottomMiddleValue: UILabel!
    @IBOutlet weak var bottomLeftValue: UILabel!
    @IBOutlet weak var bottomright: UILabel!
    @IBOutlet weak var bottommiddle: UILabel!
    @IBOutlet weak var bottomleft: UILabel!
    @IBOutlet weak var topRightValue: UILabel!
    @IBOutlet weak var topMiddlevalue: UILabel!
    @IBOutlet weak var topLeftvalue: UILabel!
    @IBOutlet weak var topRight: UILabel!
    @IBOutlet weak var topmiddle: UILabel!
    @IBOutlet weak var topleft: UILabel!
    @IBOutlet weak var headLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
