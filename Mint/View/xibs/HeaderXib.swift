//
//  HeaderXib.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 08/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
protocol goback:class {
    func goback()
}
class HeaderXib: UIView {
   
    var delegate:goback?
  
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       
    }
    @IBAction func backButton(_ sender: Any) {
        delegate?.goback()
    }
    
}
