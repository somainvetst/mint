//
//  StatementofAccountTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 22/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class StatementofAccountTableViewCell: UITableViewCell {

    @IBOutlet weak var images: UIImageView!
    @IBOutlet weak var navdate: UILabel!
    @IBOutlet weak var currentnavValue: UILabel!
    @IBOutlet weak var folioValue: UILabel!
    @IBOutlet weak var currentNAv: UILabel!
    @IBOutlet weak var folio: UILabel!
    @IBOutlet weak var marketValue: UILabel!
    @IBOutlet weak var balanceValue: UILabel!
    @IBOutlet weak var market: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var schemeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
