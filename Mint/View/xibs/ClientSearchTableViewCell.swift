//
//  ClientSearchTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
protocol clientSearch:class {
    func search(name:String , type:String)
}
class ClientSearchTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var familyButton: UIButton!
    @IBOutlet weak var investorButton: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchtf: UITextField!
    var name:String = ""
    var type = ""
    var delegate:clientSearch?
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.layer.borderWidth = 1.0
        cardView.layer.borderColor = UIColor.gray.cgColor
        familyButton.backgroundColor = .white
         investorButton.titleLabel?.textColor = .white
        familyButton.layer.cornerRadius = 20
        searchtf.layer.cornerRadius = searchtf.frame.size.height / 2
        searchtf.layer.borderWidth = 0.25
        searchtf.layer.borderColor = UIColor.gray.cgColor
        let paddingView : UIView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: searchtf.frame.height))
        searchtf.leftView = paddingView
        searchtf.leftViewMode = UITextField.ViewMode.always
      
       
    }
    override func draw(_ rect: CGRect) {
        if type == "familyHead" {
            familyButton.backgroundColor = .white
            investorButton.backgroundColor = .clear
            familyButton.layer.cornerRadius = 20
            familyButton.setTitleColor(#colorLiteral(red: 0.1882352941, green: 0.2470588235, blue: 0.6235294118, alpha: 1), for: .normal)
            investorButton.titleLabel?.textColor = .white
            
        }
        else {
            investorButton.layer.backgroundColor = UIColor.white.cgColor
            familyButton.backgroundColor = .clear
            investorButton.setTitleColor(#colorLiteral(red: 0.1882352941, green: 0.2470588235, blue: 0.6235294118, alpha: 1), for: .normal)
            investorButton.layer.cornerRadius = 20
            familyButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        }
        searchView.layer.cornerRadius = 30
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    @IBAction func familyBClicked(_ sender: Any) {
        familyButton.backgroundColor = .white
        investorButton.backgroundColor = .clear
        familyButton.layer.cornerRadius = 20
        familyButton.setTitleColor(#colorLiteral(red: 0.1882352941, green: 0.2470588235, blue: 0.6235294118, alpha: 1), for: .normal)
        investorButton.titleLabel?.textColor = .white
        type = "familyHead"
        delegate?.search(name: name, type: type)
    }
    
    @IBAction func investorBclicked(_ sender: Any) {
        investorButton.layer.backgroundColor = UIColor.white.cgColor
        familyButton.backgroundColor = .clear
        investorButton.setTitleColor(#colorLiteral(red: 0.1882352941, green: 0.2470588235, blue: 0.6235294118, alpha: 1), for: .normal)
        investorButton.layer.cornerRadius = 20
        familyButton.titleLabel?.textColor = .white
      //  investorButton.titleLabel?.textColor = .blue
        type = "client"
         delegate?.search(name: name, type: type)
    }
 
    
    @IBAction func search(_ sender: Any) {
        guard let name = searchtf.text else {return}
        delegate?.search(name: name, type: type)
    }
    
}
