//
//  TransactionTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 20/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    @IBOutlet weak var amountValue: UILabel!
    @IBOutlet weak var transactionvalue: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var transactionType: UILabel!
    @IBOutlet weak var dateValue: UILabel!
    @IBOutlet weak var folioValue: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var folio: UILabel!
    @IBOutlet weak var schemeName: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
