//
//  FolioDetail2TableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 23/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class FolioDetail2TableViewCell: UITableViewCell {
    
    @IBOutlet weak var topValueConstant: NSLayoutConstraint!
    @IBOutlet weak var topConstant: NSLayoutConstraint!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var topLabelvalue: UILabel!
    @IBOutlet weak var bottomLabel1: UILabel!
    @IBOutlet weak var bottomLabel2: UILabel!
    @IBOutlet weak var bottomLabel3: UILabel!
    @IBOutlet weak var bottomValue1: UILabel!
    @IBOutlet weak var bottomValue2: UILabel!
    @IBOutlet weak var bottomValue3: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
}
