//
//  PortfolioSummaryTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 19/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class PortfolioSummaryTableViewCell: UITableViewCell {

    @IBOutlet weak var headLabelTop: NSLayoutConstraint!
    @IBOutlet weak var folioViewHeight: NSLayoutConstraint!
    @IBOutlet weak var bottomLabel2value: UILabel!
    @IBOutlet weak var bottomLabelValue: UILabel!
    @IBOutlet weak var bottomLabel2: UILabel!
    @IBOutlet weak var bottomlabel1: UILabel!
    @IBOutlet weak var topLabel2value: UILabel!
    @IBOutlet weak var topLabel1value: UILabel!
    @IBOutlet weak var topLabel2: UILabel!
    @IBOutlet weak var toplabel1: UILabel!
    @IBOutlet weak var headLabel: UILabel!
    @IBOutlet weak var folioLabel2: UILabel!
    @IBOutlet weak var folioLabel1: UILabel!
    @IBOutlet weak var folioView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
