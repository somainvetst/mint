//
//  PortfolioTransactionDetailsTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class PortfolioTransactionDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var nackgroundImage: UIImageView!
    @IBOutlet weak var cagrvalue: UILabel!
    @IBOutlet weak var avgretvalue: UILabel!
    @IBOutlet weak var holdValue: UILabel!
    @IBOutlet weak var cagr: UILabel!
    @IBOutlet weak var avgreturn: UILabel!
    @IBOutlet weak var holding: UILabel!
    @IBOutlet weak var balancevalue: UILabel!
    @IBOutlet weak var gainvalue: UILabel!
    @IBOutlet weak var foliovalue: UILabel!
    @IBOutlet weak var balance: UILabel!
    @IBOutlet weak var gain: UILabel!
    @IBOutlet weak var folio: UILabel!
    @IBOutlet weak var marketValue: UILabel!
    @IBOutlet weak var market: UILabel!
    @IBOutlet weak var purchaseValue: UILabel!
    @IBOutlet weak var purchaseCost: UILabel!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var schemeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      
    }
    
}
