//
//  TransactionTypeTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 20/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class TransactionTypeTableViewCell: UITableViewCell {
    var heigh:NSLayoutConstraint!
    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    var toolImage:[UIImage] = []
    var toolName:[String] = []
    var sipDates:[String] = []
    var stpDates:[String] = []
    var swpDates:[String] = []
    var frequency:[String] = []
    var count:Int = 0
    var nav = UINavigationController()
    var data:PassedSchemeForTransaction?
    override func awakeFromNib() {
        super.awakeFromNib()
       
         collectionView.register(cell: CalculatorsCollectionViewCell.self)
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}

extension TransactionTypeTableViewCell : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if toolName.isEmpty == false {
            
            if toolName.contains("NRP") {
                guard let index = toolName.firstIndex(of: "NRP") else { return UICollectionViewCell() }
                toolName[index] = "Purchase"
                toolImage[index] = UIImage(named: "purchase2")!
            }
            else if toolName.contains("SWO") {
                guard let index = toolName.firstIndex(of: "SWO") else  { return UICollectionViewCell() }
                toolName[index] = "Switch"
                 toolImage[index] = UIImage(named: "switch_float")!
            }
            else if toolName.contains("NRS") {
                guard let index = toolName.firstIndex(of: "NRS") else  { return UICollectionViewCell() }
                toolName[index] = "Redeem"
                toolImage[index] = UIImage(named: "redeem2")!
            }
            else if toolName.contains("SWP") {
                guard let index = toolName.firstIndex(of: "SWP") else  { return UICollectionViewCell() }
                toolName[index] = "SWP"
                toolImage[index] = UIImage(named: "swp_float")!
            }
            else if toolName.contains("STP") {
                guard let index = toolName.firstIndex(of: "STP") else  { return UICollectionViewCell() }
                toolName[index] = "STP"
                toolImage[index] = UIImage(named: "stp2")!
            }
            else if toolName.contains("SIP") {
                guard let index = toolName.firstIndex(of: "SIP") else  { return UICollectionViewCell() }
                toolName[index] = "SIP"
                toolImage[index] = UIImage(named: "sip2")!
            }
        }
       
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CalculatorsCollectionViewCell", for: indexPath) as! CalculatorsCollectionViewCell
         collectionHeight.constant = collectionView.contentSize.height
            cell.calculatorImage.image = toolImage[indexPath.row]
            cell.calculatorName.text = toolName[indexPath.row]
            cell.horizontalView.isHidden = true
            cell.verticalView.isHidden = true
            
            return cell
      
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size.width / 3.0
        return CGSize(width:size - 10 , height: size)
    }
  
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       let cell = collectionView.cellForItem(at: indexPath) as! CalculatorsCollectionViewCell
        switch cell.calculatorName.text {
        case "Purchase":
            Routes.shared.goToPurchaseTransaction(nav: nav)
        case "SIP":
            Routes.shared.goToSipTransaction(nav: nav , dates: sipDates , frequency : frequency, schemeData: [])
        case "Redeem":
            
                Routes.shared.goToRedemptionTransaction(nav: nav)
        case "Switch":
            Routes.shared.goToSwitchTransaction(nav: nav)
        default:
            print("")
        }
      
    }
    
}
