//
//  PortfolioScheme1TableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 08/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class PortfolioScheme1TableViewCell: UITableViewCell {

    @IBOutlet weak var bottomvalue3: UILabel!
    @IBOutlet weak var bottomValue2: UILabel!
    @IBOutlet weak var bottomValue1: UILabel!
    @IBOutlet weak var bottomLabel3: UILabel!
    @IBOutlet weak var bottomLabel2: UILabel!
    @IBOutlet weak var bottomLabel1: UILabel!
    @IBOutlet weak var midValue3: UILabel!
    @IBOutlet weak var midValue2: UILabel!
    @IBOutlet weak var midvalue1: UILabel!
    @IBOutlet weak var midLabel3: UILabel!
    @IBOutlet weak var misLabel2: UILabel!
    @IBOutlet weak var midLabe1: UILabel!
    @IBOutlet weak var headValue2: UILabel!
    @IBOutlet weak var headValue1: UILabel!
    @IBOutlet weak var headLabel2: UILabel!
    @IBOutlet weak var headLabel1: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
