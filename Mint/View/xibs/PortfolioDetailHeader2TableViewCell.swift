//
//  PortfolioDetailHeader2TableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class PortfolioDetailHeader2TableViewCell: UITableViewCell {

    @IBOutlet weak var topMiddleValue: UILabel!
    @IBOutlet weak var topMiddle: UILabel!
    @IBOutlet weak var imageV: UIImageView!
    @IBOutlet weak var bottomvalue3: UILabel!
    @IBOutlet weak var bottomvalue2: UILabel!
    @IBOutlet weak var bottomvalue1: UILabel!
    @IBOutlet weak var bottomthree: UILabel!
    @IBOutlet weak var bottomtwo: UILabel!
    @IBOutlet weak var bottomone: UILabel!
    @IBOutlet weak var topRightValue: UILabel!
    @IBOutlet weak var topRight: UILabel!
    @IBOutlet weak var topLeftValue: UILabel!
    @IBOutlet weak var topLeft: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
