//
//  DashboardTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class DashboardTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var imageHeight: NSLayoutConstraint!
  
   
    @IBOutlet weak var marketChangeImg: UIImageView!
    @IBOutlet weak var MarketChange: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bottomValue3: UILabel!
    @IBOutlet weak var bottomValue2: UILabel!
    @IBOutlet weak var bottomValue1: UILabel!
    @IBOutlet weak var bottomhead3: UILabel!
    @IBOutlet weak var bottomhead2: UILabel!
    @IBOutlet weak var bottomHead1: UILabel!
    @IBOutlet weak var marketValue: UILabel!
    @IBOutlet weak var market: UILabel!
    @IBOutlet weak var nameofClient: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var detailsView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.frame = self.bounds
    }
}
