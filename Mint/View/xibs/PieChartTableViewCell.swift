//
//  PieChartTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Charts

class PieChartTableViewCell: UITableViewCell {

    @IBOutlet weak var pieChartView: PieChartView!
    @IBOutlet weak var nameofChart: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    func setChart(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [PieChartDataEntry] = []
        print(values)
        //if dataPoints.count == values.count
        // {
        for i in 0..<dataPoints.count {
            let dataEntry =  PieChartDataEntry(value: Double(values[i]), label: dataPoints[i])
            dataEntries.append(dataEntry)
            
            print(dataEntries)
        }
        // }
        let pieChartDataSet = PieChartDataSet(entries: dataEntries, label: "")
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 1
        formatter.multiplier = 1.0
        pieChartDataSet.valueFormatter = DefaultValueFormatter(formatter:formatter)
        pieChartDataSet.sliceSpace = 2.0
        pieChartView.usePercentValuesEnabled = true
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.legend.enabled = true
        
        var colors: [UIColor] = []
        
//        for _ in 0..<dataPoints.count {
//            let red = Double(arc4random_uniform(256))
//            let green = Double(arc4random_uniform(256))
//            let blue = Double(arc4random_uniform(256))
//
//            let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
//            colors.append(color)
//        }
        let redcolor = UIColor(red: CGFloat(255.0/255), green: CGFloat(237/255), blue: CGFloat(213/255), alpha: 1)
        let greencolor = UIColor(red: CGFloat(250/255), green: CGFloat(210.0/255), blue: CGFloat(112/255), alpha: 1)
        let orangecolor = UIColor(red: CGFloat(203.0/255), green: CGFloat(116.0/255), blue: CGFloat(89/255),alpha: 1)
         let bluecolor = UIColor(red: CGFloat(161.0/255), green: CGFloat(85.0/255), blue: CGFloat(121/255),alpha: 1)
        let color5 = UIColor(red: CGFloat(121.0/255), green: CGFloat(148.0/255), blue: CGFloat(207/255),alpha: 1)
        let color6 = UIColor(red: CGFloat(220.0/255), green: CGFloat(163.0/255), blue: CGFloat(194/255),alpha: 1)
        colors.append(redcolor)
        colors.append(greencolor)
        colors.append(orangecolor)
        colors.append(bluecolor)
        pieChartDataSet.colors = colors
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        //   pieChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        
        
    }
}
