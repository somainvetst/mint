//
//  RedeemTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 04/11/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class RedeemTableViewCell: UITableViewCell {

    @IBOutlet weak var amountViewHeight: NSLayoutConstraint!
    @IBOutlet weak var amountTf: UITextField!
    @IBOutlet weak var amountView: UIView!
    @IBOutlet weak var types: UISegmentedControl!
    var delegate:orderTap?
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    @IBAction func totalAmount(_ sender: Any) {
    }
    @IBAction func orderButton(_ sender: Any) {
        delegate?.ordertapped()
    }
    
    @IBAction func typeChanged(_ sender: Any) {
        switch types.selectedSegmentIndex {
        case 0:
            amountView.isHidden = false
            amountViewHeight.constant = 50
        case 1:
                amountView.isHidden = false
              amountViewHeight.constant = 50
        default:
                amountView.isHidden = true
              amountViewHeight.constant = 0
        }
    }
}
