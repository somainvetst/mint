//
//  BrokerSearchPanelTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import TextFieldEffects
protocol SearchClient:class {
    func searchTapped(name:String , type:String)
}
class BrokerSearchPanelTableViewCell: UITableViewCell {
    var delegate:SearchClient?
    var type:String = ""
    //@IBOutlet weak var investor: UIButton!
    //@IBOutlet weak var family: UIButton!
    @IBOutlet weak var search: UIButton!
    @IBOutlet weak var headView: UIView!
    @IBOutlet weak var searchtf: HoshiTextField!
    //@IBOutlet weak var investorImage: UIImageView!
    //@IBOutlet weak var familyHeadImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    @IBAction func tapfamily(_ sender: Any) {
//    familyHeadImage.image = UIImage(named: "filled")
//    investorImage.image = UIImage(named: "notfilled")
        type = "familyHead"
    }
    
    @IBAction func tapInvestor(_ sender: Any) {
//    familyHeadImage.image = UIImage(named: "notfilled")
//    investorImage.image = UIImage(named: "filled")
        type = "client"
    }
    
    @IBAction func searchTapped(_ sender: Any) {
        guard let name = searchtf.text else {return}
//        if familyHeadImage.image == UIImage(named: "filled") {
//            type = "familyHead"
//
//        }
//        else {
//            type = "client"
//        }
       
        delegate?.searchTapped(name: name, type: type)
    }
}
