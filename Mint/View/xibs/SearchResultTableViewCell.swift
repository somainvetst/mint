//
//  SearchResultTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    @IBOutlet weak var firstRightLabel: UILabel!
    @IBOutlet weak var firstLabel: UILabel!
    @IBOutlet weak var phoneValue: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var panvalue: UILabel!
    @IBOutlet weak var pan: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var type: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
}
