//
//  TransactionHeaderType2TableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 28/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class TransactionHeaderType2TableViewCell: UITableViewCell {

    @IBOutlet weak var folioValue: UILabel!
    @IBOutlet weak var folio: UILabel!
    @IBOutlet weak var uccvalue: UILabel!
    @IBOutlet weak var ucc: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var schemeName: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
