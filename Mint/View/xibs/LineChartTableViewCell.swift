//
//  LineChartTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 22/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Charts
protocol timetap:class {
    func tap(data:String)
}
class LineChartTableViewCell: UITableViewCell , ChartViewDelegate  {

    @IBOutlet weak var timePeriod: UITextField!
    @IBOutlet weak var netInvestment: UILabel!
    @IBOutlet weak var currentValue: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var detailsView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var lineChartView: LineChartView!
    var picker = UIPickerView()
    var delegate:timetap?
    
    var time = ["This Month" , "Last 1 month" , "Current FY" , "Last FY" , "Last 1 Year" , "Since Inception"]
    override func awakeFromNib() {
        super.awakeFromNib()
        // timePeriod.text = "This Month"
        picker.delegate = self
        detailsView.isHidden = true
        lineChartView.delegate = self
        let marker:BalloonMarker = BalloonMarker(color: UIColor.red, font: UIFont(name: "Helvetica", size: 12)!, textColor: .black, insets: UIEdgeInsets(top: 7.0, left: 7.0, bottom: 7.0, right: 7.0))
        marker.minimumSize = CGSize(width:75.0, height:35.0)
        lineChartView.marker = marker
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
   public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
   

      }
    @IBAction func Tapped(_ sender: Any) {
        timePeriod.inputView = picker
        
       
    }
    func setChart(dataPoints: [String], values1: [Double] , values2:[Double]) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
            let data = LineChartData()
            var lineChartEntry1 = [ChartDataEntry]()
           
            
          
             for i in 0..<values1.count {
                    lineChartEntry1.append(ChartDataEntry(x: Double(i), y: Double(values1[i]) ?? 0.0))
                }
        let set1 = LineChartDataSet(entries: lineChartEntry1, label: "Current Value")
        set1.axisDependency = .left // Line will correlate with left axis values
           set1.setColor(UIColor.red.withAlphaComponent(0.5))
           set1.setCircleColor(UIColor.red)
           set1.lineWidth = 2.0
           set1.circleRadius = 3.0
           set1.fillAlpha = 65 / 255.0
           set1.fillColor = UIColor.red
           set1.highlightColor = UIColor.white
           set1.drawCircleHoleEnabled = true
           set1.drawValuesEnabled = false

          data.addDataSet(set1)
        
         if (values2.count > 0) {
             var lineChartEntry2 = [ChartDataEntry]()
             for i in 0..<values2.count {
                 lineChartEntry2.append(ChartDataEntry(x: Double(i), y: Double(values2[i]) ?? 0.0))
             }
            let set2 = LineChartDataSet(entries: lineChartEntry2, label: "Net Investment")
            set2.axisDependency = .left // Line will correlate with left axis values
               set2.setColor(UIColor.green.withAlphaComponent(0.5))
               set2.setCircleColor(UIColor.green)
               set2.lineWidth = 2.0
               set2.circleRadius = 3.0
               set2.fillAlpha = 65 / 255.0
               set2.fillColor = UIColor.green
               set2.highlightColor = UIColor.white
               set2.drawCircleHoleEnabled = true
              set2.drawValuesEnabled = false
         data.addDataSet(set2)
            
            self.lineChartView.drawBordersEnabled = false
            self.lineChartView.data = data
            lineChartView.leftAxis.enabled = false
            lineChartView.rightAxis.enabled = false
            
            //   pieChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
            
            
        }
    }
}
extension LineChartTableViewCell: UIPickerViewDelegate , UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return time.count
    }
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
      return time[row]
    }
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        timePeriod.text = time[row]
        delegate?.tap(data: timePeriod.text!)
    }
    
}
