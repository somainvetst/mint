//
//  FolioLookupTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 22/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
protocol tappedfolio:class {
    func tapped(index:IndexPath)
}
class FolioLookupTableViewCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var sidemenuButton: UIButton!
    @IBOutlet weak var panvalue: UILabel!
    @IBOutlet weak var holdingvalue: UILabel!
    @IBOutlet weak var investorvalue: UILabel!
    @IBOutlet weak var pan: UILabel!
    @IBOutlet weak var holding: UILabel!
    @IBOutlet weak var investor: UILabel!
    @IBOutlet weak var uccValue: UILabel!
    @IBOutlet weak var foliovalue: UILabel!
    @IBOutlet weak var amountValue: UILabel!
    @IBOutlet weak var ucc: UILabel!
    @IBOutlet weak var folio: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var schemeName: UILabel!
    var delegate:tappedfolio?
    var index:IndexPath?
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    @IBAction func sideButtonTapped(_ sender: Any) {
        delegate?.tapped(index: index!)
    }
    
}
