//
//  TransactionTypeFilterTableViewCell.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 14/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class TransactionTypeFilterTableViewCell: UITableViewCell {

    @IBOutlet weak var typeName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
      
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
      
    }

}
