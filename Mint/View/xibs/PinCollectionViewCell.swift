//
//  PinCollectionViewCell.swift
//  Thukral Capital Market
//
//  Created by Excel Net Solutions Pvt Ltd on 01/05/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class PinCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var circularView: UIView!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var eraseImage: UIImageView!
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        circularView.layer.cornerRadius = circularView.frame.height / 2
    }
}
