//
//  HeadView.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 08/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
protocol back:class {
    func goBack()
}
@IBDesignable
class HeaderView: UIView {
   
    @IBInspectable var heads:String = ""
    @IBInspectable var vc:String = ""
    @IBInspectable var views = ViewController()
    var delegate:back?
    
    var subViewColor:UIColor = Shared.sharedInstance.color
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // custom setup
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
       
    }
   @objc func back(_sender:UIButton) {

    
    self.delegate?.goBack()
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        let view = HeaderView.instanceFromNib()
        
        for subview in view.subviews as [UIView]
        {
            if subview.tag == 0
            {
                if let label = view.viewWithTag(20) as? UILabel {
                    label.text = heads
                }
                if let button = view.viewWithTag(10) as? UIButton {
                    button.addTarget(self, action:  #selector(self.back(_sender:)), for: .touchUpInside)
                }
            }
        }
        addSubview(view)
    }

    class func instanceFromNib() -> UIView {
       
        return UINib(nibName: "HeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}

