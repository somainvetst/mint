//
//  FilledButton.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit

class FilledButton: UIButton {

    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = 20
        self.layer.backgroundColor = Shared.sharedInstance.color.cgColor
        self.titleLabel?.textColor = UIColor.white
        
    }
}
