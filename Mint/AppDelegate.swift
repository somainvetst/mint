//
//  AppDelegate.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 08/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
import Siren
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate , UITabBarControllerDelegate  {

    var window: UIWindow?
    var times:String = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Siren.shared.wail()
       Fabric.with([Crashlytics.self])
        IQKeyboardManager.shared.enable = true
        CoreData.core.fetchCartData(entity: "Logindata")
        if CoreData.core.data.isEmpty == true {
     let dashvc: DashboardViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController;
            
            
            let nav = UINavigationController(rootViewController: dashvc)
            nav.navigationBar.isHidden = true
            nav.interactivePopGestureRecognizer?.isEnabled = false
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = nav
        }
        else {
             let vc: PinViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PinViewController") as! PinViewController;
            let nav = UINavigationController(rootViewController: vc)
            nav.navigationBar.isHidden = true
             nav.interactivePopGestureRecognizer?.isEnabled = false
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = nav
        }
        return true
    }
   
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        if viewController is MenuViewController {
          
             let newVC = UINavigationController.init(rootViewController: Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "MenuViewController"))
                newVC.view.backgroundColor = .clear
                newVC.navigationBar.isHidden = true
                tabBarController.definesPresentationContext = true
                newVC.modalPresentationStyle = .overCurrentContext
               // tabBarController.navigationController?.navigationBar.isHidden = true
                tabBarController.present(newVC, animated: true)
            
                return false
            
        }
    
        return true
    }
    
   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when te application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
       
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
//        let currentDate = Date()
//        UserDefaults.standard.set(currentDate, forKey: "DateToLogout")
        self.saveContext()
    }
    //Shortcuts
       var launchedShortcutItem: UIApplicationShortcutItem?
    enum ShortcutIdentifier: String {
        
        case Dynamic
        
        // MARK: Initializers
        init?(fullNameForType: String) {
            guard let last = fullNameForType.components(separatedBy: ".").last else { return nil }
            
            self.init(rawValue: last)
        }
        
        // MARK: Properties
        var type: String {
            return Bundle.main.bundleIdentifier! + ".\(self.rawValue)"
        }
    }
    func handleShortcutItem(item: UIApplicationShortcutItem) -> Bool {
           
           var handled = false
           // Verify that the provided shortcutItem's type is one handled by the application.
           guard ShortcutIdentifier(fullNameForType: item.type) != nil else { return false }
           guard let shortCutType = item.type as String? else { return false }
           
           let mainStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
           var reqVC: UIViewController!
       var type = item.localizedTitle
           switch shortCutType {
               
           case ShortcutIdentifier.Dynamic.type:
            switch type {
            case "Portfolio Details":
                PinViewController.dynamicMenu = 10
            case "Transactions":
                PinViewController.dynamicMenu = 01
            case "Client Search":
                PinViewController.dynamicMenu = 03
            default:
                PinViewController.dynamicMenu = 02
            }
               self.handleDynamicAction()
               return true
               
           default:
               print("Shortcut Item Handle func")
           }
           
           if let homeVC = self.window?.rootViewController as? UINavigationController {
               homeVC.pushViewController(reqVC, animated: true)
           } else {
               return false
           }
           
           return handled
           
       }
       
       func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
           
           completionHandler(handleShortcutItem(item: shortcutItem))
           
       }

       
       func handleDynamicAction() {
           
           let mainStoryboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)

           if let homeVC = self.window?.rootViewController as? UINavigationController {
               
               if let orngVC = mainStoryboard.instantiateViewController(withIdentifier: "PinViewController") as? PinViewController {
                   
                   orngVC.isItPresentingViaShortcutAction = true
                   homeVC.pushViewController(orngVC, animated: true)

               }
               
           }
       }
//Timers
  
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Mint")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

