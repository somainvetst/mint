//
//  PasseedSchemeForTransaction.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 30/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import Foundation
struct PassedSchemeForTransaction {
    var name:String
    var clientName:String
    var ucc:String
    var folio:String
}
