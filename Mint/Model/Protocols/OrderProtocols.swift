//
//  OrderProtocols.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 16/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON
public protocol orderTap {
    func ordertapped()
}
public protocol confirmOtp {
    func otp(data:JSON)
}
