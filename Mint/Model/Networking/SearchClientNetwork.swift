//
//  SearchClientNetwork.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 26/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
final class SearchClientNetwork {
    var data:JSON?
    var delegate:Completion?
    final func network(table:UITableView  , name:String ){
        
        guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
        guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
        let headers = ["cookie" :
            "connect.sid=\(connect)" + ";" +
            "c_ux=\(cux)"
        ]
        let param1 = [["name" : name]]
        let param2 = ["componentName":"ClientFinder"]
       
        
        do {
            let data = try JSONSerialization.data(withJSONObject: param1, options: .prettyPrinted)
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
            let urlParams2 = param2.compactMap({ (key, value) -> String in
                return "\(key)=\(value)"
            }).joined(separator: "&")
           
            let url:String = "\(Shared.sharedInstance.baseUrl)broker/investorManagement/clientFinder?pageSize=20&currentPage=1&orderBy=name&orderByDesc=false&filters=\(urlEncodedJson!)&componentForLoader=\(urlParams2)&selectedUser="
            Alamofire.request(url, method: .get, headers: headers)
                .responseJSON { response in
                    // print(response)
                    switch response.result {
                    case .success(let data):
                        let json = JSON(data)
                        if Shared.sharedInstance.jsonData != json {
                            Shared.sharedInstance.jsonData = json
                        }
                        self.data = json
                    //    print(self.data)
                         Shared.sharedInstance.time = 0
                        table.reloadData()
                        self.delegate?.complete(data: json)
                        break
                    case .failure(let error):
                        
                        print(error)
                    }
            }
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
    }
    
}
