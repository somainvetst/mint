//
//  NetworkingHelper.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import Foundation
enum HTTPMethods:String {
    case post = "POST"
    case get = "GET"
}
class NetworkingHelper {
    static let shared = NetworkingHelper()
    private init() {}
    func getRequest<T:Decodable>(url:String , method:String , body:Data? = nil , completion: @escaping(T) -> Void , onError: @escaping(String) -> Void) {
        guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
        guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
        _ = ["cookie" :
            "connect.sid=\(connect)" + ";" +
            "c_ux=\(cux)"
        ]
        let convertedUrl = URL(string: url)!
        var urlRequest = URLRequest(url: convertedUrl, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 60)
        urlRequest.httpMethod = "GET"
        urlRequest.httpBody = body
        urlRequest.setValue("connect.sid=\(connect)" + ";" +
            "c_ux=\(cux)" , forHTTPHeaderField: "cookie")
        
       print(urlRequest.debugDescription)
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
           
            if error != nil {
                
                DispatchQueue.main.async {
                    onError("Something went wrong in request" )
                     print(url)
                }
            }
            else {
                do {
                   
                    
                    self.printJson(data: data)
                let json = try JSONDecoder().decode(T.self, from: data ?? Data())
                DispatchQueue.main.async {
                   
                    completion(json)
                    print(json)
                     print(url)
                }
                }
                catch let error {
                    DispatchQueue.main.async {
                        if let decodingError = error as? DecodingError {
                            print(decodingError.localizedDescription)
                        }
                    
                    onError("something went wrong in parsing")
                    }
                }
            }
        }.resume()
        
        
    }
    func printJson(data: Data?){
        print(try? JSONSerialization.jsonObject(with: data ?? Data(), options: .allowFragments))
    }
    
}

