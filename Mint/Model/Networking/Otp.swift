//
//  Otp.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 18/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class Otp  {
    var deleg:confirmOtp?
    var delegate:Completion?
    func getOtp(token:String) {
            let url = URL(string: "\(Shared.sharedInstance.baseUrl)auth/sendOTP")
            let parameters:Parameters = ["token" : token]
            Alamofire.request(url!, method: .post, parameters: parameters ,encoding: JSONEncoding.default, headers: nil).responseJSON {
                       response in
                       switch response.result {
                       case .success(let data):
                           let json = JSON(data)
                           print(json)
                           self.delegate?.complete(data: json)
                         //  self.getnetwork.network()
                           break
                       case .failure(let error):
                           
                           print(error)
                       
                   }
        }
    }
    func authenticateOtp(token:String , otp:String) {
               let url = URL(string: "\(Shared.sharedInstance.baseUrl)auth/authenticateOTP")
        let parameters:Parameters = ["token" : token , "otp" : otp]
               Alamofire.request(url!, method: .post, parameters: parameters ,encoding: JSONEncoding.default, headers: nil).responseJSON {
                          response in
                          switch response.result {
                          case .success(let data):
                              let json = JSON(data)
                              print(json)
                              self.deleg?.otp(data: json)
                            //  self.getnetwork.network()
                              break
                          case .failure(let error):
                              
                              print(error)
                          
                      }
           }
       }
}
