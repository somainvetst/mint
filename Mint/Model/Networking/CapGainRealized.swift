//
//  CapGainRealized.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 27/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
final class CapGainRealized {
    var data:JSON?
    var url:String = ""
   final func network(year:String , id:String) {
        guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
        guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
        let headers = ["cookie" :
            "connect.sid=\(connect)" + ";" +
            "c_ux=\(cux)"
        ]
        let params = ["componentName":"CapGainRealized"]
        let param2 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
        let urlParams = params.compactMap({ (key, value) -> String in
            return "\(key)=\(value)"
        }).joined(separator: "&")
        do {
            let data = try JSONSerialization.data(withJSONObject: param2, options: .prettyPrinted)
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
            if Shared.sharedInstance.clientType == "" {
                url = "\(Shared.sharedInstance.baseUrl)client/reports/capitalGain/getRealized?applicantUid=\(id)&year=\(year)&componentForLoader=\(urlParams)"
            }
            else {
                url = "\(Shared.sharedInstance.baseUrl)client/reports/capitalGain/getRealized?applicantUid=\(Shared.sharedInstance.typeId)&year=\(year)&componentForLoader=\(urlParams)&investorUid=\(Shared.sharedInstance.typeId)&selectedUser=\(urlEncodedJson!)"
            }
            Alamofire.request(url, method: .get, headers: headers)
                .responseJSON { response in
                    // print(response)
                    switch response.result {
                    case .success(let data):
                        let json = JSON(data)
                        // print(url)
                         Shared.sharedInstance.time = 0
                        if Shared.sharedInstance.jsonData != json {
                            Shared.sharedInstance.jsonData = json
                        }
                        self.data = json
                        
                    //    print(self.data)
                        break
                    case .failure(let error):
                        
                        print(error)
                    }
            }
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
        
    }
}
