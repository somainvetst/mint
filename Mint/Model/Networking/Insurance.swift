
//
//  Insurance.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
class Insurance {
    var data:JSON?
      var url:String = ""
      var delegate:Completion?
      func network(table:UITableView , id:String){
            let date = Date()
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd"
        _ = formatter.string(from: date)
            guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
            guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
            let headers = ["cookie" :
                "connect.sid=\(connect)" + ";" +
                "c_ux=\(cux)"
            ]
            let param1 = [["applicantUIds" : [Shared.sharedInstance.typeId]]]
            let param2 = ["componentName":"lifeInsuranceTable"]
        
            let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
           
            let urlParams2 = param2.compactMap({ (key, value) -> String in
                return "\(key)=\(value)"
            }).joined(separator: "&")
            do {
                let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
                let data1 = try JSONSerialization.data(withJSONObject: param1, options: .prettyPrinted)
                let jsonString1 = NSString(data: data1, encoding: String.Encoding.utf8.rawValue)
                let urlEncodedJson1 = jsonString1!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                if Shared.sharedInstance.clientType == "" {
                    url = "\(Shared.sharedInstance.baseUrl)broker/insurance/getInsuranceList?currentPage=1&pageSize=20&orderBy=issuingDate&orderByDesc=true&category=\(id)&componentForLoader=\(urlParams2)&filters=&selectedUser="
                }
                else {
                    url = "\(Shared.sharedInstance.baseUrl)client/insurance/getInsuranceList?currentPage=1&pageSize=20&orderBy=issuingDate&orderByDesc=true&category=\(id)&componentForLoader=\(urlParams2)&filters=\(urlEncodedJson1!)&investorUid=\(Shared.sharedInstance.typeId)&levelNo&selectedUser=\(urlEncodedJson!)"
                }
                Alamofire.request(url, method: .get, headers: headers)
                    .responseJSON { response in
                        // print(response)
                        switch response.result {
                        case .success(let data):
                            let json = JSON(data)
                            // print(url)
                           
                            self.data = json
                              print(self.data)
                           
                           if Shared.sharedInstance.jsonData != json {
                                                     Shared.sharedInstance.jsonData = json
                                                 }
                            self.delegate?.complete(data: self.data)
                            table.reloadData()
                            break
                        case .failure(let error):
                            
                            print(error)
                        }
                }
            }
            catch let JSONError as NSError {
                print("\(JSONError)")
            }
            
        }

}
