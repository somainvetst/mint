//
//  ForgotPassword.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 18/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class ForgotPassword {
    var delegate:Completion?
    func forgot(name:String) {
        let url = URL(string: "\(Shared.sharedInstance.baseUrl)auth/sendPasswordResetMail")
        let parameters:Parameters = ["username" : name]
        Alamofire.request(url!, method: .post, parameters: parameters ,encoding: JSONEncoding.default, headers: nil).responseJSON {
                   response in
                   switch response.result {
                   case .success(let data):
                       let json = JSON(data)
                       print(json)
                       self.delegate?.complete(data: json)
                     //  self.getnetwork.network()
                       break
                   case .failure(let error):
                       
                       print(error)
                   
               }
    }
}
}
