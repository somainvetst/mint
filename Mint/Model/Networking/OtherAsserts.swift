//
//  OtherAsserts.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class OtherAsserts {
    var data:JSON?
    var url:String = ""
    var delegate:Completion?
    func network(table:UITableView , id:String){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let endDate = formatter.string(from: date)
        guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
        guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
        let headers = ["cookie" :
            "connect.sid=\(connect)" + ";" +
            "c_ux=\(cux)"
        ]
        let param1 = ["endDate": endDate]
        let param2 = ["componentName":"folioOtherAssetTable"]
        let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
        _ = param1.compactMap({ (key, value) -> String in
            return "\(key)=\(value)"
        }).joined(separator: "&")
        let urlParams2 = param2.compactMap({ (key, value) -> String in
            return "\(key)=\(value)"
        }).joined(separator: "&")
        do {
            let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
            if Shared.sharedInstance.clientType == "" {
                url = "\(Shared.sharedInstance.baseUrl)client/reports/getPortfolioSummaryForOtherAssets?filters=&componentForLoader=\(urlParams2)&pageSize=20&currentPage=1"
            }
            else {
                url = "\(Shared.sharedInstance.baseUrl)client/reports/getPortfolioSummaryForOtherAssets?filters=&componentForLoader=\(urlParams2)&pageSize=20&currentPage=1&investorUid=\(Shared.sharedInstance.typeId)&selectedUser=\(urlEncodedJson!)"
            }
            Alamofire.request(url, method: .get, headers: headers)
                .responseJSON { response in
                    // print(response)
                    switch response.result {
                    case .success(let data):
                        let json = JSON(data)
                        // print(url)
                        Shared.sharedInstance.jsonData = json
                        self.data = json
                        print(self.data)
                         Shared.sharedInstance.time = 0
                        Shared.sharedInstance.familymemberdata = self.data
                        self.delegate?.complete(data: self.data)
                        table.reloadData()
                        
                        break
                    case .failure(let error):
                        
                        print(error)
                    }
            }
        }
        catch let JSONError as NSError {
            print("\(JSONError)")
        }
        
    }
    
}
