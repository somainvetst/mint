//
//  NetworkError.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 23/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import Foundation
import SwiftyJSON

class NetworkError {
   
    static let errorcheck = NetworkError()
    func errors(data:JSON?) -> Bool {
        if data?["status"] == -1 &&  data?["message"].stringValue == "User not authenticated" {
          
            return false
        }
        else {
       
            return true
        }
    }
}
