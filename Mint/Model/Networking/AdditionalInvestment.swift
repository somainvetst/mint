//
//  AdditionalInvestment.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 01/11/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class AdditionalInvestment {
    var data:JSON?
    var url:String = ""
    var delegate:Completion?
    func getbalanceUnits(table:UITableView , id:String) {
        let date = Date()
              let formatter = DateFormatter()
              formatter.dateFormat = "yyyy-MM-dd"
           //   let endDate = formatter.string(from: date)
              guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
              guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
              let headers = ["cookie" :
                  "connect.sid=\(connect)" + ";" +
                  "c_ux=\(cux)"
              ]
             
              let param2 = ["componentName":"componentName"]
              let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
//               let urlParams1 = param1.compactMap({ (key, value) -> String in
//                  return "\(key)=\(value)"
//              }).joined(separator: "&")
              let urlParams2 = param2.compactMap({ (key, value) -> String in
                  return "\(key)=\(value)"
              }).joined(separator: "&")
              do {
                  let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                  let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                  let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
                  if Shared.sharedInstance.clientType == "" {
                    url = "\(Shared.sharedInstance.baseUrl)client/investOnline/getBalanceUnits?folioid=\(id)&investorUid=&selectedUser="
                  }
                  else {
                      url = "\(Shared.sharedInstance.baseUrl)client/investOnline/getBalanceUnits?folioid=\(id)&investorUid=\(Shared.sharedInstance.typeId)&selectedUser=\(urlEncodedJson!)"
                  }
                  Alamofire.request(url, method: .get, headers: headers)
                      .responseJSON { response in
                          // print(response)
                          switch response.result {
                          case .success(let data):
                              let json = JSON(data)
                              // print(url)
                              if Shared.sharedInstance.jsonData != json {
                                  Shared.sharedInstance.jsonData = json
                              }
                              self.data = json
                                print(self.data)
                             
                              self.delegate?.complete(data: self.data)
                              table.reloadData()
                              break
                          case .failure(let error):
                              
                              print(error)
                          }
                  }
              }
              catch let JSONError as NSError {
                  print("\(JSONError)")
              }
              
          
    }
    func getswitchSchemes(table:UITableView , id:String) {
         //      let date = Date()
               let formatter = DateFormatter()
               formatter.dateFormat = "yyyy-MM-dd"
            //   let endDate = formatter.string(from: date)
               guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
               guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
               let headers = ["cookie" :
                   "connect.sid=\(connect)" + ";" +
                   "c_ux=\(cux)"
               ]
               let param1 = ["schid": id]
               let param2 = ["componentName":"investOnline"]
               let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
                let urlParams1 = param1.compactMap({ (key, value) -> String in
                   return "\(key)=\(value)"
               }).joined(separator: "&")
               let urlParams2 = param2.compactMap({ (key, value) -> String in
                   return "\(key)=\(value)"
               }).joined(separator: "&")
               do {
                   let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                   let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                   let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
                   if Shared.sharedInstance.clientType == "" {
                     url = "\(Shared.sharedInstance.baseUrl)client/investOnline/getTargetSchemeGroups?\(urlParams1)&orderBy=name&orderByDesc=false&hasBseCode=true&componentForLoader=\(urlParams2)&investorUid=&selectedUser="
                   }
                   else {
                       url = "\(Shared.sharedInstance.baseUrl)client/investOnline/getTargetSchemeGroups?\(urlParams1)&orderBy=name&orderByDesc=false&hasBseCode=true&componentForLoader=\(urlParams2)&investorUid=\(Shared.sharedInstance.typeId)&selectedUser=\(urlEncodedJson!)"
                   }
                   Alamofire.request(url, method: .get, headers: headers)
                       .responseJSON { response in
                           // print(response)
                           switch response.result {
                           case .success(let data):
                               let json = JSON(data)
                               // print(url)
                               if Shared.sharedInstance.jsonData != json {
                                   Shared.sharedInstance.jsonData = json
                               }
                               self.data = json
                                 print(self.data)
                              
                               table.reloadData()
                               break
                           case .failure(let error):
                               
                               print(error)
                           }
                   }
               }
               catch let JSONError as NSError {
                   print("\(JSONError)")
               }
               
           
     }
    func getValidInvestment(id:String , type:String) {
         //      let date = Date()
               let formatter = DateFormatter()
               formatter.dateFormat = "yyyy-MM-dd"
            //   let endDate = formatter.string(from: date)
               guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
               guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
               let headers = ["cookie" :
                   "connect.sid=\(connect)" + ";" +
                   "c_ux=\(cux)"
               ]
               let param1 = ["schid": id]
               let param2 = ["componentName":"investOnline"]
               let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
                let urlParams1 = param1.compactMap({ (key, value) -> String in
                   return "\(key)=\(value)"
               }).joined(separator: "&")
               let urlParams2 = param2.compactMap({ (key, value) -> String in
                   return "\(key)=\(value)"
               }).joined(separator: "&")
               do {
                   let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                   let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                   let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
                   if Shared.sharedInstance.clientType == "" {
                     url = "\(Shared.sharedInstance.baseUrl)client/investOnline/getValidInvestmentTypes?schemeGroupId=\(id)&subType=\(type)&investorUid=&selectedUser="
                   }
                   else {
                       url = "\(Shared.sharedInstance.baseUrl)client/investOnline/getValidInvestmentTypes?schemeGroupId=\(id)&subType=\(type)&investorUid=\(Shared.sharedInstance.typeId)&selectedUser=\(urlEncodedJson!)"
                   }
                   Alamofire.request(url, method: .get, headers: headers)
                       .responseJSON { response in
                           // print(response)
                           switch response.result {
                           case .success(let data):
                               let json = JSON(data)
                               // print(url)
                               if Shared.sharedInstance.jsonData != json {
                                   Shared.sharedInstance.jsonData = json
                               }
                               self.data = json
                                 print(self.data)
                              
                               self.delegate?.complete(data: self.data)
                            
                               break
                           case .failure(let error):
                               
                               print(error)
                           }
                   }
               }
               catch let JSONError as NSError {
                   print("\(JSONError)")
               }
               
           
     }
}
