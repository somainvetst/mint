//
//  GetUccInnList.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 04/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class GetUccInnList {
    var data:JSON?
   
    func network(table:UITableView , domain:String){
        guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
        guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
        let headers = ["cookie" :
            "connect.sid=\(connect)" + ";" +
            "c_ux=\(cux)"
        ]
        
        let url:String = "\(Shared.sharedInstance.baseUrl)op/getBrokerPublicInfo?brokerDomain=\(domain)&selectedUser="
        Alamofire.request(url, method: .get, headers: headers)
            .responseJSON { response in
                // print(response)
                switch response.result {
                case .success(let data):
                    let json = JSON(data)
                     Shared.sharedInstance.time = 0
                    if Shared.sharedInstance.jsonData != json {
                        Shared.sharedInstance.jsonData = json
                    }
                    self.data = json
                    print(self.data)
                    table.reloadData()
                    
                    break
                case .failure(let error):
                    
                    print(error)
                }
        }
    }
}
