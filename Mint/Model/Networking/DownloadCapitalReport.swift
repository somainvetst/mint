//
//  DownloadCapitalReport.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 27/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
protocol Savepdf:class {
    func showActivity(vc:UIViewController)
}
class DownloadCapitalReport {
      var data:JSON?
    var folderURL:URL?
    var fileURL:URL?
    var delegate:Savepdf?
    func network(year:String , uid:String , pdf:String  , completionHandler:@escaping(String, Bool)->()) {
        let uniqueName = uid + year + pdf
        let downloadUrl: String = "\(Shared.sharedInstance.baseUrl)client/reports/capitalGain/exportPDF?year=\(year)&applicantUid=\(uid)&pdfType=\(pdf)"
        let destinationPath: DownloadRequest.DownloadFileDestination = { _, _ in
//            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0];
//            let fileURL = documentsURL.appendingPathComponent("\(uniqueName).pdf")
            self.folderURL = self.createFolder(folderName: "CapitalGain Reports")
             self.fileURL = self.folderURL!.appendingPathComponent("\(uniqueName).pdf")
            return (self.fileURL!, [.removePreviousFile, .createIntermediateDirectories])
        }
        print(downloadUrl)
        Alamofire.download(downloadUrl, to: destinationPath)
            .downloadProgress { progress in
                
            }
            .responseData { response in
                print("response: \(response)")
                switch response.result{
                case .success:
                    
                    if response.destinationURL != nil, let filePath = response.destinationURL?.absoluteString {
                        completionHandler(filePath, true)
                        print(filePath)
                        do {
                            //Show UIActivityViewController to save the downloaded file
                            let contents  = try FileManager.default.contentsOfDirectory(at: self.folderURL!, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                            for indexx in 0..<contents.count {
                                if contents[indexx].lastPathComponent == self.fileURL!.lastPathComponent {
                                    let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                    self.delegate?.showActivity(vc: activityViewController)
                                    //self.present(activityViewController, animated: true, completion: nil)
                                }
                            }
                        }
                        catch (let err) {
                            print("error: \(err)")
                        }
                    }
                    break
                case .failure:
                    completionHandler("", false)
                    break
                }
        }
    }
                
        
         func networkUnrealized(uid:String   , completionHandler:@escaping(String, Bool)->()) {
                let uniqueName = uid  + "Unrealized"
                let downloadUrl: String = "\(Shared.sharedInstance.baseUrl)client/reports/capitalGain/exportUnrealizedPDF?applicantUid=\(uid)"
                let destinationPath: DownloadRequest.DownloadFileDestination = { _, _ in
        //            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0];
        //            let fileURL = documentsURL.appendingPathComponent("\(uniqueName).pdf")
                    self.folderURL = self.createFolder(folderName: "CapitalGainUnrealized Reports")
                     self.fileURL = self.folderURL!.appendingPathComponent("\(uniqueName).pdf")
                    return (self.fileURL!, [.removePreviousFile, .createIntermediateDirectories])
                }
                print(downloadUrl)
                Alamofire.download(downloadUrl, to: destinationPath)
                    .downloadProgress { progress in
                        
                    }
                    .responseData { response in
                        print("response: \(response)")
                        switch response.result{
                        case .success:
                            
                            if response.destinationURL != nil, let filePath = response.destinationURL?.absoluteString {
                                completionHandler(filePath, true)
                                print(filePath)
                                do {
                                    //Show UIActivityViewController to save the downloaded file
                                    let contents  = try FileManager.default.contentsOfDirectory(at: self.folderURL!, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                                    for indexx in 0..<contents.count {
                                        if contents[indexx].lastPathComponent == self.fileURL!.lastPathComponent {
                                            let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                            self.delegate?.showActivity(vc: activityViewController)
                                            //self.present(activityViewController, animated: true, completion: nil)
                                        }
                                    }
                                }
                                catch (let err) {
                                    print("error: \(err)")
                                }
                            }
                            break
                        case .failure:
                            completionHandler("", false)
                            break
                        }
                        
                }
    }
    
    final func createFolder(folderName:String)->URL
    {
        var paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = paths[0] as? String ?? ""
        let dataPath: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(folderName).absoluteString
        if !FileManager.default.fileExists(atPath: dataPath) {
            try? FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
        }
        let fileURL = URL(string: dataPath)
        return fileURL!
    }
}
