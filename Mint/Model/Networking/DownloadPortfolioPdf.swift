//
//  DownloadPortfolioPdf.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 04/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
protocol Savepdfs:class {
    func showActivity(vc:UIViewController)
}
class DownloadPortfolioPdf {
    var data:JSON?
    var folderURL:URL?
    var fileURL:URL?
    var delegate:Savepdfs?
    func network(levelno:String , uid:String  , completionHandler:@escaping(String, Bool)->()) {
        let uniqueName = uid + "PortfolioDetails"
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let endDate = formatter.string(from: date)
         let param1 = [["endDate" : endDate]]
         let param3 = ["uid" : uid, "levelNo" : levelno]
         do {
            let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
            let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
            let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
            let data1 = try JSONSerialization.data(withJSONObject: param1, options: .prettyPrinted)
            let jsonString1 = NSString(data: data1, encoding: String.Encoding.utf8.rawValue)
            let urlEncodedJson1 = jsonString1!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        let downloadUrl: String = "\(Shared.sharedInstance.baseUrl)client/reports/downloadPortfolioReport?filters=\(urlEncodedJson1!)&investorUid=\(uid)&group=appid&selectedUser=\(urlEncodedJson!)"
        let destinationPath: DownloadRequest.DownloadFileDestination = { _, _ in
            //            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0];
            //            let fileURL = documentsURL.appendingPathComponent("\(uniqueName).pdf")
            self.folderURL = self.createFolder(folderName: "PortFolioDetailsReports")
            self.fileURL = self.folderURL!.appendingPathComponent("\(uniqueName).pdf")
            return (self.fileURL!, [.removePreviousFile, .createIntermediateDirectories])
        }
        print(downloadUrl)
        Alamofire.download(downloadUrl, to: destinationPath)
            .downloadProgress { progress in
                
            }
            .responseData { response in
                print("response: \(response)")
                switch response.result{
                case .success:
                    
                    if response.destinationURL != nil, let filePath = response.destinationURL?.absoluteString {
                        completionHandler(filePath, true)
                        print(filePath)
                        do {
                            //Show UIActivityViewController to save the downloaded file
                            let contents  = try FileManager.default.contentsOfDirectory(at: self.folderURL!, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                            for indexx in 0..<contents.count {
                                if contents[indexx].lastPathComponent == self.fileURL!.lastPathComponent {
                                    let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                    self.delegate?.showActivity(vc: activityViewController)
                                    //self.present(activityViewController, animated: true, completion: nil)
                                }
                            }
                        }
                        catch (let err) {
                            print("error: \(err)")
                        }
                    }
                    break
                case .failure:
                    completionHandler("", false)
                    break
                }
                
        }
        }
         catch let JSONError as NSError {
            print("\(JSONError)")
        }
    }
    func networkSummary(levelno:String , uid:String  , completionHandler:@escaping(String, Bool)->()) {
           let uniqueName = uid + "PortfolioSummary"
           let date = Date()
           let formatter = DateFormatter()
           formatter.dateFormat = "yyyy-MM-dd"
           let endDate = formatter.string(from: date)
        let param1 = [["endDate" : endDate]  , ["activeFoliosOnly" : "false"]]
            let param3 = ["uid" : uid, "levelNo" : levelno]
            do {
               let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
               let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
               let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
               let data1 = try JSONSerialization.data(withJSONObject: param1, options: .prettyPrinted)
               let jsonString1 = NSString(data: data1, encoding: String.Encoding.utf8.rawValue)
               let urlEncodedJson1 = jsonString1!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
           let downloadUrl: String = "\(Shared.sharedInstance.baseUrl)client/dashboard/downloadPortfolioSummary?filters=\(urlEncodedJson1!)&selectedUser=\(urlEncodedJson!)"
           let destinationPath: DownloadRequest.DownloadFileDestination = { _, _ in
               //            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0];
               //            let fileURL = documentsURL.appendingPathComponent("\(uniqueName).pdf")
               self.folderURL = self.createFolder(folderName: "PortfolioSummary")
               self.fileURL = self.folderURL!.appendingPathComponent("\(uniqueName).pdf")
               return (self.fileURL!, [.removePreviousFile, .createIntermediateDirectories])
           }
           print(downloadUrl)
           Alamofire.download(downloadUrl, to: destinationPath)
               .downloadProgress { progress in
                   
               }
               .responseData { response in
                   print("response: \(response)")
                   switch response.result{
                   case .success:
                       
                       if response.destinationURL != nil, let filePath = response.destinationURL?.absoluteString {
                           completionHandler(filePath, true)
                           print(filePath)
                           do {
                               //Show UIActivityViewController to save the downloaded file
                               let contents  = try FileManager.default.contentsOfDirectory(at: self.folderURL!, includingPropertiesForKeys: nil, options: .skipsHiddenFiles)
                               for indexx in 0..<contents.count {
                                   if contents[indexx].lastPathComponent == self.fileURL!.lastPathComponent {
                                       let activityViewController = UIActivityViewController(activityItems: [contents[indexx]], applicationActivities: nil)
                                       self.delegate?.showActivity(vc: activityViewController)
                                       //self.present(activityViewController, animated: true, completion: nil)
                                   }
                               }
                           }
                           catch (let err) {
                               print("error: \(err)")
                           }
                       }
                       break
                   case .failure:
                       completionHandler("", false)
                       break
                   }
                   
           }
           }
            catch let JSONError as NSError {
               print("\(JSONError)")
           }
       }
    final func createFolder(folderName:String)->URL
    {
        var paths: [Any] = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory: String = paths[0] as? String ?? ""
        let dataPath: String = URL(fileURLWithPath: documentsDirectory).appendingPathComponent(folderName).absoluteString
        if !FileManager.default.fileExists(atPath: dataPath) {
            try? FileManager.default.createDirectory(atPath: dataPath, withIntermediateDirectories: false, attributes: nil)
        }
        let fileURL = URL(string: dataPath)
        return fileURL!
    }
}
