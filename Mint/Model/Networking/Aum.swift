//
//  Aum.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 30/10/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class Aum {
    var data:JSON?
    var data1:JSON?
    var data2:JSON?
         var url:String = ""
         var delegate:Completion?
         func network(table:UITableView){
             
               guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
               guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
               let headers = ["cookie" :
                   "connect.sid=\(connect)" + ";" +
                   "c_ux=\(cux)"
               ]
               let param1 = [["applicantUIds" : [Shared.sharedInstance.typeId]]]
               let param2 = ["componentName":"aumReport"]
           
               let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
              
               let urlParams2 = param2.compactMap({ (key, value) -> String in
                   return "\(key)=\(value)"
               }).joined(separator: "&")
               do {
                   let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                   let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                   let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
                   let data1 = try JSONSerialization.data(withJSONObject: param1, options: .prettyPrinted)
                   let jsonString1 = NSString(data: data1, encoding: String.Encoding.utf8.rawValue)
                   let urlEncodedJson1 = jsonString1!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                   if Shared.sharedInstance.clientType == "" {
                       url = "\(Shared.sharedInstance.baseUrl)broker/AUMReport/getAUMReportForMutualFunds?filters=&groupBy=1001&orderBy=AUM&orderByDesc=true&pageSize=100&componentForLoader=\(urlParams2)&currentPage=1&selectedUser="
                   }
                   else {
                       url = "\(Shared.sharedInstance.baseUrl)client/AUMReport/getAUMReportForMutualFunds?filters=&groupBy=1001&orderBy=AUM&orderByDesc=true&pageSize=100&componentForLoader=\(urlParams2)&currentPage=1&selectedUser=selectedUser=\(urlEncodedJson!)"
                   }
                   Alamofire.request(url, method: .get, headers: headers)
                       .responseJSON { response in
                           // print(response)
                           switch response.result {
                           case .success(let data):
                               let json = JSON(data)
                               // print(url)
                              if Shared.sharedInstance.jsonData != json {
                              Shared.sharedInstance.jsonData = json
                              }
                               self.data = json
                               //  print(self.data)
                              
                              
                               self.delegate?.complete(data: self.data)
                               table.reloadData()
                               break
                           case .failure(let error):
                               
                               print(error)
                           }
                   }
               }
               catch let JSONError as NSError {
                   print("\(JSONError)")
               }
               
           }
    func network2(table:UITableView , id:String){
                
                  guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
                  guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
                  let headers = ["cookie" :
                      "connect.sid=\(connect)" + ";" +
                      "c_ux=\(cux)"
                  ]
                  let param1 = [["fundid" : id]]
                  let param2 = ["componentName":"aumReport"]
              
                  let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
                 
                  let urlParams2 = param2.compactMap({ (key, value) -> String in
                      return "\(key)=\(value)"
                  }).joined(separator: "&")
                  do {
                      let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                      let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                      let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
                      let data1 = try JSONSerialization.data(withJSONObject: param1, options: .prettyPrinted)
                      let jsonString1 = NSString(data: data1, encoding: String.Encoding.utf8.rawValue)
                      let urlEncodedJson1 = jsonString1!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                      if Shared.sharedInstance.clientType == "" {
                          url = "\(Shared.sharedInstance.baseUrl)broker/AUMReport/getAUMReportForMutualFunds?filters=\(urlEncodedJson1!)&groupBy=1002&orderBy=AUM&orderByDesc=true&pageSize=100&componentForLoader=\(urlParams2)&currentPage=1&selectedUser="
                      }
                      else {
                          url = "\(Shared.sharedInstance.baseUrl)client/AUMReport/getAUMReportForMutualFunds?filters=&groupBy=1001&orderBy=AUM&orderByDesc=true&pageSize=100&componentForLoader=\(urlParams2)&currentPage=1&selectedUser=selectedUser=\(urlEncodedJson1!)"
                      }
                      Alamofire.request(url, method: .get, headers: headers)
                          .responseJSON { response in
                              // print(response)
                              switch response.result {
                              case .success(let data):
                                  let json = JSON(data)
                                  // print(url)
                                 if Shared.sharedInstance.jsonData != json {
                                                           Shared.sharedInstance.jsonData = json
                                                       }
                                  self.data1 = json
                               //     print(self.data1)
                                 
                                 
                                  self.delegate?.complete(data: self.data1)
                                  table.reloadData()
                                  break
                              case .failure(let error):
                                  
                                  print(error)
                              }
                      }
                  }
                  catch let JSONError as NSError {
                      print("\(JSONError)")
                  }
                  
              }
    func network3(table:UITableView , id:String , sid:String){
                  
                    guard let connect = UserDefaults.standard.value(forKey: "connect.sid") else {return }
                    guard let cux = UserDefaults.standard.value(forKey: "cux") else {return}
                    let headers = ["cookie" :
                        "connect.sid=\(connect)" + ";" +
                        "c_ux=\(cux)"
                    ]
        let param1 = [["fundid" : id] , ["schid" : sid]]
                    let param2 = ["componentName":"aumReport"]
                
                    let param3 = ["uid" : Shared.sharedInstance.typeId , "levelNo" : Shared.sharedInstance.typeLevel]
                   
                    let urlParams2 = param2.compactMap({ (key, value) -> String in
                        return "\(key)=\(value)"
                    }).joined(separator: "&")
                    do {
                        let data = try JSONSerialization.data(withJSONObject: param3, options: .prettyPrinted)
                        let jsonString = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                        let urlEncodedJson = jsonString!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed) // Trigger alaomofire request with url
                        let data1 = try JSONSerialization.data(withJSONObject: param1, options: .prettyPrinted)
                        let jsonString1 = NSString(data: data1, encoding: String.Encoding.utf8.rawValue)
                        let urlEncodedJson1 = jsonString1!.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
                        if Shared.sharedInstance.clientType == "" {
                            url = "\(Shared.sharedInstance.baseUrl)broker/AUMReport/getAUMReportForMutualFunds?filters=\(urlEncodedJson1!)&groupBy=100&orderBy=AUM&orderByDesc=true&pageSize=100&componentForLoader=\(urlParams2)&currentPage=1&selectedUser="
                        }
                        else {
                            url = "\(Shared.sharedInstance.baseUrl)client/AUMReport/getAUMReportForMutualFunds?filters=&groupBy=1001&orderBy=AUM&orderByDesc=true&pageSize=100&componentForLoader=\(urlParams2)&currentPage=1&selectedUser=selectedUser=\(urlEncodedJson!)"
                        }
                        Alamofire.request(url, method: .get, headers: headers)
                            .responseJSON { response in
                                // print(response)
                                switch response.result {
                                case .success(let data):
                                    let json = JSON(data)
                                    // print(url)
                                   
                                    self.data2 = json
                                      print(self.data2)
                                   if Shared.sharedInstance.jsonData != json {
                                                             Shared.sharedInstance.jsonData = json
                                                         }
                                   
                                    self.delegate?.complete(data: self.data2)
                                    table.reloadData()
                                    break
                                case .failure(let error):
                                    
                                    print(error)
                                }
                        }
                    }
                    catch let JSONError as NSError {
                        print("\(JSONError)")
                    }
                    
                }
}

