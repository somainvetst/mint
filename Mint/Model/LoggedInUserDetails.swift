//
//  LoggedInUserDetails.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 09/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//
import Foundation

// MARK: - LoggedInUserData
class LoggedInUserDetails: Decodable {
    let status: Int?
    let message: String?
    let result: Result?
}

// MARK: - Result
class Result: Decodable {
    let username, name, uid, brokerDomain: String?
    let token: JSONNull?
    let userType, levelid, bid: String?
    let firstLogin: Int?
    let brokerUid: String?
    let orgKey: JSONNull?
    let email, phone: String?
    let txnEnable: Int?
    let lowerReportingLevels: [LowerReportingLevel]?
    let levelNo: Int?
    let reportingLevelName: String?
  
}

// MARK: - LowerReportingLevel
class LowerReportingLevel: Decodable {
    let levelid: String?
    let levelNo: Int?
    let levelName: String?
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {
    
    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }
    
    public var hashValue: Int {
        return 0
    }
    
    public init() {}
    
    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}
