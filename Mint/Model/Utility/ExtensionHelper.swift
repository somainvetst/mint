//
//  ExtensionHelper.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 12/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
extension Array where Element: Equatable {
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }
    
}

extension UIView {
    
    static var nib: UINib {
        
        return UINib(nibName: "\(self)", bundle: nil)
    }
}

extension UINib {
    
    func instantiate() -> Any? {
        
        return self.instantiate(withOwner: nil, options: nil).first
    }
}
extension UIView {
    
    static func instantiateFromNib() -> Self? {
        
        func instanceFromNib<T: UIView>() ->T? {
            
            return nib.instantiate() as? T
        }
        
        return instanceFromNib()
    }
}
protocol IdentifiableCell: class {
    
    static var identifier: String { get }
}

extension UITableViewCell: IdentifiableCell {
    
    static var identifier: String {
        
        return "\(self)"
    }
}
extension UICollectionViewCell: IdentifiableCell {
    
    static var identifier: String {
        
        return "\(self)"
    }
}

extension UITableView {
    
    func register<T>(cell: T.Type) where T: IdentifiableCell, T: UITableViewCell {
        
        register(cell.nib, forCellReuseIdentifier: cell.identifier)
    }
    
    func dequeueReusableCell<T: IdentifiableCell>(with cell: T.Type) -> T? {
        
        return dequeueReusableCell(withIdentifier: cell.identifier) as? T
        
    }
}
extension UICollectionView {
    
    func register<T>(cell: T.Type) where T: IdentifiableCell, T: UICollectionViewCell {
        
        register(cell.nib, forCellWithReuseIdentifier: cell.identifier)
    }
    
    
}
extension Collection where Iterator.Element == String {
    var doubleArray: [Double] {
        return compactMap{ Double($0) }
    }
    var floatArray: [Float] {
        return compactMap{ Float($0) }
    }
}
extension UIImageView {
    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}
extension Double {
    func roundtoPlace(places : Int) -> Double {
        let divisor = pow(10.0 , Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension Date {
    
    func getPreviousMonth() -> Date? {
        return Calendar.current.date(byAdding: .month, value: -1, to: self)
    }
    
    // This Month Start
    func getThisMonthStart() -> Date? {
        let components = Calendar.current.dateComponents([.year, .month], from: self)
        return Calendar.current.date(from: components)!
    }
    
    func getThisMonthEnd() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.month += 1
        components.day = 1
        components.day -= 1
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    //Last Month Start
    func getLastMonthStart() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.month -= 1
        return Calendar.current.date(from: components as DateComponents)!
    }
    
    //Last Month End
    func getLastMonthEnd() -> Date? {
        let components:NSDateComponents = Calendar.current.dateComponents([.year, .month], from: self) as NSDateComponents
        components.day = 1
        components.day -= 1
        return Calendar.current.date(from: components as DateComponents)!
    }
}
extension Calendar {
    func startOfYear(_ date: Date) -> Date {
        return self.date(from: self.dateComponents([.year], from: date))!
    }
    
    func endOfYear(_ date: Date) -> Date {
        return self.date(from: DateComponents(year: self.component(.year, from: date), month: 12, day: 31))!
    }
}
//Animations
extension UIView {
    func showViewFromBottom(height:CGFloat) {
        
        self.transform = CGAffineTransform(translationX: 0, y: height)
        UIView.animate(withDuration: 0.5) {
            self.transform = .identity
        }
        
    }
    func hideView() {
        self.transform = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.5) {
            self.transform = .identity
        }
    }
}
extension DateFormatter {
    func years<R: RandomAccessCollection>(_ range: R) -> [String] where R.Iterator.Element == Int { setLocalizedDateFormatFromTemplate("yyyy")
        return range.compactMap{ DateComponents(calendar: .current, year: $0).date }.compactMap{ string(from: $0) } }
}
extension String{
    func toCurrencyFormat() -> String {
        if let intValue = Double(self){
            let val = intValue.roundtoPlace(places: 0)
            let intval = Int(val)
            let numberFormatter = NumberFormatter()
            numberFormatter.locale = Locale(identifier: "en_IN")
            numberFormatter.numberStyle = NumberFormatter.Style.currency
            var amount = numberFormatter.string(from: NSNumber(value: intval)) ?? ""
            if let range = amount.range(of: ".") {
                amount = amount[..<range.lowerBound].trimmingCharacters(in: .whitespaces)
            }
            return amount
        }
        return ""
    }
    func toDoubleCuurency() -> String {
        if let intValue = Double(self){
            let val = intValue
            let numberFormatter = NumberFormatter()
            numberFormatter.locale = Locale(identifier: "en_IN")
            numberFormatter.numberStyle = NumberFormatter.Style.currency
            let amount = numberFormatter.string(from: NSNumber(value: val)) ?? ""
            return amount
        }
        return ""
        
    }
}
extension Date {

    func offsetFrom(date : Date) -> String {

        let dayHourMinuteSecond: Set<Calendar.Component> = [.day, .hour, .minute, .second]
        let difference = NSCalendar.current.dateComponents(dayHourMinuteSecond, from: date, to: self);

        let seconds = "\(difference.second ?? 0)"
        let minutes = "\(difference.minute ?? 0)m" + " " + seconds
        let hours = "\(difference.hour ?? 0)h" + " " + minutes
        let days = "\(difference.day ?? 0)d" + " " + hours

        if let day = difference.day, day          > 0 { return days }
        if let hour = difference.hour, hour       > 0 { return hours }
        if let minute = difference.minute, minute > 0 { return minutes }
        if let second = difference.second, second > 0 { return seconds }
        return ""
    }

}
extension String {
    func toDate() -> String {
        let dateFormatting = DateFormatter()
        dateFormatting.locale = Locale(identifier: "en_US_POSIX")
        dateFormatting.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        guard let date = dateFormatting.date(from: self) else {return ""}
        let finalFormat = DateFormatter()
        finalFormat.dateFormat = Shared.sharedInstance.dateFormatter
        print(date  , "jjh")
        let dateString = finalFormat.string(from: date)
        print(dateString)
        return dateString
    }
    func toBasicDate() -> String {
        let dateFormatting = DateFormatter()
        dateFormatting.dateFormat = "yyyy-MM-dd"
        guard let date = dateFormatting.date(from: self) else {return ""}
        let finalFormat = DateFormatter()
        finalFormat.dateFormat = Shared.sharedInstance.dateFormatter
        let dateString = finalFormat.string(from: date)
        return dateString
    }
}
extension UIViewController {
    
    func alert(message: String, title: String = "") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}
extension UITextField {
    
    /// Validates all text field are non-nil and non-empty, Returns true if all fields pass.
    /// - Returns: Bool
    static func validateAll(textFields:[UITextField]) -> Bool {
        // Check each field for nil and not empty.
        for field in textFields {
            // Remove space and new lines while unwrapping.
            guard let fieldText = field.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
                return false
            }
            // Are there no other charaters?
            if (fieldText.isEmpty) {
                return false
            }
            
        }
        // All fields passed.
        return true
    }
}
extension String {
    
    func toShortCurrency() -> String {
        if self == "0" {
            return "0"
        }
        else {
            var currency = self
            if currency.hasPrefix("-") {
                currency.removeFirst()
                
                
                
                if let  amount = Double(currency) {
                    if amount >= 10000000.0 {
                        var shortAmount = amount
                        
                        shortAmount /= 10000000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "-₹\(String(shortAmount))Cr"
                    }
                        
                    else if amount >= 1000000.0 {
                        var shortAmount = amount
                        shortAmount /= 1000000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "-₹\(String(shortAmount))M"
                    }
                    else  if amount >= 100000.0 {
                        var shortAmount = amount
                        shortAmount /= 100000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "-₹\(String(shortAmount))Lc"
                    }
                    else if amount >= 1000.0 {
                        var shortAmount = amount
                        shortAmount /= 1000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "-₹\(String(shortAmount))K"
                    }
                }
            }
            else {
                if let  amount = Double(self) {
                    if amount >= 10000000.0 {
                        var shortAmount = amount
                        
                        shortAmount /= 10000000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "₹\(String(shortAmount))Cr"
                    }
                        
                    else if amount >= 1000000.0 {
                        var shortAmount = amount
                        shortAmount /= 1000000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "₹\(String(shortAmount))M"
                    }
                    else  if amount >= 100000.0 {
                        var shortAmount = amount
                        shortAmount /= 100000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "₹\(String(shortAmount))Lc"
                    }
                    else if amount >= 1000.0 {
                        var shortAmount = amount
                        shortAmount /= 1000.0
                        shortAmount = shortAmount.roundtoPlace(places: 2)
                        return "₹\(String(shortAmount))K"
                    }
                }
            }
            return ""
        }
    }
}

extension String {
    func roundOffString() -> String {
        guard var doub = Double(self) else {
            return ""
        }
        doub = doub.roundtoPlace(places: 2)
        return String(doub)
    }
}
extension String {
    func mappedTransactionType() -> String {
        if self == "SWI" {
            
            
            return "Switch In"
        }
        else if self == "DVP" {
            
            
            return "Dividend Payout"
        }
        else if self == "SWO" {
            
            
            return  "Switch Out"
        }
        else if self == "NRP" || self == "ESP"  || self == "EBP" {
            
            return  "Purchase"
        }
        else if self == "DRI" {
            
            return  "Dividend Reinvestment"
        }
        else if self == "NRS" || self == "ESS"  || self == "EBS" {
            
            return  "Sell"
        }
        else if self == "BON" || self == "BOB"  {
            
            return  "Bonus"
        }
        else if self == "STI" {
            
            return  "Systematic Transfer In"
        }
        else if self == "STO" {
            
            return  "Systematic Transfer Out"
        }
        else if self == "POS" {
            
            return  "Dividend Paid"
        }
        else if self == "POB" {
            
            return "Interest"
        }
        else if self == "SI" {
            
            return  "Single"
        }
        else if self == "JO" {
            
            return  "Joint"
        }
        else if self == "AS" {
            
            return  "Anyone or Survivor"
        }
        else if self == "ES" {
            
            return  "Either or Survivor"
        }
        else if self == "SIP" {
            
            return  "SIP"
        }
        return "Not Available"
    }
    func transactionToShortForm() -> String {
        if self == "Purchase" {
            return "NRP"
        }
        else if self == "SIP" {
            return "SIP"
        }
        else if self == "STI" {
            return "STI"
        }
        else if self == "Div Reinvestment" {
            return "DIR"
        }
        else if self == "Bonus" {
            return "BON"
        }
        else if self == "Sell" {
            return "NRS"
        }
        else if self == "STO" {
            return "STO"
        }
        else if self == "Switch Out" {
            return "SWO"
        }
        else if self == "SWP" {
            return "SWO"
        }
        else if self == "Div Payout" {
            return "DVP"
        }
        
        return ""
    }
    func subTypesTranaction() -> String {
           if self == "G" {
               return "Group"
           }
           else if self == "DR" {
               return "Dividend Reinvestment"
           }
           else if self == "STI" {
               return "STI"
           }
           return ""
       }
}
extension UIView {
    enum Direction: Int {
        case topToBottom = 0
        case bottomToTop
        case leftToRight
        case rightToLeft
    }
    func startShimmeringAnimation(animationSpeed: Float = 1.4,
                                  direction: Direction = .leftToRight,
                                  repeatCount: Float = MAXFLOAT) {
        
        // Create color  ->2
        let lightColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.1).cgColor
        let blackColor = UIColor.black.cgColor
        
        // Create a CAGradientLayer  ->3
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [blackColor, lightColor, blackColor]
        gradientLayer.frame = CGRect(x: -self.bounds.size.width, y: -self.bounds.size.height, width: 3 * self.bounds.size.width, height: 3 * self.bounds.size.height)
        
        switch direction {
        case .topToBottom:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 0.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 1.0)
            
        case .bottomToTop:
            gradientLayer.startPoint = CGPoint(x: 0.5, y: 1.0)
            gradientLayer.endPoint = CGPoint(x: 0.5, y: 0.0)
            
        case .leftToRight:
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
            
        case .rightToLeft:
            gradientLayer.startPoint = CGPoint(x: 1.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 0.0, y: 0.5)
        }
        
        gradientLayer.locations =  [0.35, 0.50, 0.65] //[0.4, 0.6]
        self.layer.mask = gradientLayer
        
        // Add animation over gradient Layer  ->4
        CATransaction.begin()
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = [0.0, 0.1, 0.2]
        animation.toValue = [0.8, 0.9, 1.0]
        animation.duration = CFTimeInterval(animationSpeed)
        animation.repeatCount = repeatCount
        CATransaction.setCompletionBlock { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.layer.mask = nil
        }
        gradientLayer.add(animation, forKey: "shimmerAnimation")
        CATransaction.commit()
    }
    
    func stopShimmeringAnimation() {
        self.layer.mask = nil
    }
}
extension String {
    func trailingTrim() -> String {
        var newString = self
        
        while newString.last?.isWhitespace == true {
            newString = String(newString.dropLast())
        }
        
        return newString
    }
}
