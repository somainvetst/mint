//
//  Routes.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 14/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
class Routes {
    static let shared = Routes()
    
    func gotoLogin(nav:UINavigationController , id:Int) {
        let loginView:LoginViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        loginView.id = id
        nav.pushViewController(loginView, animated: true)
    }
    func gotoDashboard(nav:UINavigationController) {
        let dashvc: DashboardViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController;
        nav.pushViewController(dashvc, animated: true)
    }
    func gotoPin(nav:UINavigationController) {
        let vc: PinViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PinViewController") as! PinViewController;
        nav.pushViewController(vc, animated: true)
    }
    func gotoTab(nav:UINavigationController) {
        let vc: TabBar  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "TabBar") as! TabBar;
        nav.pushViewController(vc, animated: true)
    }
    func gotoFolio(nav:UINavigationController) {
        let vc: FolioLookupViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "FolioLookupViewController") as! FolioLookupViewController;
        nav.pushViewController(vc, animated: true)
    }
    func gotoCapitalReport(nav:UINavigationController) {
        let vc: CapitalGainViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "CapitalGainViewController") as! CapitalGainViewController;
        nav.pushViewController(vc, animated: true)
    }
    func goToSipTransaction(nav:UINavigationController , dates:[String] , frequency:[String] , schemeData:[PassedSchemeForTransaction]) {
        let vc : SipTransactionViewController = Shared.sharedInstance.storyboard2.instantiateViewController(withIdentifier: "SipTransactionViewController") as! SipTransactionViewController
        vc.sipDates = dates
        vc.frequency = frequency
       // vc.data = schemeData[0]
        nav.pushViewController(vc, animated: true)
    }
    func goToPortfolioSummary(nav:UINavigationController) {
        let vc: PortfolioSummaryViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioSummaryViewController") as! PortfolioSummaryViewController
        nav.pushViewController(vc, animated: true)
    }
    func goToClientsSearch(nav:UINavigationController) {
        let vc: ClientSearchViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "ClientSearchViewController") as! ClientSearchViewController
        nav.pushViewController(vc, animated: true)
    }
    func goToTransaction(nav:UINavigationController) {
           let vc: TransactionsViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "TransactionsViewController") as! TransactionsViewController
           nav.pushViewController(vc, animated: true)
       }
    func goToPortfolio(nav:UINavigationController) {
              let vc: PortfolioDetailsViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioDetailsViewController") as! PortfolioDetailsViewController
              nav.pushViewController(vc, animated: true)
          }
    func goToPortfolioClient(nav:UINavigationController) {
                let vc:PortfolioClientViewController = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PortfolioClientViewController") as! PortfolioClientViewController
                nav.pushViewController(vc, animated: true)
            }
    func goToInsurance(nav:UINavigationController) {
                  
               }
    func goToRedemptionTransaction(nav:UINavigationController) {
           let vc : RedemptionViewController = Shared.sharedInstance.storyboard2.instantiateViewController(withIdentifier: "RedemptionViewController") as! RedemptionViewController
           nav.pushViewController(vc, animated: true)
    }
    func goToSwitchTransaction(nav:UINavigationController) {
              let vc : SwitchViewController = Shared.sharedInstance.storyboard2.instantiateViewController(withIdentifier: "SwitchViewController") as! SwitchViewController
              nav.pushViewController(vc, animated: true)
       }
    func goToWebView(nav:UINavigationController , heading:String , url:String) {
        let vc: WebViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "WebViewController") as! WebViewController
                      vc.heading = heading
                      vc.urlLink = url
                      nav.pushViewController(vc, animated: true)
    }
    func goToPurchaseTransaction(nav:UINavigationController) {
                 let vc : PurchaseViewController = Shared.sharedInstance.storyboard2.instantiateViewController(withIdentifier: "PurchaseViewController") as! PurchaseViewController
                 nav.pushViewController(vc, animated: true)
          }
}
