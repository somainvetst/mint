//
//  CoreData.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 10/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData
import SwiftyJSON
class CoreData {
    static let core = CoreData()
    var data:[NSManagedObject] = []
    
    func saveData(datatoAdd:[String:Any] , enntity:String , attribute:String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: enntity, in: context)
        if isExist(entity: enntity).description  == "false" {
            let newData = NSManagedObject(entity: entity!, insertInto: context)
            newData.setValue(datatoAdd, forKey: attribute)
            do {
                try context.save()
                data.append(newData)
            } catch {
                print("Failed")
            }
        }
        else {
            let request = NSFetchRequest<NSManagedObject>(entityName: enntity)
let result = try? context.fetch(request)
            let res = result?[0]
            let new = res?.value(forKey: attribute) as! [String:Any]
//            for i in datatoAdd {
//                if new.contains(i) {
//
//                }
//                else {
                    //new = new + datatoAdd
                    res?.setValue(new , forKey: attribute)
                    do {
                        try context.save()
                        guard let res = res else {
                            return
                        }
                        data.append(res)
                    }
                    catch {
                        print("failed")
                    }
               // }
            }
        }
    
    func fetchCartData(entity:String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request =  NSFetchRequest<NSManagedObject>(entityName: entity)
        // request.predicate = NSPredicate(format: "id == %@", id)
        request.returnsObjectsAsFaults = false
        request.returnsDistinctResults = true
        do {
            data = try context.fetch(request)
          //  print(data)
            
        } catch {
            
            print("Failed")
        }
        
    }
    func deleteData(datatofetch:[[String:String]] , entity:String , attribute:String) {
        if isExist(entity: entity).description == "false" {}
        else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
//            let entity = NSEntityDescription.entity(forEntityName: "Cart", in: context)
            let request =  NSFetchRequest<NSManagedObject>(entityName: entity)
            var result = try? context.fetch(request)
            
            let res = result![0]
            
            var sname = res.value(forKeyPath: attribute) as! [[String:String]]
            for i in datatofetch {
                sname.remove(object: i)
                print("deleted")
                res.setValue(sname, forKey: attribute)
                do {
                    try context.save()
                    data.append(res)
                    
                } catch {
                    print("Failed saving")
                }
            }
        }
        
    }
    func deleteAllData(entity:String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.persistentContainer.viewContext
        let DelAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: entity))
        do {
            try managedContext.execute(DelAllReqVar)
        }
        catch {
            print(error)
        }
    }
    
    
    func isExist(entity:String) -> Bool {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entity)
        let res = try! context.fetch(request)
        
        return res.count > 0 ? true : false
    }
}
