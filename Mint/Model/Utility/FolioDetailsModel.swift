//
//  FolioDetailsModel.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 23/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import Foundation
class FolioDetailsModel {
    var schemeName:String
    var amount:String
    var folio:String
    var ucc:String
    var taxstatus:String
    var holding:String
    var pan:String
    var investor:String
    var client:String
    var email:String
    var address:String
    init(schemename:String , amount:String , folio :String , ucc:String , taxstatus:String , holding:String , pan:String , investor:String , client:String , address:String , email:String) {
        self.address = address
        self.amount = amount
        self.client = client
        self.email = email
        self.folio = folio
        self.holding = holding
        self.investor = investor
        self.pan = pan
        self.schemeName = schemename
        self.ucc = ucc
        self.taxstatus = taxstatus
    }
}
