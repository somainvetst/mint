//
//  Transactiondata.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 04/11/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import Foundation
class Transactiondata {
    static let shared = Transactiondata()
    var name = ""
    var folioid = ""
    var schemename = ""
    var ucc = ""
    var schid = ""
    var switchSchemeName = ""
    var subType:[String] = []
    var folioNum = ""
}
