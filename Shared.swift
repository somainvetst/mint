//
//  Shared.swift
//  Mint
//
//  Created by Excel Net Solutions Pvt Ltd on 08/08/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
protocol logging:class {
    func logout()
}
class Shared {
    static let sharedInstance = Shared()
    var delegate:logging?
    
    let color = UIColor(red: 1.0/255.0, green: 148.0/255.0, blue: 119.0/255.0, alpha: 1.0)
    
    var baseUrl:String = "https://\(DomainName.domain.domainNames ?? "").investwell.app/api/"
    var cux:String = ""
    var key:String = ""
    var logindatabroker:[String] = []
    var loginDataid:[String] = []
    var name:String = ""
    var id:Int = 200
    var membersName:[String] = []
    var levelidMain:String = ""
    var bid:String = ""
    var familymemberdata:JSON?
    var clientType:String = ""
    var typeId:String = ""
    var typeAppid:String = ""
    var typeLevel:String = ""
    var clientName:String = ""
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let storyboard2 = UIStoryboard(name: "Transactions", bundle: nil)
    var brokerDomain:String = ""
    var dateFormatter = "dd-MM-yyyy"
    var time:Int = 0
    var logo:UIImage? = nil
    var sellArray:[Int] = [1,1,1,1,1,1]
    var purchaseArray:[Int] = [1,1,1,1,1,1,1]
    var jsonData : JSON? {
        didSet {
            delegate?.logout()
        }
    
    }
    func loggingout(nav:UINavigationController) {
      
            if jsonData?["status"] == -1 &&  jsonData?["message"].stringValue == "User not authenticated" {
                let vc: PinViewController  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "PinViewController") as! PinViewController;
                nav.pushViewController(vc, animated: true)
                
            }
    }
    func clear() {
        cux = ""
        key = ""
      //  logindata = nil
        name = ""
        membersName = []
        familymemberdata = nil
        clientType = ""
        typeLevel = ""
        typeId = ""
        clientName = ""
        brokerDomain = ""
        id = 0
        typeAppid = ""
        levelidMain = ""
        bid = ""
    }
}

