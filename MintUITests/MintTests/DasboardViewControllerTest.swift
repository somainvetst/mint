//
//  DasboardViewControllerTest.swift
//  MintTests
//
//  Created by Excel Net Solutions Pvt Ltd on 06/09/19.
//  Copyright © 2019 Excel Net Solutions Pvt Ltd. All rights reserved.
//

import XCTest
@testable import Mint
class DasboardViewControllerTest: XCTestCase {
    var controller:DashboardViewController!
    override func setUp() {
        
        controller  = Shared.sharedInstance.storyboard.instantiateViewController(withIdentifier: "DashboardViewController") as? DashboardViewController;
        let dummy = controller.view
        controller.viewDidLoad()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testTableViewOutlet() {
        XCTAssertNotNil(controller.table , "initialise table")
        XCTAssertNotNil(controller.backBtn , "connect backbutton")
        let cell1 = controller.table.dequeueReusableCell(with: DashboardTableViewCell.self)
        XCTAssertNotNil(cell1, "register dashboard cell")
        let cell2 = controller.table.dequeueReusableCell(with: InvestmentRouteTableViewCell.self)
        XCTAssertNotNil(cell2, "register investment cell")
        let cell3 = controller.table.dequeueReusableCell(with: CalculatorsTableViewCell.self)
        XCTAssertNotNil(cell3, "register calculator cell")
        let cell4 = controller.table.dequeueReusableCell(with: BrokerSearchPanelTableViewCell.self)
        XCTAssertNotNil(cell4, "register brokersearch cell")
        let cell5 = controller.table.dequeueReusableCell(with: PieChartTableViewCell.self)
        XCTAssertNotNil(cell5, "register piechart cell")
        
    }
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
